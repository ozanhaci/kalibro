#!/usr/bin/python
# encoding: utf-8
"""
Kalibro
Licensed under GNU GPLv3
Copyright By OZAN HACIBEKIROGLU
Home Page: http://www.ozanh.com/kalibro
e-mail: ozan_haci@yahoo.com
"""
from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import
import os
import sys

__version__ = "2.6b1"
__author__ = "Ozan HACIBEKIROGLU"

if sys.version_info[:2] != (2, 7):
    sys.stderr.write("Only Python 2.7 is supported")
    sys.exit(1)

import wx

from kalibrolib import pubsub
from kalibrolib import startup
from kalibrolib import auth
from kalibrolib.mainwin import MainWindow
from kalibrolib.utils import isWritable
from kalibrolib.debugging import DEBUG, log_debug, debug_trace


if __name__ == "__main__":
    if hasattr(sys, "frozen"):
        argv0 = sys.executable
    else:
        argv0 = __file__
    curdir = os.path.abspath(os.path.dirname(unicode(argv0,
                                                sys.getfilesystemencoding())))
    os.chdir(curdir)

    app = wx.App(0)
    if not isWritable(curdir):
        error_msg = (("You do not have write permission in %s\n" % curdir) +
                     "Directory must be writable.\nExiting...")
        wx.MessageBox(error_msg, "Error", wx.ICON_ERROR)
        try:
            sys.stderr.write(error_msg + "\n")
        except:
            app.Exit()
    else:
        startup.register()
        if hasattr(sys, "frozen"):
            try:
                sys.stderr = open("out_err.txt", "a+")
                sys.stdout = open("out.txt", "a+")
            except:
                # silently ignore if user has no write permission to curdir
                pass
        if DEBUG:
            log_debug(debug_trace(), "Python Version=" +
                      ".".join(map(str, sys.version_info[:3])))
            log_debug(debug_trace(), "Version=" + __version__)
            log_debug(debug_trace(), "Author=" + __author__)
        pubsub.publish("start")
        def authorized():
            pubsub.unsubscribe("authorized", authorized)
            MainWindow()
        pubsub.subscribe("authorized", authorized)
        if not auth.ModelAuth().auth_installed():
            pubsub.publish("authorized")
        else:
            auth.LoginWin()
    app.MainLoop()
    pubsub.publish("exit")
