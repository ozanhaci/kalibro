# encoding: utf-8
from __future__ import print_function

import os
import sys
import subprocess
import zipfile

if sys.version_info[:2] != (2, 7):
    sys.stderr.write("Sorry, only Python 2.7 is supported")
    sys.exit(1)

PY2EXE = False
if sys.argv[-1] == "py2exe":
    from distutils.core import setup
    from py2exe.build_exe import py2exe
    PY2EXE = True
else:
    from setuptools import setup

sys.path.insert(0, ".")
sys.path.insert(1, "./csv2db")

from kalibrolib.constants import IS_WINDOWS

CUR_DIR = os.path.abspath(os.path.dirname(__file__))
DIST_DIR = os.path.abspath(os.path.join(CUR_DIR, "dist"))

data_files = [(r"locale/en/LC_MESSAGES", [r"locale/en/LC_MESSAGES/Kalibro.mo"]),
              (r"locale/pl/LC_MESSAGES", [r"locale/pl/LC_MESSAGES/Kalibro.mo"]),
              (r"locale/tr/LC_MESSAGES", [r"locale/tr/LC_MESSAGES/Kalibro.mo"]),
              ("", ["README.txt",
                    "INSTALL.txt",
                    "RELEASE_NOTES.txt",
                    "report.html",
                    "kalibro.png",
                    ".new",
                    "kalibro_main.csv2db"
                    ]
               ),
              ]
licenses_dict = {}
for root, dirs, files in os.walk("./licenses"):
    for f in files:
        target = os.path.join(root, f)
        targetrel = os.path.relpath(target)
        targetdir = os.path.dirname(targetrel)
        licenses_dict.setdefault(targetdir, []).append(target)
data_files = data_files + list(licenses_dict.items())

packages = []
includes = ["six"]
excludes = ['_gtkagg', '_tkagg', 'bsddb', 'curses', 'pywin.debugger',
            'pywin.debugger.dbgcon', 'pywin.dialogs', 'tcl',
            'Tkconstants', 'Tkinter', "_imagingtk", "PIL._imagingtk",
            "ImageTk", "PIL.ImageTk", "FixTk",
            "_ssl", "sphinx",
            # csv2db-importer - no oracle, mysql and postgres support now
            "cx_Oracle", "psycopg2", "pymysql", "MySQLdb", "pysqlite2",
            # unneccessary packages
            "pytest", "_pytest", "unittest", "doctest", "test", "mock",
            ]
dll_excludes = ['libgdk-win32-2.0-0.dll', 'libgobject-2.0-0.dll',
                'tcl84.dll', 'tk84.dll',
                'msvcp90.dll', 'POWRPROF.dll', 'PSAPI.DLL', 'OLEAUT32.dll',
                'USER32.dll', 'COMCTL32.dll', 'SHELL32.dll', 'ole32.dll',
                'WINMM.dll', 'WSOCK32.dll', 'COMDLG32.dll', 'ADVAPI32.dll',
                'GDI32.dll', 'WS2_32.dll', 'WINSPOOL.DRV', 'CRYPT32.dll',
                'KERNEL32.dll', 'RPCRT4.dll', "VERSION.dll",
                "w9xpopen.exe",
                # csv2db-importer
                "MSVCR100.dll", "NETAPI32.dll", "ntdll.dll", "msvcrt.dll",
                "MPR.dll", "OCI.dll", "Secur32.dll", "SHFOLDER.dll",
                "mfc90.dll",
                "API-MS-Win-Core-Debug-L1-1-0.dll",
                "API-MS-Win-Core-DelayLoad-L1-1-0.dll",
                "API-MS-Win-Core-LocalRegistry-L1-1-0.dll",
                "API-MS-Win-Core-ErrorHandling-L1-1-0.dll",
                "API-MS-Win-Core-Handle-L1-1-0.dll",
                "API-MS-Win-Core-Interlocked-L1-1-0.dll",
                "API-MS-Win-Core-IO-L1-1-0.dll",
                "API-MS-Win-Core-LibraryLoader-L1-1-0.dll",
                "API-MS-Win-Core-Localization-L1-1-0.dll",
                "API-MS-Win-Core-Memory-L1-1-0.dll",
                "API-MS-Win-Core-Misc-L1-1-0.dll",
                "API-MS-Win-Core-ProcessEnvironment-L1-1-0.dll",
                "API-MS-Win-Core-ProcessThreads-L1-1-0.dll",
                "API-MS-Win-Core-Profile-L1-1-0.dll",
                "API-MS-Win-Core-String-L1-1-0.dll",
                "API-MS-Win-Core-Synch-L1-1-0.dll",
                "API-MS-Win-Core-SysInfo-L1-1-0.dll"]

def get_py2exe_params():
    if not PY2EXE:
        return {}
    return dict(windows=[{'script': 'kalibro.py',
                          'dest_base': 'Kalibro',
                          'icon_resources': [(0, 'kalibro.ico')]
                          },
                         {'script': 'csv2db/csv2db-importer.py',
                          'dest_base': 'csv2db-importer',
                          'icon_resources': [(0, 'kalibro.ico')]
                          }],
                options={'py2exe': {'compressed': 1,
                                    'bundle_files': 3,
                                    'optimize': 0,
                                    'dist_dir': DIST_DIR,
                                    'packages': packages,
                                    'includes': includes,
                                    'excludes': excludes,
                                    'dll_excludes': dll_excludes,
                                    'unbuffered': True,
                                    # 'xref': True,
                                    }
                         },
                zipfile='lib.zip'
               )


def get_setuptools_params():
    if not PY2EXE:
        win_tests_require = []
        win_install_requires = []
        if IS_WINDOWS:
            win_tests_require = []
            win_install_requires = ["sqlalchemy >= 1.0"]
        return dict(zip_safe=False,
                    scripts=['kalibro.py'],
                    packages=["kalibrolib"],
                    include_package_data=True,
                    test_suite="kalibrolib.test.suite",
                    tests_require=[
                                   "jinja2 >= 2.8",
                                   "six >= 1.0",
                                   ] + win_tests_require,
                    install_requires=[
                                      "jinja2 >= 2.8",
                                      ] + win_install_requires
                    )
    return {}


VERSION = "2.6.2.1"
params = get_py2exe_params()
params.update(get_setuptools_params())
setup(name="Kalibro",
      version=VERSION,
      author="Ozan HACIBEKIROGLU",
      author_email="ozan_haci@yahoo.com",
      url="http://www.ozanh.com/kalibro",
      license="GPL",
      data_files=data_files,
      **params
    )

if PY2EXE:
    if "UPX_PATH" in os.environ and os.environ["UPX_PATH"] and os.path.isfile(os.environ["UPX_PATH"]):
        dll_list = [i for i in os.listdir(DIST_DIR)
                    if i.lower()[-4:] in (".dll", ".exe", ".pyd")]
        for d in dll_list:
            fpath = os.path.join(DIST_DIR, d)
            try:
                res = subprocess.call([os.environ["UPX_PATH"], fpath])
                print("UPX Done=", fpath)
            except Exception as e:
                sys.stderr.write(str(e) + "\n")
    else:
        print("NO UPX")


    def zipdir(path, ziph):
        # ziph is zipfile handle
        for root, dirs, files in os.walk(path):
            for file_ in files:
                fpath = os.path.join(root, file_)
                arcname = fpath.replace(path, "")
                arcname = arcname.strip("\\")
                ziph.write(fpath, arcname=arcname)


    def do_dist_zip():
        zipfilepath = os.path.join(CUR_DIR, 'Kalibro-v%s.zip' % VERSION)
        if os.path.exists(zipfilepath):
            os.remove(zipfilepath)
        zipf = zipfile.ZipFile(zipfilepath, 'w', compression=zipfile.ZIP_DEFLATED)
        zipdir(DIST_DIR, zipf)
        zipf.close()

    do_dist_zip()

print("DONE")
