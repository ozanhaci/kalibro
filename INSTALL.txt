REQUIREMENTS FOR WINDOWS
  - Install C++ 2008 REDISTRIBUTABLE (if not exist)
  - Supported Operating Systems: Windows 7, 10
  - Tested on Windows 7 and 10


REQUIREMENTS FOR LINUX
  - Install Python 2.7 (already included in most Linux Distros)
  - Install wxPython 3.0.2 (apt-get install python-wxgtk3.0)
  - Install other modules (pip install -r requirements.txt)
  - User must have write permission in application directory
  - Tested on and Debian 8 (Jessie) GNU/Linux


HOW TO UPGRADE
  - If you use version 2.4 or older, you must upgrade to version 2.5 at first then to newer versions
  - Backup "kalibro.sqlite" database file located in application directory to another directory, just in case.
  - For zip version: Copy "kalibro.sqlite" file from old application directory to new application directory
  - For installer version: Run installer and then it extracts files into application's directory.


INSTALLATION - SOURCE CODE
  - Make sure you have wxPython installed and other Python requirements are installed
  - Extract the tar.gz file and run "kalibro.py" file (make kalibro.py file executable)
  OR
  $ git clone --recursive https://bitbucket.org/ozanhaci/kalibro.git
  $ cd kalibro
  $ chmod +x kalibro.py
  $ ./kalibro.py
  OR
  $ python kalibro.py


INSTALLATION - EXECUTABLE FILE FOR WINDOWS
  - Extract the zip file and run "Kalibro.exe" file


BUILDING EXE FROM SOURCE CODE (Windows)

  - Requirements:
      Python 2.7
      see requirements.txt file for required packages
  - UPX is optional to compress DLL files
    set UPX_PATH environment variable to path of upx.exe
  - Run "setup.py" in source code


SUPPORT
  - http://www.ozanh.com/kalibro
  - https://bitbucket.org/ozanhaci/kalibro
  - ozan_haci@yahoo.com
