# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import threading
import weakref
import traceback

import wx

from .debugging import DEBUG, log_debug, debug_trace

RLock = threading.RLock()

__all__ = ["subscribe", "unsubscribe", "unsubscribe_all", "publish"]


def subscribe(topic, func):
    return MyPubSub.subscribe(topic, func)


def unsubscribe(topic, func=None):
    return MyPubSub.unsubscribe(topic, func=None)


def unsubscribe_all():
    with RLock:
        topics = MyPubSub._registry.keys()
        for topic in topics:
            unsubscribe(topic, None)


def publish(topic, *args, **kwargs):
    return MyPubSub.publish(topic, *args, **kwargs)


class _weak_callable(object):

    def __init__(self, obj, func):
        self._obj = obj
        self._meth = func

    def __call__(self, *args, **kws):
        if self._obj is not None:
            return self._meth(self._obj, *args, **kws)
        else:
            return self._meth(*args, **kws)

    def __getattr__(self, attr):
        if attr in ('__self__', 'im_self'):
            return self._obj
        if attr in ('__func__', 'im_func'):
            return self._meth
        raise AttributeError, attr


class WeakMethod(object):
    """ Wraps a function or, more importantly, a bound method, in
    a way that allows a bound method's object to be GC'd, while
    providing the same interface as a normal weak reference. """

    def __init__(self, fn, callback=None):
        self._obj = weakref.ref(fn.__self__, callback)
        self._meth = fn.__func__

    def __call__(self):
        if self._dead():
            return None
        return _weak_callable(self._obj(), self._meth)

    def _dead(self):
        return self._obj is not None and self._obj() is None


class MyPubSub(object):
    """
    Simple pubsub implementation
    """

    _registry = {}

    @classmethod
    def subscribe(cls, topic, func):
        assert hasattr(func, "__call__")

        def clean(topic):
            def cleaner(dead):
                cls._clean(dead, topic)
            return cleaner

        with RLock:
            if topic not in cls._registry:
                cls._registry[topic] = []
            if not hasattr(func, "__self__"):
                wref = weakref.ref(func, clean(topic))
            else:
                wref = WeakMethod(func, clean(topic))
            if wref not in cls._registry[topic]:
                cls._registry[topic].append(wref)

    @classmethod
    def unsubscribe(cls, topic, func):
        with RLock:
            if topic not in cls._registry:
                return
            else:
                if func is not None:
                    wref = weakref.ref(func)
                    cls._registry[topic] = [i for i in cls._registry[topic] if i != wref]
                    if not cls._registry.get(topic):
                        cls._registry.pop(topic, None)
                else:
                    cls._registry.pop(topic, None)

    @classmethod
    def publish(cls, topic, *args, **kwargs):
        with RLock:
            if topic in cls._registry:
                removables = []
                for func in cls._registry[topic]:
                    try:
                        func()(*args, **kwargs)
                    except (TypeError,
                            weakref.ReferenceError,
                            wx.PyDeadObjectError):
                        removables.append(func)
                        continue
                    except:
                        if DEBUG:
                            log_debug("registry: %r" % cls._registry)
                            log_debug(debug_trace(), traceback.format_exc())
                        removables.append(func)
                        continue
                for rf in removables:
                    try:
                        cls._registry[topic].remove(rf)
                    except (IndexError, KeyError):
                        pass
                if not cls._registry.get(topic):
                    cls._registry.pop(topic, None)

    @classmethod
    def _clean(cls, dead=None, topic=None):
        with RLock:
            if DEBUG:
                log_debug(dead, "topic =", topic)
            if dead is None and topic is None:
                for topic in cls._registry.keys():
                    cls._registry[topic] = list(filter(lambda x: x() is not None,
                                                       cls._registry.get(topic, [])))
            else:
                cls._registry[topic] = [i for i in cls._registry.get(topic, []) if i is not dead]
            if DEBUG:
                log_debug(cls._registry)

