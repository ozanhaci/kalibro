# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import sys
import os
import sqlite3

import wx
import wx.lib.mixins.listctrl as listmix

from .conn_sqlite import MainSql, IntegrityError
from .auth import get_user, ModelAuth
from . import images


class ReportStore(listmix.ColumnSorterMixin, object):

    def __init__(self, parent, icon, recID=None, devNo=None, caller=None):
        # record ID and device number

        amodel = ModelAuth()
        username = get_user()
        installed = amodel.auth_installed()
        if installed:
            self.has_write_permission = amodel.has_write_perm(username)
        else:
            self.has_write_permission = True

        self.recID = recID
        self.devNo = devNo

        self.frame_width = -1
        self.frame_height = 500
        fTitle = "Kalibro " + _("Report Store - Save Your Reports Safely")
        self.frame = wx.Frame(parent, -1, title=fTitle,
                              style=wx.DEFAULT_FRAME_STYLE |
                              wx.FRAME_FLOAT_ON_PARENT,
                              size=(self.frame_width, self.frame_height))
        self.frame.SetIcon(icon)
        self.panel = wx.Panel(self.frame, -1)

        gbSizer = wx.GridBagSizer(4, 4)

        self.dropTextBox = wx.TextCtrl(self.panel,
                                       size=(200, 100),
                                       style=wx.TE_MULTILINE | wx.TE_READONLY |
                                       wx.TE_NO_VSCROLL | wx.RAISED_BORDER)
        self.dropTextBox.SetValue(_("Drop Your Report File To Here"))
        self.dropTextBox.SetBackgroundColour("#D4D0C8")
        self.dropTextBox.SetDropTarget(MyFileDropTarget(self.dropTextBox))

        self.fileSelectButton = wx.Button(self.panel, -1, label=_("Select File"))

        self.dropFileNameBox = wx.TextCtrl(self.panel,
                                           size=(100, -1),
                                           style=wx.TE_READONLY)
        self.dropFileNameBox.SetBackgroundColour("#D4D0C8")
        dropfilenameWH = self.dropFileNameBox.GetSizeTuple()
        self.reportNumber = wx.TextCtrl(self.panel, size=dropfilenameWH)
        # CALL to get report number
        self.getReportNumber()

        self.saveButton = wx.Button(self.panel, label=_("Save File to Database"))
        self.saveButton.Enable(self.has_write_permission)

        self.createReportList()

        self.img_list = wx.ImageList(16, 16)
        self.sm_up_bmp = self.img_list.Add(images.SmallUpArrow.GetBitmap())
        self.sm_dn_bmp = self.img_list.Add(images.SmallDnArrow.GetBitmap())
        self.listCtrlVar.SetImageList(self.img_list, wx.IMAGE_LIST_SMALL)

        refreshButton = wx.Button(self.panel, -1, label=_("Refresh"))
        self.getButton = wx.Button(self.panel, label=_("Save File to Disk"))
        self.getButton.Disable()
        self.deleteListButton = wx.Button(self.panel, -1, label=_("Delete"))
        self.deleteListButton.Disable()

        # EVENTS
        def getFileName(evt):
            tmp = self.dropTextBox.GetValue()
            if os.path.isfile(tmp):
                fname = os.path.basename(tmp)
                self.dropFileNameBox.SetValue(fname)
                self.save2Db(evt)

        def selectFile(evt):
            dlg = \
            wx.FileDialog(self.panel, message=_("Choose a file"),
                          defaultDir=os.getcwd(),
                          defaultFile="",
                          wildcard="*.*|*.*",
                          style=wx.OPEN)
            if dlg.ShowModal() == wx.ID_OK:
                path = dlg.GetPath()
                self.dropTextBox.SetValue(path)
            dlg.Destroy()

        self.panel.Bind(wx.EVT_BUTTON, selectFile, self.fileSelectButton)
        self.panel.Bind(wx.EVT_BUTTON, getFileName, self.saveButton)
        self.panel.Bind(wx.EVT_BUTTON, self.save2Disk, self.getButton)
        self.panel.Bind(wx.EVT_BUTTON, (lambda evt: self.onRefreshList(evt)),
                        refreshButton)
        self.panel.Bind(wx.EVT_TEXT, self.saveReportNumber, self.reportNumber)
        self.panel.Bind(wx.EVT_LIST_ITEM_SELECTED, self.onItemSelected,
                        self.listCtrlVar)
        self.panel.Bind(wx.EVT_LIST_ITEM_DESELECTED, self.onItemDeselected,
                        self.listCtrlVar)
        self.panel.Bind(wx.EVT_BUTTON, self.onDeleteItem, self.deleteListButton)

        self.frame.Bind(wx.EVT_CLOSE, (lambda evt: self.onExit(caller, evt)))

        if self.recID and self.devNo:
            label = wx.StaticText(self.panel, -1, _("Record ID: %s\nDevice No: %s") % (self.recID, self.devNo),
                                  style=wx.ALIGN_LEFT | wx.RAISED_BORDER)
            label.SetBackgroundColour(wx.WHITE)
            label.SetForegroundColour(wx.BLUE)

            gbSizer.Add(label, (0, 2), (2, 2), flag=wx.LEFT | wx.ALL | wx.GROW)

        gbSizer.Add(self.dropTextBox, (0, 0), (2, 2), flag=wx.ALL | wx.GROW)
        label = wx.StaticText(self.panel, label=_("OR Select File"),
                            style=wx.ALIGN_LEFT | wx.RAISED_BORDER)
        label.SetBackgroundColour(wx.WHITE)
        gbSizer.Add(label, (2, 0), (1, 2), flag=wx.GROW)
        gbSizer.Add(self.fileSelectButton, (3, 0), (1, 2), flag=wx.GROW)

        label = wx.StaticText(self.panel, -1, _("File Name"),
                              style=wx.ALIGN_LEFT | wx.RAISED_BORDER)
        label.SetBackgroundColour(wx.WHITE)
        gbSizer.Add(label, (2, 2), flag=wx.ALL | wx.GROW)

        label = wx.StaticText(self.panel, -1, _("Report No"),
                              style=wx.ALIGN_LEFT | wx.RAISED_BORDER)
        label.SetBackgroundColour(wx.WHITE)
        gbSizer.Add(label, (2, 3), flag=wx.ALL | wx.GROW)

        gbSizer.Add(self.dropFileNameBox, (3, 2), flag=wx.ALL | wx.GROW)
        gbSizer.Add(self.reportNumber, (3, 3), flag=wx.ALL | wx.GROW)
        gbSizer.Add(self.saveButton, (4, 2), (1, 2), flag=wx.ALL | wx.GROW)

        gbSizer.Add(self.listCtrlVar, (6, 0), (10, 5), flag=wx.ALL | wx.GROW)

        gbSizer.Add(refreshButton, (16, 0), flag=wx.ALL | wx.GROW)
        gbSizer.Add(self.getButton, (16, 1), flag=wx.ALL | wx.GROW)
        gbSizer.Add(self.deleteListButton, (16, 3), flag=wx.ALL | wx.GROW)

        sizerV = wx.BoxSizer(wx.VERTICAL)
        sizerV.Add(gbSizer, flag=wx.EXPAND | wx.ALL, border=5)

        self.panel.SetAutoLayout(True)
        self.panel.SetSizer(sizerV)
        sizerV.Fit(self.panel)

        self.frame.Fit()
        self.frame.Center()
        self.frame.Show(True)

        self.onRefreshList()

    def onExit(self, caller, evt=None):
        caller.clearGrid()
        caller.fetchSubData(self.devNo)
        self.frame.Destroy()

    def onRefreshList(self, evt=None):
        self.listCtrlVar.DeleteAllItems()
        self.getReportData()
        for k, v in self.reportList.items():
            index = self.listCtrlVar.InsertStringItem(sys.maxint, v[0])
            self.listCtrlVar.SetStringItem(index, 1, v[1])
            self.listCtrlVar.SetStringItem(index, 2, v[2])
            self.listCtrlVar.SetStringItem(index, 3, v[3])
            self.listCtrlVar.SetItemData(index, k)
        for i in range(self.listCtrlVar.GetColumnCount()):
            self.listCtrlVar.SetColumnWidth(i, wx.LIST_AUTOSIZE_USEHEADER)
        if evt:
            evt.Skip()

    def getReportData(self):
        query = "SELECT id,registryNo,reportNo,fileName FROM reportBlobData"
        myob = MainSql()
        result = myob.fetch_all(query).result
        self.reportList = {}
        result2 = []
        temp = []

        for j in xrange(len(result)):
            for k in xrange(len(result[j])):
                temp.append(unicode(result[j][k]))
            else:
                result2.append(temp)
                temp = []

        for i in xrange(len(result2)):
            self.reportList[i] = result2[i]

    def getReportNumber(self, evt=None):
        tail = {"id": self.recID, "regno": self.devNo}
        query = "SELECT trackingNo FROM subrecords WHERE id=:id AND registryNo=:regno LIMIT 1"
        myob = MainSql()
        result = myob.fetch_all(query, tail=tail).result
        if(len(result) > 0):
            repNo = result[0][0]
            self.reportNumber.SetValue(repNo)

    def saveReportNumber(self, evt=None):
        rnum = self.reportNumber.GetValue()
        tail = {"rnum": rnum, "id": self.recID, "devno": self.devNo}
        query = "UPDATE subrecords SET trackingNo=:rnum WHERE id=:id AND registryNo=:devno"
        myob = MainSql()
        myob.execStmt(query, tail=tail)
        evt.Skip()

    def createReportList(self, evt=None):
        self.getReportData()
        self.itemDataMap = self.reportList

        self.listCtrlVar = MyListCtrl(self.panel, -1,
                                      style=wx.LC_REPORT
                                      | wx.LC_SORT_ASCENDING
                                      | wx.LC_VRULES
                                      | wx.LC_HRULES
                                      | wx.LC_SINGLE_SEL
                                      )
        label = _("ID")
        self.listCtrlVar.InsertColumn(0, label)
        label = _("Registry No")
        self.listCtrlVar.InsertColumn(1, label)
        label = _("Report No")
        self.listCtrlVar.InsertColumn(2, label)
        label = _("File Name")
        self.listCtrlVar.InsertColumn(3, label)

        #self.onRefreshList()
        listmix.ColumnSorterMixin.__init__(self, 4)

    def GetListCtrl(self):
        return self.listCtrlVar

    def GetSortImages(self):
        return (self.sm_dn_bmp, self.sm_up_bmp)

    def onItemSelected(self, event):
        self.currentItem = event.m_itemIndex
        if not self.deleteListButton.Enabled:
            self.deleteListButton.Enable(self.has_write_permission)
            self.getButton.Enable(self.has_write_permission)
        event.Skip()

    def onDeleteItem(self, event):
        self.getButton.Disable()
        self.deleteListButton.Disable()
        dlg = wx.MessageDialog(self.panel,
                               _("Do you want to delete this report permanently?"),
                               _("Warning"),
                               wx.YES_NO | wx.NO_DEFAULT | wx.ICON_WARNING)
        if dlg.ShowModal() != wx.ID_YES:
            dlg.Destroy()
            return
        dlg.Destroy()
        recId = self.listCtrlVar.GetItemText(self.currentItem)
        regNo = self.getColumnText(self.currentItem, 1)
        devRepNo = self.getColumnText(self.currentItem, 2)
        query = "DELETE FROM reportBlobData WHERE id=? AND registryNo=? AND reportNo=?"
        tail = (recId, regNo, devRepNo)
        myob = MainSql()
        myob.execStmt(query, tail)
        self.onRefreshList()

    def onItemDeselected(self, event):
        if self.deleteListButton.Enabled:
            self.deleteListButton.Disable()
            self.getButton.Disable()
        event.Skip()

    def getColumnText(self, index, col):
        item = self.listCtrlVar.GetItem(index, col)
        return item.GetText()

    def save2Db(self, evt=None):
        if(self.reportNumber.GetValue() == ""):
            self.messageOK(msg=_("Report Number is missing"),
                           caption=_("Warning"))
            evt.Skip()
            return

        myob = MainSql()
        with open(self.dropTextBox.GetValue(), "rb") as bf:
            blobData = bf.read()
        queryInsert = """INSERT INTO reportBlobData
            (id,registryNo,reportNo,fileName,fileData)
            VALUES (:id,:registryNo,:reportNo,:fileName,:fileData)"""
        queryUpdate = """UPDATE reportBlobData SET reportNo=:reportNo,
            fileName=:fileName,fileData=:fileData WHERE id=:id AND
            registryNo=:registryNo"""
        tail = {"id": self.recID, "registryNo": self.devNo,
                "reportNo": self.reportNumber.GetValue(),
                "fileName": self.dropFileNameBox.GetValue(),
                "fileData": sqlite3.Binary(blobData)}
        try:
            myob.execStmt(queryInsert, tail)
        except IntegrityError:
            dlg = wx.MessageDialog(self.panel,
                                   _("File has already been uploaded for this record.\n"
                                     "Do you want to replace the existing file with this one?"),
                                   _("Warning"),
                                    wx.YES_NO | wx.ICON_WARNING)
            if dlg.ShowModal() == wx.ID_YES:
                myob.execStmt(queryUpdate, tail)
            dlg.Destroy()
        self.onRefreshList()
        evt.Skip()

    def save2Disk(self, evt=None):
        reportNum = self.reportNumber.GetValue()
        if(reportNum == ""):
            self.messageOK(msg=_("Report Number is missing"),
                           caption=_("Warning"))
            evt.Skip()
            return

        recId = self.listCtrlVar.GetItemText(self.currentItem)
        regNo = self.getColumnText(self.currentItem, 1)
        tail = (recId, regNo)
        query = ("SELECT * FROM reportBlobData"
                 " WHERE id=? AND registryNo=? LIMIT 1")
        myob = MainSql()
        result = myob.fetch_all(query, tail=tail).result
        if(len(result) == 0):
            evt.Skip()
            return
        filename = result[0][3]

        dlg = wx.FileDialog(
            self.panel, message=_("Save"), defaultDir=os.getcwd(),
            defaultFile=filename, wildcard=_("All Files(*.*)|*.*"),
            style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT
            )

        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            file = open(path, "wb")
            # 4 is the column of blob data (0..4)
            file.write(result[0][4])
            file.close()

        dlg.Destroy()
        evt.Skip()

    def messageOK(self, msg, caption, evt=None):
        wx.MessageBox(msg, caption, style=wx.OK)


class MyFileDropTarget(wx.FileDropTarget, object):

    def __init__(self, obj):
        wx.FileDropTarget.__init__(self)
        self.obj = obj

    def OnDropFiles(self, x, y, filenames):
        self.obj.Clear()
        self.obj.SetInsertionPointEnd()
        if(os.path.isfile(filenames[0])):
            self.obj.ChangeValue(filenames[0])


class MyListCtrl(wx.ListCtrl, listmix.ListCtrlAutoWidthMixin, object):

    def __init__(self, parent, ID, pos=wx.DefaultPosition,
                 size=wx.DefaultSize, style=0):
        wx.ListCtrl.__init__(self, parent, ID, pos, size, style)
        listmix.ListCtrlAutoWidthMixin.__init__(self)

