# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import re
import datetime
import threading
from collections import OrderedDict

import wx
import wx.html as html
import wx.lib.mixins.listctrl as listmix

from .conn_sqlite import MainSql
from .debugging import debug_decorator
from .utils import coltr, str_result, get_icon
from .reporting import reporting
from .config import ConfigManager
from . import pubsub
from . import images

str = unicode

SI_TYPE_STRING = "string"
SI_TYPE_INTEGER = "int"
SI_TYPE_FLOAT = "float"
SI_TYPE_DATE = "date"

SI_PYVAL_STRING_FUNC = lambda x: x
SI_PYVAL_INTEGER_FUNC = lambda x: int(x)
SI_PYVAL_FLOAT_FUNC = lambda x: float(x)

SR_VAL_FLOAT_FUNC = SR_VAL_INTEGER_FUNC = \
    lambda x: str(x) if x is not None else None
SR_VAL_STRING_FUNC = lambda x: x


def conv_date_str(dobj, dtformat):
    if isinstance(dobj, (datetime.date, datetime.datetime)):
        return dobj.strftime(dtformat)
    return str(dobj) if dobj is not None else ""

SR_VAL_DATE_FUNC = conv_date_str


def conv_date_py(inputstr, dtformat):
    return datetime.datetime.strptime(inputstr, dtformat).date()

SI_PYVAL_DATETIME_FUNC = conv_date_py

ID_MENU_SEARCH = wx.NewId()
ID_MENU_CLEAR_FORM = wx.NewId()
ID_MENU_CLEAR_LIST = wx.NewId()
ID_MENU_REPORTING = wx.NewId()

PLACEHOLDER_COUNTER = 0
RLOCK = threading.RLock()

@debug_decorator
def build_qry_where(val, columnname, pholdmemory=None):
    if val == "!":
        return (" ifnull(\"%s\",'')='' " % columnname), {}
    global PLACEHOLDER_COUNTER
    with RLOCK:
        PLACEHOLDER_COUNTER += 1
    assert isinstance(val, basestring)
    assert isinstance(columnname, basestring)
    if pholdmemory is None:
        pholdmemory = []
    assert isinstance(pholdmemory, list)
    placeholder = re.sub("\W", "_", columnname)
    if placeholder not in pholdmemory:
        # placeholder = columnname
        pass
    else:
        placeholder = placeholder + "_" + str(PLACEHOLDER_COUNTER)

    if placeholder:
        pholdmemory.append(placeholder)
    val = val.strip()

    semisplit = re.split(r"(?<!\\)(;)", val, re.I | re.U)
    if len(semisplit) > 1:
        semisplit = [i for i in semisplit if i != ";"]
        if len(semisplit) == 1:
            semisplit.append("!")
        if len(semisplit) > 1:
            or_conds = []
            or_tails = {}
            for item in semisplit:
                clause, tail = build_qry_where(item, columnname, pholdmemory)
                if clause is not None:
                    or_conds.append(clause)
                    or_tails.update(tail)
            if or_conds:
                return "(" + "OR".join(or_conds) + ")", or_tails

    match = re.match(r"\s*(?P<first>.*?)\s*\.\.\s*(?P<second>.*)\s*",
                     val, re.I | re.U)
    if(match and "first" in match.groupdict() and
       "second" in match.groupdict()):
        tdict = {}
        for k in match.groupdict():
            tdict[placeholder + "_" + k] = \
                match.groupdict()[k]
        return (" {colname} BETWEEN :{placeholder}_first AND "
                ":{placeholder}_second ".\
                format(colname=columnname,
                       placeholder=placeholder)), tdict
    else:
        cond_fmt = " {colname} {op} :{placeholder} "
        if val.startswith("<=") or val.startswith(">="):
            op, val = val[0:2], val[2:].strip()
            tdict = {placeholder: val}
            return (cond_fmt.format(colname=columnname, op=op,
                                    placeholder=placeholder)), tdict
        elif val.startswith("!="):
            op, val = val[0:2], val[2:].strip()
            tdict = {placeholder: val}
            return (cond_fmt.format(colname=columnname, op=op,
                                    placeholder=placeholder)), tdict
        elif val.startswith("<") or val.startswith(">"):
            op, val = val[0], val[1:].strip()
            tdict = {placeholder: val}
            return (cond_fmt.format(colname=columnname, op=op,
                                    placeholder=placeholder)), tdict
        elif val.startswith("="):
            op, val = val[0], val[1:].strip()
            likematch = re.search(r"(?<!\\)(%)", val, re.I | re.U)
            if likematch:
                op = "LIKE"
            tdict = {placeholder: val}
            return (cond_fmt.format(colname=columnname, op=op,
                                    placeholder=placeholder)), tdict
        elif val:
            op = "LIKE"
            if "%" not in val:
                val = "%" + val + "%"
            tdict = {placeholder: val}
            return (cond_fmt.format(colname=columnname, op=op,
                                    placeholder=placeholder)), tdict
    return None, None


class SearchListCtrl(wx.ListCtrl, listmix.ListCtrlAutoWidthMixin):

    def __init__(self, parent, size=wx.DefaultSize, style=0):
        wx.ListCtrl.__init__(self, parent, size=size, style=style)
        listmix.ListCtrlAutoWidthMixin.__init__(self)


class SearchModel(object):

    def __init__(self, db):
        self.db = db

    def get_dev_statuses(self):
        qry = '''SELECT name FROM status ORDER BY name ASC'''
        return self.db.fetch_all(qry).result

    def get_owners(self):
        qry = '''SELECT name FROM owner ORDER BY name ASC'''
        return self.db.fetch_all(qry).result

    def get_cal_results(self):
        qry = '''SELECT name FROM result ORDER BY name ASC'''
        return self.db.fetch_all(qry).result

    def get_add_field_labels(self):
        return self.db.fetch_all('''SELECT IntLabel,TextLabel1,TextLabel2,DateLabel
            FROM mainAddFieldLabel LIMIT 1''').result

    def get_report_data(self, numregnos, regnos):
        qry = 'SELECT * FROM main WHERE registryNo IN (%s)' % \
            ",".join(["?"] * numregnos)
        return self.db.fetch_all(qry, regnos).result

    def get_all(self, labelDict, where, tail):
        query = '''SELECT main.*,subrecords.plannedDate,
        subrecords.realizedDate,subrecords.trackingNo,
        subrecords.method, subrecords.result, subrecords.path,
        mainAddField.IntValue as "%(IntLabel)s",
        mainAddField.TextValue1 as "%(TextLabel1)s",
        mainAddField.TextValue2 as "%(TextLabel2)s",
        mainAddField.DateValue as "%(DateLabel)s"
        FROM main LEFT JOIN subrecords ON
        main.registryNo=subrecords.registryNo
        LEFT JOIN mainAddField ON
        main.registryNo=mainAddField.registryNo''' % labelDict
        if where:
            query = query + " WHERE " + " AND ".join(where)
        query += " order by main.registryNo asc, subrecords.id asc"
        return (self.db.fetch_all(query, tail).result, self.db.getColNames())


class SearchManager(object):

    topic_update_dev_status = "update_dev_status"
    topic_update_owner = "update_owner"

    def __init__(self, view, model_cls, db_cls):
        self.view = view
        self.model_cls = model_cls
        self.model = model_cls(db=db_cls())
        self.add_field_labels = {}

    def init(self):
        self.populate_dev_status()
        self.populate_owner()
        self.populate_cal_result()
        self.subscribes()

    def subscribes(self):
        pubsub.subscribe(self.topic_update_dev_status, self.populate_dev_status)
        pubsub.subscribe(self.topic_update_owner, self.populate_owner)

    def populate_dev_status(self):
        wid = self.view.search_items["status"]["widget"]
        wid.Clear()
        result = self.model.get_dev_statuses()
        items = [""] + [i[0] for i in result if i[0]]
        wid.AppendItems(items)

    def populate_owner(self):
        wid = self.view.search_items["owner"]["widget"]
        wid.Clear()
        result = self.model.get_owners()
        items = [""] + [i[0] for i in result if i[0]]
        wid.AppendItems(items)

    def populate_cal_result(self):
        wid = self.view.search_items["result"]["widget"]
        wid.Clear()
        result = self.model.get_cal_results()
        items = [""] + [i[0] for i in result if i[0]]
        wid.AppendItems(items)

    def get_add_field_labels(self):
        result = self.model.get_add_field_labels()
        if result:
            self.add_field_labels["IntLabel"] = result[0][0]
            self.add_field_labels["TextLabel1"] = result[0][1]
            self.add_field_labels["TextLabel2"] = result[0][2]
            self.add_field_labels["DateLabel"] = result[0][3]

    def on_search_select(self, regno):
        if not regno:
            return
        pubsub.publish("search_select", regno)

    def on_clear_search_result(self):
        self.view.listResult.DeleteAllItems()

    def on_clear_form(self):
        first = True
        for k, v in self.view.search_items.iteritems():
            if first and hasattr(v["widget"], "SetFocus"):
                v["widget"].SetFocus()
                first = False
            if hasattr(v["widget"], "Clear"):
                if isinstance(v["widget"], wx.ComboBox):
                    v["widget"].SetSelection(0)
                else:
                    v["widget"].Clear()

    def on_reporting(self):
        numrows = self.view.listResult.GetItemCount()
        if numrows <= 0:
            return
        regnos = set()
        for r in range(numrows):
            regno = self.view.listResult.GetItemText(r)
            regnos.add(regno)
        regnos = list(regnos)
        numregnos = len(regnos)
        result = self.model.get_report_data(numregnos, regnos)
        result = str_result(result, self.view.dateformat)
        config = ConfigManager()
        reporting(parent=self.view, size=(400, 400), select_data=result,
                  dateformat=self.view.dateformat, language=config["language"],
                  file_encoding=config["file_encoding"])

    def on_search(self):
        self.view.listResult.DeleteAllItems()
        result = self.model.get_add_field_labels()
        labelDict = {}
        if result and len(result) > 0:
            labelDict["IntLabel"] = result[0][0]
            labelDict["TextLabel1"] = result[0][1]
            labelDict["TextLabel2"] = result[0][2]
            labelDict["DateLabel"] = result[0][3]

        where = []
        tail = {}
        error_msg_value_error = _("Search field value error")
        for k, v in self.view.search_items.iteritems():
            if isinstance(v["widget"], wx.TextCtrl):
                val = v["widget"].GetValue().strip()
            elif isinstance(v["widget"], wx.ComboBox):
                val = v["widget"].GetStringSelection()
                if val:
                    val = "=" + val
            columnname = v.get("column", k)
            if not val:
                continue
            if v["type"] == SI_TYPE_STRING:
                whclause, tailnew = build_qry_where(val, columnname)
                if whclause is not None:
                    where.append(whclause)
                    tail.update(tailnew)
                else:
                    self.view.messagebox(error_msg_value_error,
                                         _("Error"), wx.ICON_ERROR)
                    return
            elif v["type"] == SI_TYPE_INTEGER or v["type"] == SI_TYPE_FLOAT:
                whclause, tailnew = build_qry_where(val, columnname)
                try:
                    if whclause is not None:
                        where.append(whclause)
                        tailnew = {xk: v["pyval"](xy)
                                   for xk, xy in tailnew.items()}
                        tail.update(tailnew)
                    else:
                        self.view.messagebox(error_msg_value_error,
                                             _("Error"), wx.ICON_ERROR)
                        return
                except Exception as e:
                    self.view.messagebox(str(e), _("Error"), wx.ICON_ERROR)
                    return
            elif v["type"] == SI_TYPE_DATE:
                whclause, tailnew = build_qry_where(val, columnname)
                try:
                    if whclause is not None:
                        where.append(whclause)
                        tailnew = {xk: v["pyval"](xy)
                                   for xk, xy in tailnew.items()}
                        tail.update(tailnew)
                    else:
                        self.view.messagebox(error_msg_value_error,
                                             _("Error"), wx.ICON_ERROR)
                        return
                except Exception as e:
                    self.view.messagebox(str(e), _("Error"), wx.ICON_ERROR)
                    return

        result, columns = self.model.get_all(labelDict, where, tail)
        searchlist = list(self.view.search_items.keys())
        row = 0
        self.view.itemDataMap = []  # for column sort
        labelDict_values = list(labelDict.values())
        labelDict_items = list(labelDict.items())
        for rec in result:
            itemData = []
            for idx, colval in enumerate(rec):
                colname = columns[idx]
                try:
                    listidx = searchlist.index(colname)
                except ValueError:
                    continue
                if listidx == 0:
                    colval = self.view.search_items[colname]["strval"](colval)
                    colval = colval if colval is not None else ""
                    self.view.listResult.InsertStringItem(row, colval)
                    self.view.listResult.SetItemData(row, row)
                    itemData.append(colval)
                    break
            for idx, colval in enumerate(rec):
                colname = columns[idx]
                if colname in labelDict_values:
                    for lb, lbv in labelDict_items:
                        if lbv == colname:
                            colname = lb
                            break
                try:
                    listidx = searchlist.index(colname)
                except ValueError:
                    continue
                if listidx != 0:
                    colval = self.view.search_items[colname]["strval"](colval)
                    colval = colval if colval is not None else ""
                    self.view.listResult.SetStringItem(row, listidx, colval)
                    itemData.append(colval)
            row += 1
            self.view.itemDataMap.append(itemData)
        #print(self.view.itemDataMap)
        for i in range(self.view.listResult.GetColumnCount()):
            self.view.listResult.SetColumnWidth(i, wx.LIST_AUTOSIZE_USEHEADER)


class MainSearchWin(wx.Frame, listmix.ColumnSorterMixin):

    def __init__(self, parent=None, manager_cls=SearchManager,
                 model_cls=SearchModel, db_cls=MainSql):
        wx.Frame.__init__(self, parent, size=(1000, 750))
        self.init()
        self.SetTitle(_("Search"))
        self.SetIcon(get_icon())
        self.dateformat = ConfigManager()["date_format"]
        self.panel = wx.Panel(self)
        self.gbsizer = wx.GridBagSizer(10, 10)

        self.manager = manager_cls(view=self, model_cls=model_cls,
                                   db_cls=db_cls)
        self.search_items = OrderedDict()
        self.create_widgets()
        self.create_events()
        self.manager.init()

        self.panel.SetSizerAndFit(self.gbsizer)
        self.CenterOnParent()
        self.Show()

    def create_widgets(self):
        menubar = wx.MenuBar()
        menu_ops = wx.Menu()
        menu_ops.Append(ID_MENU_SEARCH, _("&Search") + "\tCtrl+S")
        menu_ops.Append(ID_MENU_CLEAR_FORM, _("Clear &Form") + "\tCtrl+Shift+F")
        menu_ops.Append(ID_MENU_CLEAR_LIST, _("Clear &List") + "\tCtrl+Shift+L")
        menu_ops.Append(ID_MENU_REPORTING, _("&Reporting") + "\tCtrl+Shift+R")
        menubar.Append(menu_ops, _("&Operations"))
        self.SetMenuBar(menubar)

        panel = self.panel

        bmp = wx.ArtProvider.GetBitmap(wx.ART_FIND, wx.ART_OTHER, (32, 32))
        self.btnSearch = wx.Button(panel, label="Search")
        self.btnSearch.SetBitmap(bmp)
        self.gbsizer.Add(self.btnSearch, (0, 0), (2, 1),
                         flag=wx.ALIGN_LEFT | wx.ALL | wx.GROW)

        bmp = wx.ArtProvider.GetBitmap(wx.ART_INFORMATION,
                                       wx.ART_OTHER, (32, 32))
        self.btnHelp = wx.Button(panel, label=_("Help"))
        self.btnHelp.SetBitmap(bmp)
        self.gbsizer.Add(self.btnHelp, (2, 0), (2, 1), flag=wx.ALL | wx.GROW)

        self.btnClearForm = wx.Button(panel, label=_("Clear Form"))
        self.gbsizer.Add(self.btnClearForm, (4, 0), flag=wx.ALL | wx.GROW)

        self.btnClearResult = wx.Button(panel, label=_("Clear List"))
        self.gbsizer.Add(self.btnClearResult, (5, 0), flag=wx.ALL | wx.GROW)

        self.btnReporting = wx.Button(panel, label=_("Reporting"))
        self.btnReporting.SetToolTipString(_("Results will be transferred to the reporting"))
        self.gbsizer.Add(self.btnReporting, (6, 0), flag=wx.ALL | wx.GROW)

        label = wx.StaticText(panel, label=coltr("registryNo", "main"))
        self.gbsizer.Add(label, (1, 1), flag=wx.ALIGN_RIGHT)
        wid = wx.TextCtrl(panel, size=(150, -1))
        self.search_items["registryNo"] = \
            {"widget": wid,
             "type": SI_TYPE_STRING,
             "pyval": SI_PYVAL_STRING_FUNC,
             "strval": SR_VAL_STRING_FUNC,
             "tr": label.Label,
             "column":"main.registryNo"}
        self.gbsizer.Add(wid, (1, 2))

        label = wx.StaticText(panel, label=coltr("deviceName", "main"))
        self.gbsizer.Add(label, (2, 1), flag=wx.ALIGN_RIGHT)
        wid = wx.TextCtrl(panel, size=(150, -1))
        self.search_items["deviceName"] = {"widget": wid,
                                           "type": SI_TYPE_STRING,
                                           "pyval": SI_PYVAL_STRING_FUNC,
                                           "strval": SR_VAL_STRING_FUNC,
                                           "tr": label.Label}
        self.gbsizer.Add(wid, (2, 2))

        label = wx.StaticText(panel, label=coltr("deviceManuf", "main"))
        self.gbsizer.Add(label, (3, 1), flag=wx.ALIGN_RIGHT)
        wid = wx.TextCtrl(panel, size=(150, -1))
        self.search_items["deviceManuf"] = {"widget": wid,
                                            "type": SI_TYPE_STRING,
                                            "pyval": SI_PYVAL_STRING_FUNC,
                                            "strval": SR_VAL_STRING_FUNC,
                                            "tr": label.Label}
        self.gbsizer.Add(wid, (3, 2))

        label = wx.StaticText(panel, label=coltr("manufSerialNo", "main"))
        self.gbsizer.Add(label, (4, 1), flag=wx.ALIGN_RIGHT)
        wid = wx.TextCtrl(panel, size=(150, -1))
        self.search_items["manufSerialNo"] = \
            {"widget": wid, "type": SI_TYPE_STRING,
             "pyval": SI_PYVAL_STRING_FUNC,
             "strval": SR_VAL_STRING_FUNC,
             "tr": label.Label}
        self.gbsizer.Add(wid, (4, 2))

        label = wx.StaticText(panel, label=coltr("features", "main"))
        self.gbsizer.Add(label, (5, 1), flag=wx.ALIGN_RIGHT)
        wid = wx.TextCtrl(panel, size=(150, -1))
        self.search_items["features"] = {"widget": wid, "type": SI_TYPE_STRING,
                                         "pyval": SI_PYVAL_STRING_FUNC,
                                         "strval": SR_VAL_STRING_FUNC,
                                         "tr": label.Label}
        self.gbsizer.Add(wid, (5, 2))

        label = wx.StaticText(panel, label=coltr("status", "main"))
        self.gbsizer.Add(label, (6, 1), flag=wx.ALIGN_RIGHT)
        wid = wx.ComboBox(panel, size=(150, -1),
                          style=wx.CB_DROPDOWN | wx.CB_READONLY | wx.CB_SORT)
        self.search_items["status"] = {"widget": wid,
                                       "type": SI_TYPE_STRING,
                                       "pyval": SI_PYVAL_STRING_FUNC,
                                       "strval": SR_VAL_STRING_FUNC,
                                       "tr": label.Label}
        self.gbsizer.Add(wid, (6, 2))

        label = wx.StaticText(panel, label=coltr("owner", "main"))
        self.gbsizer.Add(label, (7, 1), flag=wx.ALIGN_RIGHT)
        wid = wx.ComboBox(panel, size=(150, -1),
                          style=wx.CB_DROPDOWN | wx.CB_READONLY | wx.CB_SORT)
        self.search_items["owner"] = {"widget": wid,
                                      "type": SI_TYPE_STRING,
                                      "pyval": SI_PYVAL_STRING_FUNC,
                                      "strval": SR_VAL_STRING_FUNC,
                                      "tr": label.Label}
        self.gbsizer.Add(wid, (7, 2))

        label = wx.StaticText(panel, label=coltr("comingDate", "main"))
        self.gbsizer.Add(label, (8, 1), flag=wx.ALIGN_RIGHT)
        wid = wx.TextCtrl(panel, size=(150, -1))
        self.search_items["comingDate"] = \
            {"widget": wid, "type": SI_TYPE_DATE,
             "pyval": lambda x: SI_PYVAL_DATETIME_FUNC(x, self.dateformat),
             "strval": lambda x: SR_VAL_DATE_FUNC(x, self.dateformat),
             "tr": label.Label}
        self.gbsizer.Add(wid, (8, 2))

        label = wx.StaticText(panel, label=coltr("description", "main"))
        self.gbsizer.Add(label, (9, 1), flag=wx.ALIGN_RIGHT)
        wid = wx.TextCtrl(panel, size=(150, -1))
        self.search_items["description"] = \
            {"widget": wid, "type": SI_TYPE_STRING,
             "pyval": SI_PYVAL_STRING_FUNC,
             "strval": SR_VAL_STRING_FUNC,
             "tr": label.Label}
        self.gbsizer.Add(wid, (9, 2))

        label = wx.StaticText(panel, label=coltr("plannedDate", "subrecords"))
        self.gbsizer.Add(label, (1, 3), flag=wx.ALIGN_RIGHT)
        wid = wx.TextCtrl(panel, size=(150, -1))
        self.search_items["plannedDate"] = \
            {"widget": wid,
             "type": SI_TYPE_DATE,
             "pyval": lambda x: SI_PYVAL_DATETIME_FUNC(x, self.dateformat),
             "strval": lambda x: SR_VAL_DATE_FUNC(x, self.dateformat),
             "tr": label.Label}
        self.gbsizer.Add(wid, (1, 4))

        label = wx.StaticText(panel, label=coltr("realizedDate", "subrecords"))
        self.gbsizer.Add(label, (2, 3), flag=wx.ALIGN_RIGHT)
        wid = wx.TextCtrl(panel, size=(150, -1))
        self.search_items["realizedDate"] = \
            {"widget": wid, "type": SI_TYPE_DATE,
             "pyval": lambda x: SI_PYVAL_DATETIME_FUNC(x, self.dateformat),
             "strval": lambda x: SR_VAL_DATE_FUNC(x, self.dateformat),
             "tr": label.Label}
        self.gbsizer.Add(wid, (2, 4))

        label = wx.StaticText(panel, label=coltr("trackingNo", "subrecords"))
        self.gbsizer.Add(label, (3, 3), flag=wx.ALIGN_RIGHT)
        wid = wx.TextCtrl(panel, size=(150, -1))
        self.search_items["trackingNo"] = {"widget": wid,
                                           "type": SI_TYPE_STRING,
                                           "pyval": SI_PYVAL_STRING_FUNC,
                                           "strval": SR_VAL_STRING_FUNC,
                                           "tr": label.Label}
        self.gbsizer.Add(wid, (3, 4))

        label = wx.StaticText(panel, label=coltr("method", "subrecords"))
        self.gbsizer.Add(label, (4, 3), flag=wx.ALIGN_RIGHT)
        wid = wx.TextCtrl(panel, size=(150, -1))
        self.search_items["method"] = {"widget": wid, "type": SI_TYPE_STRING,
                                       "pyval": SI_PYVAL_STRING_FUNC,
                                       "strval": SR_VAL_STRING_FUNC,
                                       "tr": label.Label}
        self.gbsizer.Add(wid, (4, 4))

        label = wx.StaticText(panel, label=coltr("result", "subrecords"))
        self.gbsizer.Add(label, (5, 3), flag=wx.ALIGN_RIGHT)
        wid = wx.ComboBox(panel, size=(150, -1),
                          style=wx.CB_DROPDOWN | wx.CB_READONLY | wx.CB_SORT)
        self.search_items["result"] = {"widget": wid, "type": SI_TYPE_STRING,
                                       "pyval": SI_PYVAL_STRING_FUNC,
                                       "strval": SR_VAL_STRING_FUNC,
                                       "tr": label.Label}
        self.gbsizer.Add(wid, (5, 4))

        label = wx.StaticText(panel, label=coltr("path", "subrecords"))
        self.gbsizer.Add(label, (6, 3), flag=wx.ALIGN_RIGHT)
        wid = wx.TextCtrl(panel, size=(150, -1))
        self.search_items['path'] = {"widget": wid, "type": SI_TYPE_STRING,
                                     "pyval": SI_PYVAL_STRING_FUNC,
                                     "strval": SR_VAL_STRING_FUNC,
                                     "tr": label.Label}
        self.gbsizer.Add(wid, (6, 4))

        # fetch the label names
        self.manager.get_add_field_labels()

        label = wx.StaticText(panel,
                              label=self.manager.add_field_labels.get("IntLabel",
                                                                "<IntLabel>"))
        self.gbsizer.Add(label, (1, 5), flag=wx.ALIGN_RIGHT)
        self.gbsizer.Add(wx.StaticText(panel, -1, _("Additional Fields")), (0, 6))
        wid = wx.TextCtrl(panel, size=(150, -1))
        self.search_items['IntLabel'] = {"widget": wid, "type": SI_TYPE_INTEGER,
                                         "pyval": SI_PYVAL_INTEGER_FUNC,
                                         "strval": SR_VAL_INTEGER_FUNC,
                                         "tr": label.Label}
        self.gbsizer.Add(wid, (1, 6))

        label = wx.StaticText(panel,
                              label=self.manager.add_field_labels.get("TextLabel1",
                                                              "<TextLabel1>"))
        self.gbsizer.Add(label, (2, 5), flag=wx.ALIGN_RIGHT)
        wid = wx.TextCtrl(panel, size=(150, -1))
        self.search_items['TextLabel1'] = {"widget": wid, "type": SI_TYPE_STRING,
                                           "pyval": SI_PYVAL_STRING_FUNC,
                                           "strval": SR_VAL_STRING_FUNC,
                                           "tr": label.Label}
        self.gbsizer.Add(wid, (2, 6))

        label = wx.StaticText(panel,
                              label=self.manager.add_field_labels.get("TextLabel2",
                                                            "<TextLabel2>"))
        self.gbsizer.Add(label, (3, 5), flag=wx.ALIGN_RIGHT)
        wid = wx.TextCtrl(panel, size=(150, -1))
        self.search_items['TextLabel2'] = {"widget": wid, "type": SI_TYPE_STRING,
                                           "pyval": SI_PYVAL_STRING_FUNC,
                                           "strval": SR_VAL_STRING_FUNC,
                                           "tr": label.Label}
        self.gbsizer.Add(wid, (3, 6))

        label = wx.StaticText(panel,
                              label=self.manager.add_field_labels.get("DateLabel",
                                                            "<DateLabel>"))
        self.gbsizer.Add(label, (4, 5), flag=wx.ALIGN_RIGHT)
        wid = wx.TextCtrl(panel, size=(150, -1))
        self.search_items['DateLabel'] = \
            {"widget": wid, "type": SI_TYPE_DATE,
             "pyval": lambda x: SI_PYVAL_DATETIME_FUNC(x, self.dateformat),
             "strval": lambda x: SR_VAL_DATE_FUNC(x, self.dateformat),
             "tr": label.Label}
        self.gbsizer.Add(wid, (4, 6))

        self.listResult = SearchListCtrl(panel,
                                         size=(1, -1),
                                         style=wx.LC_REPORT |
                                         wx.BORDER_NONE |
                                         wx.LC_SINGLE_SEL |
                                         wx.LC_VRULES |
                                         wx.LC_HRULES)

        for i, k in enumerate(self.search_items):
            self.listResult.InsertColumn(i, self.search_items[k]["tr"])

        self.gbsizer.Add(self.listResult, (10, 0), (10, 7),
                         flag=wx.ALL | wx.GROW)

        listmix.ColumnSorterMixin.__init__(self, len(self.search_items))
        self.img_list = wx.ImageList(16, 16)
        self.img_down = self.img_list.Add(images.SmallDnArrow.GetBitmap())
        self.img_up = self.img_list.Add(images.SmallUpArrow.GetBitmap())
        self.listResult.SetImageList(self.img_list, wx.IMAGE_LIST_SMALL)

        self.gbsizer.AddGrowableCol(6)

    def GetListCtrl(self):
        return self.listResult

    def GetSortImages(self):
        return (self.img_down, self.img_up)

    def create_events(self):
        self.btnSearch.Bind(wx.EVT_BUTTON, self.on_search)
        self.btnClearForm.Bind(wx.EVT_BUTTON, self.on_clear_form)
        self.btnClearResult.Bind(wx.EVT_BUTTON, self.on_clear_search_result)
        self.btnHelp.Bind(wx.EVT_BUTTON, self.on_help)
        self.listResult.Bind(wx.EVT_LIST_ITEM_SELECTED, self.on_search_select)
        self.btnReporting.Bind(wx.EVT_BUTTON, self.on_reporting)
        self.Bind(wx.EVT_MENU, self.on_search, id=ID_MENU_SEARCH)
        self.Bind(wx.EVT_MENU, self.on_clear_form, id=ID_MENU_CLEAR_FORM)
        self.Bind(wx.EVT_MENU, self.on_clear_search_result,
                  id=ID_MENU_CLEAR_LIST)
        self.Bind(wx.EVT_MENU, self.on_reporting, id=ID_MENU_REPORTING)

    def on_reporting(self, evt):
        self.manager.on_reporting()
        evt.Skip()

    def on_clear_form(self, evt):
        self.manager.on_clear_form()
        evt.Skip()

    def on_clear_search_result(self, evt):
        self.manager.on_clear_search_result()
        evt.Skip()

    def on_search_select(self, evt):
        regno = evt.GetText()
        self.manager.on_search_select(regno)
        evt.Skip()

    def on_help(self, evt=None):
        size = list(self.GetSizeTuple())
        size[0] -= 100
        size[1] -= 100
        self.help_frame = wx.Frame(self, size=size)
        self.help_frame.SetIcon(get_icon())
        self.help_win = HelpWindow(self.help_frame)
        self.help_win.SetRelatedFrame(self.help_frame, "%s")
        self.help_win.SetRelatedStatusBar(0)
        self.help_win.SetPage(self.html_help_source)
        self.help_frame.Center()
        self.help_frame.Show()

    def on_search(self, evt=None):
        self.manager.on_search()
        if evt:
            evt.Skip()

    def messagebox(self, msg, caption, flag=wx.OK):
        wx.MessageBox(msg, caption, flag)

    def init(self):
        self.html_help_source = get_html_help()


def get_html_help():
    return '''
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
   <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">
<TITLE>Search Help</TITLE>
</HEAD>
<BODY>
<center>
<h2>{heading}</h2>
</center>
<p>{description}</p>
<ol>
    <li>{equal}</li>
    <li>{percent}</li>
    <li>{semicolon}</li>
    <li>{exclamation}</li>
    <li>{lessthan}</li>
    <li>{greaterthan}</li>
    <li>{lessequal}</li>
    <li>{greaterequal}</li>
    <li>{dotdot}</li>
</ol>
</BODY>
</HTML>
'''.\
format(heading=_("Search Help"),
       description=_("You can enter any input into the fields "
                     "and press Search Button. Then Kalibro searches your "
                     "term in the shown fields and does case-insensitive search."
                     "If you do not use any special characters, "
                     "your search term is searched "
                     "whether the field contains the term and "
                     "fetches the result(s) in the list below the form."
                     "If you want to do advanced search "
                     "you should use special characters "
                     "which are <b>= % ; ! &lt; &gt; &lt;= &gt;= .. _</b>"),
       equal=_("<b>=</b> equal-sign  <i>If you put = before your search term, "
               "it means exact match the term</i>"),
       percent=_("<b>%</b> percent-sign  <i>If you put % before/after/in "
                 "your search term, it means zero/one/more characters "
                 "in that position. <b>Example 1:</b> %def matches def or "
                 "cdef or bcdef, etc. <b>Example 2:</b> abc% matches "
                 "abc, abcd, abcdef, etc. <b>Example 3:</b>a%def matches "
                 "adef, abdef, abcdef, etc.</i>"),
       semicolon=_("<b>;</b> semicolon  <i>Put semicolon between search "
                   "terms in the same field to search with OR operator. "
                   "<b>Example</b>: devicename1;devicename2 -&gt; fetches "
                   "both devicename1 and devicename2</i>"),
       exclamation=_("<b>!</b> exclamation  <i>Put exclamation mark "
                     "if you want the results having that field is empty<i>"),
       lessthan=_("<b>&lt;</b> less-than  <i>For numeric or date fields, if "
                  "you want the results having that field is less than "
                  "your term. <b>Example 1:</b> &lt;01-01-2016 "
                  "<b>Example 2:</b> &lt;20</i>"),
       greaterthan=_("<b>&gt;</b> greater-than  <i>Opposite of less-than "
                     "operator</i>"),
       lessequal=_("<b>&lt;=</b> less-than-or-equal-to  <i>In addition to "
                   "the less-than operator, it adds equality</i>"),
       greaterequal=_("<b>&gt;=</b> greater-than-or-equal-to  <i>"
                      "In addition to the greater-than operator, "
                      "it adds equality</i>"),
       dotdot=_("<b>..</b> dot-dot  <i>If you put dot-dot between your "
                "terms in a single field, it brings the values between "
                "two search terms. It is useful for numeric and "
                "date fields. <b>Example 1:</b> 10..20 is between "
                "10 and 20 including 10 and 20 <b>Example 2:</b> "
                "01-01-2015..01-01-2016 is between 01-01-2015 "
                "and 01-01-2016</i>"),
       underscore=_("<b>_</b> underscore  <i>Matches exactly one character "
                    "where it appears. <b>Example 1:</b> a_c matches abc "
                    "or axc or aBc</i>"))


class HelpWindow(html.HtmlWindow):
    def __init__(self, parent):
        html.HtmlWindow.__init__(self, parent,
                                 style=wx.NO_FULL_REPAINT_ON_RESIZE)
        if "gtk3" in wx.PlatformInfo or "gtk2" in wx.PlatformInfo:
            self.SetStandardFonts()


if __name__ == "__main__":
    _ = lambda x: x
    from . import utils
    utils._ = _
    app = wx.App()
    frame = MainSearchWin()
    app.MainLoop()
