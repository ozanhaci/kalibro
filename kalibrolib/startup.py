# encoding: utf-8
from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import
import os
import sys
import gettext

import wx

from . import pubsub
from . import conn_sqlite
from . import config


def load_translations(lang):
    gettext.install("Kalibro", "./locale", unicode=True)
    presLan = gettext.translation("Kalibro", "./locale",
                                  languages=[lang])
    presLan.install(unicode=True)


def start():
    pubsub.unsubscribe("start", start)
    configman = config.ConfigManager()
    try:
        configman.create()
    except:
        err_msg = "Configuration file holding settings could not be created"
        try:
            wx.MessageBox(err_msg, "Error", wx.OK)
        except:
            sys.stderr.write(err_msg + "\n")
        sys.exit(1)
    lang = configman["language"]
    load_translations(lang)
    development = os.environ.get("DEVELOPMENT", "") == "1"
    is_new = os.path.exists(".new")
    if not development and not is_new:
        return
    conn_sqlite.MainSql(forceCreate=True)
    configman.fix_compatibility()
    if not development and is_new:
        os.remove(".new")


def register():
    pubsub.subscribe("start", start)
