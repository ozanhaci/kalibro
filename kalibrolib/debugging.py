# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import os
import inspect
import functools

str = unicode

DEBUG = os.environ.get("DEBUG", "") == "1"

__all__ = ["DEBUG", "debug_decorator", "log_debug", "debug_trace"]


def debug_decorator(obj):
    """
    decorate the methods and function to log the calls and arguments
    """
    @functools.wraps(obj)
    def wrapperd(*args, **kwargs):
        log_debug(debug_trace(local=True),
                  "call %r with args=%r kwargs=%r" % (obj, args, kwargs))
        return obj(*args, **kwargs)
    if DEBUG:
        return wrapperd
    else:
        return obj


def nodebug(obj):
    setattr(obj, "DEBUG", False)
    return obj


def debug_trace(local=False):
    """
    Returns the current caller, module, line number
    """
    if not local:
        frm = inspect.stack()[1]
    else:
        frm = inspect.stack()[2]
    caller = frm[3]  # Obtain calling function
    mod = inspect.getmodule(frm[0])
    if mod is None:
        return ""
    cls_name = ""
    if "self" in frm[0].f_locals:
        cls_name = frm[0].f_locals["self"].__class__.__name__
    elif "cls" in frm[0].f_locals:
        cls_name = frm[0].f_locals["cls"].__name__
    if cls_name:
        obj = ".".join((mod.__name__, cls_name, caller))
    else:
        obj = ".".join((mod.__name__, caller))
    if local:
        lineno = inspect.currentframe().f_back.f_back.f_lineno
    else:
        lineno = inspect.currentframe().f_back.f_lineno
    obj += " Line: " + str(lineno)
    return obj


def log_debug(*args):
    print("DEBUG:", *args, end="\n\n")
