# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import os

import wx.grid
from wx.lib.wordwrap import wordwrap

from .utils import coltr


class FilePathRenderer(wx.grid.PyGridCellRenderer):
    """
    FilePathRenderer makes the path text blue and shows only file name not whole path
    """

    def __init__(self):
        wx.grid.PyGridCellRenderer.__init__(self)

    def Draw(self, grid, attr, dc, rect, row, col, isSelected):
        dc.SetBackgroundMode(wx.SOLID)
        dc.SetBrush(wx.Brush(attr.GetBackgroundColour(), wx.SOLID))
        dc.SetPen(wx.TRANSPARENT_PEN)
        dc.DrawRectangleRect(rect)
        dc.SetBackgroundMode(wx.TRANSPARENT)
        dc.SetFont(attr.GetFont())
        text = grid.GetCellValue(row, col)
        x = rect.x + 1
        y = rect.y + 1
        dc.SetTextForeground(wx.BLUE)
        try:
            text = os.path.basename(text)
        except:
            pass
        dc.DrawText(text, x, y)

    def GetBestSize(self, grid, attr, dc, row, col):
        text = grid.GetCellValue(row, col)
        dc.SetFont(attr.GetFont())
        w, h = dc.GetTextExtent(text)
        return wx.Size(w, h)

    def Clone(self):
        return FilePathRenderer()


class GridFileDropTarget(wx.FileDropTarget):

    def __init__(self, grid, target_column):
        wx.FileDropTarget.__init__(self)
        self.grid = grid
        assert target_column >= 0, "target_column is invalid"
        self.target_column = target_column

    def OnDropFiles(self, x, y, filenames):
        # the x,y coordinates here are Unscrolled coordinates.
        # They must be changed to scrolled coordinates.
        x, y = self.grid.CalcUnscrolledPosition(x, y)
        # now we need to get the row and column from the grid
        col = self.grid.XToCol(x)
        row = self.grid.YToRow(y)

        if row > -1 and col > -1:
            self.grid.SetCellValue(row, self.target_column,
                                   os.path.abspath(filenames[0]))
            self.grid.MakeCellVisible(row, self.target_column)
            self.grid.Refresh()


class CalibrationGrid(wx.grid.Grid):

    def __init__(self, parent, ID, size, numrows, numcols):
        wx.grid.Grid.__init__(self, parent, ID, size=size)
        self.CreateGrid(numrows, numcols)

        self.SetColLabelSize(75)
        self.SetRowLabelSize(0)

        self.SetColLabelValue(0, "ID")
        self.SetColSize(0, 40)
        dc = wx.ClientDC(self)
        self.SetColLabelValue(1, wordwrap(coltr("registryNo",
                                                "subrecords"), 135, dc))
        self.SetColSize(1, 150)
        self.SetColLabelValue(2, wordwrap(coltr("plannedDate",
                                                "subrecords"), 135, dc))
        self.SetColSize(2, 150)
        self.SetColLabelValue(3, wordwrap(coltr("realizedDate",
                                                "subrecords"), 135, dc))
        self.SetColSize(3, 150)
        self.SetColLabelValue(4, wordwrap(coltr("trackingNo",
                                                "subrecords"), 135, dc))
        self.SetColSize(4, 150)
        self.SetColLabelValue(5, wordwrap(coltr("method",
                                                "subrecords"), 135, dc))
        self.SetColSize(5, 150)
        self.SetColLabelValue(6, wordwrap(coltr("result",
                                                "subrecords"), 135, dc))
        self.SetColSize(6, 100)
        self.SetColLabelValue(7, wordwrap(coltr("path",
                                                "subrecords"), 135, dc))
        self.SetColSize(7, 150)
        self.setReadOnlyColumns(row=0, total=numrows)
        self.setEditorsRenderes(row=0, total=numrows)
        self.setGridHint()

        drop_target = GridFileDropTarget(self, target_column=7)
        self.SetDropTarget(drop_target)

    def SetCellValue(self, row, col, val):
        self.expandGrid(row, col)
        return wx.grid.Grid.SetCellValue(self, row, col, val)

    def GetCellValue(self, row, col):
        self.expandGrid(row, col)
        return wx.grid.Grid.GetCellValue(self, row, col)

    def expandGrid(self, row, col):
        if row + 1 > self.GetNumberRows():
            self.AppendRows(10, False)
            self.setReadOnlyColumns(row=row, total=10)
            self.setEditorsRenderes(row=row, total=10)

    def setReadOnlyColumns(self, row=0, total=1):
        '''
        prevent users from changing sensitive columns manually by
        setting them readonly
        '''
        for i in range(row, total):
            self.SetReadOnly(i, 0, True)  # ID Column
            self.SetReadOnly(i, 1, True)  # Registry No Column
            self.SetReadOnly(i, 7, True)  # Path Column

    def setEditorsRenderes(self, row=0, total=1):
        for i in range(row, total):
            self.SetCellRenderer(i, 7, FilePathRenderer())

    def setGridHint(self):
        prev_rowcol = [None, None]
        def onMouseMotion(evt):
            x, y = self.CalcUnscrolledPosition(evt.GetPosition())
            row = self.YToRow(y)
            col = self.XToCol(x)
            if (row, col) != prev_rowcol and row >= 0 and col >= 0:
                prev_rowcol[:] = [row, col]
                hinttext = self.rowColHint(row, col)
                if hinttext is None:
                    hinttext = ''
                self.GetGridWindow().SetToolTipString(hinttext)
            evt.Skip()
        self.GetGridWindow().Bind(wx.EVT_MOTION, onMouseMotion)

    def rowColHint(self, row, col):
        '''
        this method is called from setGridHint to show the whole file path of
        7th column as tooltip (only file names are shown to user on grid)
        '''
        if col == 7:
            return self.GetCellValue(row, col)
        return None
