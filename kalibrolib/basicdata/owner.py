# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import wx

from ..conn_sqlite import MainSql
from .lib import BaseManager, BaseWin, BaseModel

str = unicode


class OwnerModel(BaseModel):

    def __init__(self, db):
        self.db = db

    def get_all(self):
        qry = '''SELECT name FROM owner ORDER BY name ASC'''
        result = self.db.fetch_all(qry).result
        return [i[0] for i in result if i[0]]

    def add(self, **kw):
        query = '''INSERT INTO owner (name) VALUES (:param)'''
        tail = kw
        self.db.execStmt(query, tail)

    def delete(self, **kw):
        querySel = """SELECT 1 FROM main WHERE owner=:param"""
        tail = kw.copy()
        result = self.db.fetch_all(querySel, tail).result
        if len(result) > 0:
            return False
        query = """DELETE FROM owner WHERE name=:param"""
        self.db.execStmt(query, kw)
        return True


class OwnerManager(BaseManager):

    update_topic = "update_owner"

    def init(self):
        self.set_icon()


class OwnerWin(BaseWin):

    def __init__(self, parent=None,
                 manager_model_cls=OwnerModel,
                 manager_db_cls=MainSql):
        self.labelTxtCtrl = _("Owner")
        self.labelListBox = _("Defined Owners")
        super(OwnerWin, self).__init__(parent,
                                       manager_cls=OwnerManager,
                                       manager_db_cls=manager_db_cls,
                                       manager_model_cls=manager_model_cls)
        self.SetTitle(_("Define Device Owners"))


if __name__ == "__main__":
    _ = lambda x: x
    from .. import utils
    utils._ = _
    app = wx.App()
    frame = OwnerWin()
    app.MainLoop()
