# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

from abc import abstractmethod, ABCMeta
import traceback

import wx

from ..debugging import (DEBUG, debug_trace, log_debug)
from .. import pubsub

str = unicode


class BaseModel(object):

    __metaclass__ = ABCMeta

    def __init__(self, db):
        self.db = db

    @abstractmethod
    def get_all(self):
        ''' Override this '''
        raise NotImplementedError

    @abstractmethod
    def add(self, **kw):
        ''' Override this '''
        raise NotImplementedError

    @abstractmethod
    def delete(self, **kw):
        ''' Override this '''
        raise NotImplementedError


class BaseManager(object):

    update_topic = "topic"

    def __init__(self, view, model_cls, db_cls):
        self.view = view
        self.model_cls = model_cls
        self.model = model_cls(db=db_cls())
        self.init()

    def init(self):
        self.view.SetTitle(_("Title"))
        self.set_icon()

    def set_icon(self):
        if self.view.Parent:
            ic = self.view.Parent.GetIcon()
            self.view.SetIcon(ic)

    def on_widgets_created(self):
        self.view.btnDelete.Disable()
        self.populate()

    def populate(self):
        self.view.listBox.Clear()
        self.view.listBox.AppendItems(self.model.get_all())

    def add_new_rec(self):
        self.view.listBox.DeselectAll()
        self.view.btnDelete.Disable()
        newName = self.view.txtCtrl.GetValue().strip()
        if not newName:
            return
        allList = self.view.listBox.GetItems()
        if newName in allList:
            return self.view.msgbox(_("'%s' has already defined") % newName,
                                    _("Error"), wx.ICON_EXCLAMATION)
        try:
            self.model.add(param=newName)
        except Exception as e:
            self.view.msgbox(str(e), _("Error Occured"), wx.ICON_ERROR)
            if DEBUG:
                log_debug(debug_trace(), traceback.format_exc())
            return
        self.populate()
        self.view.txtCtrl.SetFocus()
        pubsub.publish(self.update_topic)

    def delete_rec(self):
        param = self.view.listBox.GetStringSelection()
        if not param:
            return
        try:
            res = self.model.delete(param=param)
            if res is False:
                return self.view.msgbox(_('(%s) is used in records.\n'
                                          'To delete it you must remove all its references'
                                          ) % param,
                                        _("Error"), wx.ICON_EXCLAMATION)
            pubsub.publish(self.update_topic)
        except Exception as e:
            self.view.msgbox(str(e), _("Error"), wx.ICON_ERROR)
            if DEBUG:
                log_debug(debug_trace(), traceback.format_exc())
            return
        self.populate()

    def on_click_listbox(self, selected=False):
        self.view.btnDelete.Enable(selected)


class BaseWin(wx.Frame):

    labelTxtCtrl = ""
    labelListBox = ""

    def __init__(self, parent=None,
                 manager_cls=None,
                 manager_model_cls=None,
                 manager_db_cls=None):
        wx.Frame.__init__(self, parent)
        self.manager = manager_cls(view=self, model_cls=manager_model_cls,
                                   db_cls=manager_db_cls)

        self.panel = wx.Panel(self)
        self.gbsizer = wx.GridBagSizer(5, 5)

        self.create_widgets()
        self.create_events()

        self.panel.SetSizerAndFit(self.gbsizer)
        self.Fit()
        self.CenterOnParent()
        self.Show()

    def delete_rec(self, evt):
        self.manager.delete_rec()
        evt.Skip()

    def add_new_rec(self, evt):
        self.manager.add_new_rec()
        evt.Skip()

    def create_widgets(self):
        panel = self.panel
        self.gbsizer.Add(wx.Size(10, 10), (0, 3))
        self.gbsizer.AddGrowableCol(2)
        label = wx.StaticText(panel, label=self.labelTxtCtrl)
        self.txtCtrl = wx.TextCtrl(panel, size=(200, -1))
        self.gbsizer.Add(label, (1, 1))
        self.gbsizer.Add(self.txtCtrl, (2, 1), flag=wx.ALL | wx.GROW)

        self.btnSave = wx.Button(panel, label=_("Save"))
        self.gbsizer.Add(self.btnSave, (2, 2), flag=wx.ALL | wx.GROW)

        label = wx.StaticText(panel, label=self.labelListBox)
        self.listBox = wx.ListBox(panel, size=(-1, 200),
                                      style=wx.LB_HSCROLL | wx.LB_SINGLE |
                                      wx.LB_SORT)
        self.gbsizer.Add(label, (3, 1))
        self.gbsizer.Add(self.listBox, (4, 1), (2, 2),
                         flag=wx.ALL | wx.GROW)

        self.btnDelete = wx.Button(panel, label=_("Delete"))
        self.gbsizer.Add(self.btnDelete, (6, 1))
        self.manager.on_widgets_created()

    def on_click_listbox(self, evt):
        self.manager.on_click_listbox(selected=evt.IsSelection())
        evt.Skip()

    def create_events(self):
        self.listBox.Bind(wx.EVT_LISTBOX, self.on_click_listbox)
        self.btnSave.Bind(wx.EVT_BUTTON, self.add_new_rec)
        self.btnDelete.Bind(wx.EVT_BUTTON, self.delete_rec)

    def msgbox(self, msg, caption, flag=wx.ICON_INFORMATION):
        wx.MessageBox(msg, caption, flag)


