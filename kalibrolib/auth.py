# encoding: utf-8
from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import
import threading
import hashlib
import functools
import re
import json
import traceback

import wx

from . import pubsub
from .conn_sqlite import MainSql
from .search import SearchListCtrl
from .utils import coltr, get_icon
from .debugging import DEBUG, log_debug

_USER = ""
_INSTALLED = None
RLOCK = threading.RLock()

PERM_WRITE = 1 << 0
PERM_CHG_STATUS = 1 << 1
PERM_CHG_OWNER = 1 << 2


def get_user():
    return _USER


def set_user(username):
    with RLOCK:
        global _USER
        _USER = username


def admin_required(func):
    @functools.wraps(func)
    def wrapper(*args, **kw):
        username = get_user()
        if username == "admin":
            return func(*args, **kw)
        else:
            pubsub.publish("admin_required")
    return wrapper


class ModelAuth(object):

    def __init__(self, db=None):
        if db is None:
            db = MainSql()
        self.db = db

    def auth_installed(self):
        with RLOCK:
            global _INSTALLED
            if _INSTALLED is not None:
                return _INSTALLED
            qry = 'SELECT name FROM sqlite_master WHERE type="table" AND name="auth"'
            result = self.db.fetch_all(qry).result
            tables = []
            if result and len(result[0]):
                tables = [i[0] for i in result]
            if "auth" in tables:
                qry = "SELECT * FROM auth"
                allrecs = self.db.fetch_all(qry).result
                if allrecs and len(allrecs[0]) > 0:
                    _INSTALLED = True
                else:
                    _INSTALLED = False
                return _INSTALLED
            _INSTALLED = False
            return _INSTALLED

    def has_write_perm(self, username):
        username = username.lower()
        if username == "admin":
            return True
        if not self.auth_installed():
            return True
        qry = 'SELECT write FROM auth WHERE userName=?'
        result = self.db.fetch_all(qry, (username,)).result
        if result and len(result):
            return result[0][0] > 0
        return False

    def can_change(self, permission, username):
        username = username.lower()
        if username == "admin":
            return True
        if not self.auth_installed():
            return True
        qry = 'SELECT extra FROM auth WHERE userName=?'
        result = self.db.fetch_all(qry, (username,)).result
        if result and len(result):
            try:
                extra = json.loads(result[0][0])
                permissions = extra["permissions"]
                return permission in permissions
            except:
                pass
        return False

    def can_change_dev_status(self, username):
        return self.can_change("change_status", username)

    def can_change_dev_owner(self, username):
        return self.can_change("change_owner", username)

    def verify(self, username, password):
        username = username.lower()
        passhash = self.password_hash(password)
        query = '''SELECT 1 FROM auth WHERE userName=? AND password=?'''
        tail = (username, passhash)
        result = self.db.fetch_all(query, tail).result
        if result:
            return True
        return False

    def get_all(self):
        qry = 'SELECT * FROM auth'
        return self.db.fetch_all(qry).result

    def get_user(self, username):
        username = username.lower()
        qry = 'SELECT * FROM auth WHERE userName=?'
        return self.db.fetch_all(qry, (username,)).result

    def password_hash(self, password):
        return hashlib.sha256(password).hexdigest()

    def add_user(self, username, namesurname, password, permissions):
        username = username.lower()
        qry = '''INSERT INTO auth (userName, nameSurname, password, write, extra)
            VALUES (?,?,?,?,?)'''
        password = self.password_hash(password)
        write = PERM_WRITE & permissions > 0 and 1 or 0
        if username == "admin":
            write = 1
        extra = {"permissions": []}
        if PERM_CHG_STATUS & permissions:
            extra["permissions"].append("change_status")
        if PERM_CHG_OWNER & permissions:
            extra["permissions"].append("change_owner")
        extra = json.dumps(extra)
        self.db.execute(qry, (username, namesurname, password, write, extra))
        pubsub.publish("update_users")

    def update_user(self, username, namesurname, password, permissions):
        username = username.lower()
        qry = '''UPDATE auth SET nameSurname=:nameSurname,
            password=:password, write=:write, extra=:extra
            WHERE userName=:userName'''
        password = self.password_hash(password)
        write = PERM_WRITE & permissions > 0 and 1 or 0
        if username == "admin":
            write = 1
        extra = {"permissions": []}
        if PERM_CHG_STATUS & permissions:
            extra["permissions"].append("change_status")
        if PERM_CHG_OWNER & permissions:
            extra["permissions"].append("change_owner")
        extra = json.dumps(extra)
        tail = {"nameSurname": namesurname, "password": password,
                "write": write, "userName": username, "extra": extra}
        self.db.execute(qry, tail)
        pubsub.publish("update_users")

    def update_password(self, username, password):
        username = username.lower()
        qry = '''UPDATE auth SET password=:password
            WHERE userName=:userName'''
        password = self.password_hash(password)
        tail = {"password": password, "userName": username}
        self.db.execute(qry, tail)
        pubsub.publish("update_users")

    def delete_user(self, username):
        username = username.lower()
        qry = '''DELETE FROM auth WHERE userName=:userName'''
        tail = {"userName": username}
        self.db.execute(qry, tail)
        pubsub.publish("update_users")



class LoginManager(object):

    def __init__(self, view, db_cls, model_cls):
        self.view = view
        db = db_cls()
        self.model = model_cls(db)

    def on_ok(self):
        if not self.model.auth_installed():
            self.success("admin")
        msg_err = _("Invalid username or password")
        username, password = (self.view.txt_username.GetValue(),
                              self.view.txt_password.GetValue())
        if not username or not password:
            self.view.messagebox(msg_err, _("Error"), wx.ICON_ERROR)
            return
        verified = self.model.verify(username, password)
        if verified:
            return self.success(username)
        self.view.messagebox(msg_err, _("Error"), wx.ICON_ERROR)

    def success(self, username):
        set_user(username)
        self.view.Close(True)
        pubsub.publish("authorized")


class UserManager(object):

    def __init__(self, view, db_cls, model_cls):
        self.view = view
        db = db_cls()
        self.model = model_cls(db)
        pubsub.subscribe("update_users", self.populate_users)
        pubsub.subscribe("admin_required", self.unauthorized_access)

    def unauthorized_access(self):
        self.view.messagebox(_("You are trying to perform an unauthorized "
                               "operation\nYou should be admin user."),
                             _("Error"), wx.ICON_ERROR)

    def on_add_user(self):
        username = self.view.txt_username.GetValue()
        if username == "admin":
            adminrec = self.model.get_user(username)
            if adminrec:
                if get_user() != username:
                    return self.unauthorized_access()
        else:
            if get_user() != "admin":
                rec = self.model.get_user(username)
                if rec:
                    return self.unauthorized_access()
            has_admin = self.model.get_user("admin")
            if not has_admin:
                return self.view.messagebox(_('"admin" account must be created at first'),
                                            _("Error"), wx.ICON_ERROR)

        namesurname = self.view.txt_namesurname.GetValue()
        password = self.view.txt_password.GetValue()
        permissions = self.get_permissions()
        if not self.check_inputs(username, namesurname, password, permissions):
            return
        try:
            self.model.add_user(username, namesurname, password, permissions)
            self.clear_inputs()
        except Exception as e:
            if DEBUG:
                log_debug(traceback.format_exc())
            self.view.messagebox(unicode(e), _("Error"), wx.ICON_ERROR)

    @admin_required
    def on_update_user(self):
        username = self.view.txt_username.GetValue()
        namesurname = self.view.txt_namesurname.GetValue()
        password = self.view.txt_password.GetValue()
        permissions = self.get_permissions()
        if not self.check_inputs(username, namesurname, password, permissions):
            return
        try:
            self.model.update_user(username, namesurname, password, permissions)
            self.clear_inputs()
        except Exception as e:
            self.view.messagebox(unicode(e), _("Error"), wx.ICON_ERROR)

    @admin_required
    def on_delete_user(self):
        if self.view.list_users.GetSelectedItemCount() != 1:
            self.view.btn_delete.Disable()
            return
        idx = self.get_selected_idx()
        item = self.view.list_users.GetItem(idx, 0)
        username = item.GetText()
        dlg = wx.MessageDialog(self.view,
                    _("Do you really want to delete user '%s'?" % username),
                    _("Question"), wx.YES_NO | wx.NO_DEFAULT)
        if dlg.ShowModal() != wx.ID_YES:
            return
        try:
            self.model.delete_user(username)
        except Exception as e:
            self.view.messagebox(unicode(e), _("Error"), wx.ICON_ERROR)

    def get_selected_idx(self):
        i = -1
        while True:
            i = self.view.list_users.GetNextItem(i, wx.LIST_NEXT_ALL,
                                                 wx.LIST_STATE_SELECTED)
            if i != -1:
                break
        return i

    def check_inputs(self, username, namesurname, password, permissions):
        if not password or not username:
            self.view.messagebox(_("Username or password cannot be empty"),
                                 _("Error"), wx.ICON_ERROR)
            return False
        matches = re.findall("\w+", username)
        if len(matches) > 1:
            self.view.messagebox(_("User name cannot contain space or unicode characters"),
                                 _("Error"), wx.ICON_ERROR)
            return False

        try:
            PasswordManager.check_password(password)
        except Exception as e:
            self.view.messagebox(unicode(e),
                                 _("Error"), wx.ICON_ERROR)
            return False
        assert permissions >= 0, "Unexpected Permssion Error"
        assert isinstance(namesurname, basestring)
        return True

    def get_permissions(self):
        perm_write = self.view.cb_write.GetValue() and PERM_WRITE or 0
        perm_chg_status = 0
        perm_chg_owner = 0
        if perm_write == 0:
            perm_chg_status = self.view.cb_change_dev_status.GetValue() and PERM_CHG_STATUS or 0
            perm_chg_owner = self.view.cb_change_dev_owner.GetValue() and PERM_CHG_OWNER or 0
        return perm_write | perm_chg_status | perm_chg_owner

    def on_user_selected(self, username):
        if username:
            self.view.btn_delete.Enable()

    def on_user_deselected(self):
        self.view.btn_delete.Enable(False)

    def on_user_activated(self, username):
        rec = self.model.get_user(username)
        if not rec:
            return
        userrec = dict(rec[0])
        self.clear_inputs()
        self.view.txt_username.SetValue(userrec["userName"])
        self.view.txt_namesurname.SetValue(userrec["nameSurname"] or "")
        self.view.cb_write.SetValue(userrec["write"])
        permissions = []
        try:
            permissions = json.loads(userrec["extra"])
            permissions = permissions["permissions"]
        except:
            pass
        self.view.cb_change_dev_status.SetValue("change_status" in permissions)
        self.view.cb_change_dev_owner.SetValue("change_owner" in permissions)

    def clear_inputs(self):
        self.view.txt_username.Clear()
        self.view.txt_password.Clear()
        self.view.txt_namesurname.Clear()
        self.view.cb_write.SetValue(False)

    def on_widgets_created(self):
        self.populate_users()
        self.view.btn_delete.Disable()

    def populate_users(self):
        self.view.list_users.DeleteAllItems()
        users = self.model.get_all()
        for idx, user in enumerate(users):
            user = dict(user)
            self.view.list_users.InsertStringItem(idx, user["userName"])
            self.view.list_users.SetStringItem(idx, 1, user["nameSurname"])
            self.view.list_users.SetStringItem(idx, 2, str(user["write"]))
            self.view.list_users.SetStringItem(idx, 3, user["extra"])
        for i in range(self.view.list_users.GetColumnCount()):
            self.view.list_users.SetColumnWidth(i, wx.LIST_AUTOSIZE_USEHEADER)


class PasswordManager(object):

    def __init__(self, view, db_cls, model_cls):
        self.view = view
        db = db_cls()
        self.model = model_cls(db)

    def on_change_password(self):
        username = self.view.txt_username.GetValue()
        oldpass = self.view.txt_oldpass.GetValue()
        newpass = self.view.txt_newpass.GetValue()
        passagain = self.view.txt_passagain.GetValue()
        if not username:
            self.view.messagebox(_("User Name is missing"),
                                 _("Error"), wx.ICON_ERROR)
            return
        if newpass != passagain:
            self.view.messagebox(_("Passwords do not match"),
                                 _("Error"), wx.ICON_ERROR)
            return
        result = self.model.verify(username, oldpass)
        if not result:
            self.view.messagebox(_("Wrong old password"),
                                 _("Error"), wx.ICON_ERROR)
            return
        try:
            self.check_password(newpass)
            self.model.update_password(username, newpass)
        except Exception as e:
            self.view.messagebox(unicode(e), _("Error"), wx.ICON_ERROR)
            return
        self.view.Close(True)

    def clear_inputs(self):
        self.view.txt_username.Clear()
        self.view.txt_oldpass.Clear()
        self.view.txt_newpass.Clear()
        self.view.txt_passagain.Clear()

    @classmethod
    def check_password(cls, password):
        matches = re.findall("\w+", password, re.U)
        if len(matches) > 1:
            raise Exception(_("Password cannot contain space"))
        if len(password) < 6:
            raise Exception(_("Password length must be at least 6 characters"))
        return True

    def on_widgets_created(self):
        username = get_user()
        if not username:
            self.view.messagebox(_("You must login before you change password."),
                                 _("Error"), wx.ICON_ERROR)
            self.view.Close(True)
            return
        self.view.txt_username.SetValue(username)


class LoginWin(wx.Frame):

    def __init__(self, parent=None, manager_cls=LoginManager,
                 db_cls=MainSql, model_cls=ModelAuth):
        super(LoginWin, self).__init__(parent=parent)
        self.manager = manager_cls(view=self, db_cls=db_cls, model_cls=model_cls)
        self.panel = wx.Panel(self)
        self.sizergb = wx.GridBagSizer(10, 10)
        self.create_widgets()
        self.create_events()
        self.SetTitle("Kalibro - " + _("Login"))
        self.SetIcon(get_icon())

        self.panel.SetSizerAndFit(self.sizergb)
        self.Fit()
        self.CenterOnScreen()
        self.Show()

    def create_widgets(self):
        self.sizergb.Add(wx.Size(10, 10), (0, 3))
        label = wx.StaticText(self.panel, label=_("Please Login"))
        self.sizergb.Add(label, (0, 2))

        label = wx.StaticText(self.panel, label=_("User Name"))
        self.sizergb.Add(label, (1, 1))
        self.txt_username = wx.TextCtrl(self.panel, size=(200, -1))
        self.sizergb.Add(self.txt_username, (1, 2))

        label = wx.StaticText(self.panel, label=_("Password"))
        self.sizergb.Add(label, (2, 1))
        self.txt_password = wx.TextCtrl(self.panel, style=wx.TE_PASSWORD |
                                        wx.TE_PROCESS_ENTER)
        self.sizergb.Add(self.txt_password, (2, 2), flag=wx.GROW)

        self.btn_ok = wx.Button(self.panel, label=_("Login"))
        self.sizergb.Add(self.btn_ok, (3, 2))
        self.sizergb.Add(wx.Size(10, 10), (4, 0))

    def create_events(self):
        self.btn_ok.Bind(wx.EVT_BUTTON, self.on_ok)
        self.txt_password.Bind(wx.EVT_TEXT_ENTER, self.on_ok)

    def on_ok(self, evt):
        self.manager.on_ok()
        evt.Skip()

    def messagebox(self, msg, caption, flag=wx.ICON_INFORMATION):
        wx.MessageBox(msg, caption, flag)


class UserWin(wx.Frame):

    def __init__(self, parent=None, manager_cls=UserManager,
                 db_cls=MainSql, model_cls=ModelAuth):
        super(UserWin, self).__init__(parent=parent, size=(650, 550))
        self.manager = manager_cls(view=self, db_cls=db_cls, model_cls=model_cls)
        self.panel = wx.Panel(self)
        self.sizergb = wx.GridBagSizer(10, 10)
        self.create_widgets()
        self.create_events()
        self.SetTitle("Kalibro - " + _("User Management"))
        if parent:
            icon = parent.GetIcon()
            self.SetIcon(icon)

        self.panel.SetSizerAndFit(self.sizergb)

        self.CenterOnScreen()
        self.Show()

    def create_widgets(self):

        label = wx.StaticText(self.panel, label=_("User Name"))
        self.sizergb.Add(label, (1, 1))
        self.txt_username = wx.TextCtrl(self.panel, size=(150, -1))
        self.sizergb.Add(self.txt_username, (1, 2))

        label = wx.StaticText(self.panel, label=_("Name Surname"))
        self.sizergb.Add(label, (1, 3))
        self.txt_namesurname = wx.TextCtrl(self.panel, size=(150, -1))
        self.sizergb.Add(self.txt_namesurname, (1, 4))

        label = wx.StaticText(self.panel, label=_("Password"))
        self.sizergb.Add(label, (2, 1))
        self.txt_password = wx.TextCtrl(self.panel, size=(150, -1),
                                        style=wx.TE_PASSWORD)
        self.sizergb.Add(self.txt_password, (2, 2))

        self.cb_write = wx.CheckBox(self.panel, label=_("Has Write Permission?"))
        self.sizergb.Add(self.cb_write, (2, 4))

        self.cb_change_dev_status = wx.CheckBox(self.panel,
                                label=_("Can Change Device Status?"))
        self.sizergb.Add(self.cb_change_dev_status, (3, 4))

        self.cb_change_dev_owner = wx.CheckBox(self.panel,
                                label=_("Can Change Device Owner?"))
        self.sizergb.Add(self.cb_change_dev_owner, (4, 4))

        self.btn_add = wx.Button(self.panel, label=_("Add User"))
        self.sizergb.Add(self.btn_add, (3, 2), flag=wx.GROW)
        self.btn_update = wx.Button(self.panel, label=_("Update User"))
        self.sizergb.Add(self.btn_update, (4, 2), flag=wx.GROW)

        label = wx.StaticText(self.panel, label=_("Registered Users"))
        self.sizergb.Add(label, (5, 1))
        self.list_users = SearchListCtrl(self.panel,
                                         size=(1, -1),
                                         style=wx.LC_REPORT |
                                         wx.BORDER_NONE |
                                         wx.LC_SINGLE_SEL |
                                         wx.LC_VRULES |
                                         wx.LC_HRULES)
        self.list_users.SetToolTipString(_("Double click to update"))
        self.sizergb.Add(self.list_users, (6, 1), (4, 4), flag=wx.GROW | wx.ALL)
        self.sizergb.AddGrowableCol(4)

        self.btn_delete = wx.Button(self.panel, label=_("Delete"))
        self.btn_delete.SetToolTipString(_("Delete selected user from Kalibro"))
        self.sizergb.Add(self.btn_delete, (10, 1))

        for i, k in enumerate(["userName", "nameSurname", "write", "extra"]):
            self.list_users.InsertColumn(i, coltr(k, "auth"))

        self.manager.on_widgets_created()

    def create_events(self):
        self.btn_add.Bind(wx.EVT_BUTTON, self.on_add_user)
        self.btn_update.Bind(wx.EVT_BUTTON, self.on_update_user)
        self.btn_delete.Bind(wx.EVT_BUTTON, self.on_delete_user)
        self.cb_write.Bind(wx.EVT_CHECKBOX, self.on_change_perm)
        self.cb_change_dev_status.Bind(wx.EVT_CHECKBOX, self.on_change_perm)
        self.cb_change_dev_owner.Bind(wx.EVT_CHECKBOX, self.on_change_perm)
        self.list_users.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.on_user_activated)
        self.list_users.Bind(wx.EVT_LIST_ITEM_SELECTED, self.on_user_selected)
        self.list_users.Bind(wx.EVT_LIST_ITEM_DESELECTED, self.on_user_deselected)

    def on_change_perm(self, evt):
        evt.Skip()
        if self.cb_write.GetValue():
            self.cb_change_dev_status.SetValue(False)
            self.cb_change_dev_owner.SetValue(False)
        if self.cb_change_dev_status.GetValue() or \
            self.cb_change_dev_owner.GetValue():
            self.cb_write.SetValue(False)

    def on_add_user(self, evt):
        self.manager.on_add_user()
        evt.Skip()

    def on_update_user(self, evt):
        self.manager.on_update_user()
        evt.Skip()

    def on_delete_user(self, evt):
        self.manager.on_delete_user()
        evt.Skip()

    def on_user_activated(self, evt):
        username = evt.GetText()
        self.manager.on_user_activated(username)
        evt.Skip()

    def on_user_selected(self, evt):
        username = evt.GetText()
        self.manager.on_user_selected(username)
        evt.Skip()

    def on_user_deselected(self, evt):
        self.manager.on_user_deselected()
        evt.Skip()

    def messagebox(self, msg, caption, flag=wx.ICON_INFORMATION):
        wx.MessageBox(msg, caption, flag)


class PasswordWin(wx.Frame):

    def __init__(self, parent=None, manager_cls=PasswordManager,
                 db_cls=MainSql, model_cls=ModelAuth):
        super(PasswordWin, self).__init__(parent=parent)
        self.manager = manager_cls(view=self, db_cls=db_cls, model_cls=model_cls)
        self.panel = wx.Panel(self)
        self.sizergb = wx.GridBagSizer(10, 10)
        self.create_widgets()
        self.create_events()
        self.SetTitle("Kalibro - " + _("Change Password"))
        if parent:
            icon = parent.GetIcon()
            self.SetIcon(icon)

        self.panel.SetSizerAndFit(self.sizergb)
        self.Fit()
        self.CenterOnScreen()
        self.Show()

    def create_widgets(self):
        self.sizergb.Add(wx.Size(10, 10), (0, 3))
        label = wx.StaticText(self.panel, label=_("User Name"))
        self.sizergb.Add(label, (1, 1))
        self.txt_username = wx.TextCtrl(self.panel, size=(150, -1),
                                        style=wx.TE_READONLY)
        self.sizergb.Add(self.txt_username, (1, 2))

        label = wx.StaticText(self.panel, label=_("Old Password"))
        self.sizergb.Add(label, (2, 1))
        self.txt_oldpass = wx.TextCtrl(self.panel, size=(150, -1),
                                       style=wx.TE_PASSWORD)
        self.sizergb.Add(self.txt_oldpass, (2, 2))

        label = wx.StaticText(self.panel, label=_("New Password"))
        self.sizergb.Add(label, (3, 1))
        self.txt_newpass = wx.TextCtrl(self.panel, size=(150, -1),
                                       style=wx.TE_PASSWORD)
        self.sizergb.Add(self.txt_newpass, (3, 2))

        label = wx.StaticText(self.panel, label=_("New Password Again"))
        self.sizergb.Add(label, (4, 1))
        self.txt_passagain = wx.TextCtrl(self.panel, size=(150, -1),
                                         style=wx.TE_PASSWORD)
        self.sizergb.Add(self.txt_passagain, (4, 2))

        self.btn_change = wx.Button(self.panel, label=_("Change Password"))
        self.sizergb.Add(self.btn_change, (5, 2), flag=wx.GROW)
        self.sizergb.Add(wx.Size(10, 10), (6, 0))

        self.manager.on_widgets_created()

    def create_events(self):
        self.btn_change.Bind(wx.EVT_BUTTON, self.on_change_password)

    def on_change_password(self, evt):
        self.manager.on_change_password()
        evt.Skip()

    def messagebox(self, msg, caption, flag=wx.ICON_INFORMATION):
        wx.MessageBox(msg, caption, flag)


if __name__ == "__main__":
    app = wx.App(0)
    coltr = lambda x, y: x
    _ = lambda x: x
    UserWin()
    app.MainLoop()
