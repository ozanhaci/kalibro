#
# The Alphanum Algorithm is an improved sorting algorithm for strings
# containing numbers.  Instead of sorting numbers in ASCII order like
# a standard sort, this algorithm sorts numbers in numeric order.
#
# The Alphanum Algorithm is discussed at http://www.DaveKoelle.com
#
#* Python implementation provided by Chris Hulan (chris.hulan@gmail.com)
#* Distributed under same license as original
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#

import re

__all__ = ["alphanum"]

def int_float(func1, func2, val):
    try:
        return func1(val)
    except ValueError:
        return func2(val)

def chunkify(strval):
    """
    return a list of numbers and non-numeric substrings of +strval+
    the numeric substrings are converted to integer, non-numeric are left as is
    """
    chunks = re.findall("(\d+\.\d+|\d+|\D+)", strval)
    chunks = [re.match('\d', x) and int_float(int, float, x) or x for x in chunks]  #convert numeric strings to numbers
    return chunks

def alphanum(a, b):
    '''
    split strings into chunks
    
    >>> unsorted=['z9.doc', 'z5.doc', 'z17.doc', 'z12.doc', 'z102.doc', 'z6.doc', 'z11.doc', 'z20.doc', 'z19.doc', 'z16.doc', 'z101.doc', 'z18.doc', 'z13.doc', 'z4.doc', 'z10.doc', 'z14.doc', 'z7.doc', 'z1.doc', 'z15.doc', 'z100.doc', 'z2.doc', 'z3.doc', 'z8.doc']
    >>> sorted_=unsorted[:]
    >>> sorted_.sort(alphanum)
    >>> sorted_ == ['z1.doc', 'z2.doc', 'z3.doc', 'z4.doc', 'z5.doc', 'z6.doc', 'z7.doc', 'z8.doc', 'z9.doc', 'z10.doc', 'z11.doc', 'z12.doc', 'z13.doc', 'z14.doc', 'z15.doc', 'z16.doc', 'z17.doc', 'z18.doc', 'z19.doc', 'z20.doc', 'z100.doc', 'z101.doc', 'z102.doc']
    True
    '''
    aChunks = chunkify(a)
    bChunks = chunkify(b)
    return cmp(aChunks, bChunks)

