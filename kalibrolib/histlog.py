# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import datetime
from collections import OrderedDict

import wx
from ObjectListView import FastObjectListView, ColumnDefn

from .config import ConfigManager
from .conn_sqlite import MainSql
from .utils import coltr, get_icon
from .debugging import DEBUG, log_debug
from .search import HelpWindow, build_qry_where, get_html_help

str = unicode


def defValueGetter(wid):
    return wid.GetValue()


def defValueConverter(val):
    return val


def intValueConverter(val):
    try:
        return int(val)
    except ValueError:
        return val


def realValueConverter(val):
    try:
        return float(val)
    except ValueError:
        return val


COLS = {"INTEGER":   {"widgetCls":     wx.TextCtrl,
                      "valueGetter":   defValueGetter,
                      "valueConverter":intValueConverter},
        "REAL":      {"widgetCls":     wx.TextCtrl,
                      "valueGetter":   defValueGetter,
                      "valueConverter":realValueConverter},
        "TEXT":      {"widgetCls":     wx.TextCtrl,
                      "valueGetter":   defValueGetter,
                      "valueConverter":defValueConverter},
        "TIMESTAMP": {"widgetCls":     wx.TextCtrl,
                      "valueGetter":   defValueGetter,
                      "valueConverter":defValueConverter},
        "DATETIME":  {"widgetCls":     wx.TextCtrl,
                      "valueGetter":   defValueGetter,
                      "valueConverter":defValueConverter},
        "DATE":      {"widgetCls":     wx.TextCtrl,
                      "valueGetter":   defValueGetter,
                      "valueConverter":defValueConverter},
        }


def stringConverter(type_, **kw):
    def intConverter(val):
        if val is None:
            return ""
        return "%s" % val
    def datetimeConverter(val):
        if val is None:
            return ""
        else:
            if isinstance(val, (datetime.datetime, datetime.date,
                                datetime.datetime.time)):
                return val.strftime(datetime_format)
            return val or ""
    if type_  in (int, long, float):
        return intConverter
    elif type_ in (datetime.datetime, datetime.date, datetime.time):
        datetime_format = kw["datetime_format"]
        return datetimeConverter


class BaseModel(object):
    def __init__(self, tablename, db_cls=MainSql):
        self.tablename = tablename
        self.db = db_cls()

    def fields_meta(self):
        meta = []
        result = self.db.fetch_all('pragma table_info("%s");' % self.tablename).result
        for row in result:
            rowdict = dict(row)
            meta.append(rowdict)
        return meta

    def search(self, where, tail, pagesize, pagenum, order_by=" ORDER BY timestamp DESC "):
        if pagesize > 0:
            qry = ('SELECT * FROM %s' % self.tablename + " " + where + order_by +
                   " LIMIT " + str(pagesize) + " OFFSET " + str(pagesize * (pagenum - 1)))
        else:
            qry = 'SELECT * FROM %s' % self.tablename + order_by
        if DEBUG:
            log_debug("SQL:", qry, tail)
        return self.db.fetch_all(qry, tail).result

    def count(self, where, tail):
        qry = 'SELECT COUNT(*) FROM %s' % self.tablename + " " + where
        if DEBUG:
            log_debug("SQL:", qry, tail)
        return self.db.fetch_all(qry, tail).result[0][0]


class BaseHistLogManager(object):
    def __init__(self, view, tablename, db_cls=MainSql):
        self.view = view
        self.tablename = tablename
        self.fields_meta = OrderedDict()
        self.model = self.get_model_cls()(tablename, db_cls=db_cls)
        # self.create_widgets()

    def get_fields_meta(self):
        if self.fields_meta:
            return self.fields_meta
        meta = self.model.fields_meta()
        for row in meta:
            _type = row["type"].split()[0]
            self.fields_meta[row["name"]] = \
                {"label": coltr(row["name"], self.tablename),
                 "widgetCls": COLS.get(_type,
                                        {"widgetCls":wx.TextCtrl})["widgetCls"],
                 "valueGetter": COLS.get(_type,
                                         {"valueGetter": lambda x: None})["valueGetter"],
                 "valueConverter": COLS.get(_type,
                                         {"valueConverter": lambda x: x})["valueConverter"],
                 "widget": None,
                 "type": _type,
                 }
        return self.fields_meta

    def get_model_cls(self):
        '''
        Override this and return model class
        '''
        return BaseModel

    def get_panel_name(self):
        '''
        Override this and return string (panel name)
        '''

    def get_sizer_colnum(self):
        return 2

    def create_widgets(self):
        fields_meta = self.get_fields_meta()
        numcols = self.get_sizer_colnum()
        totalcols = len(fields_meta)
        numrows = (int(totalcols / self.get_sizer_colnum()) +
                   (totalcols % self.get_sizer_colnum() > 0))
        self.view.sizerflx = wx.FlexGridSizer(cols=numcols * 2,
                                              rows=numrows, hgap=3, vgap=3)
        sizerflx = self.view.sizerflx
        sizerflx.SetFlexibleDirection(wx.VERTICAL)
        for fkey, field in fields_meta.items():
            widget = field["widgetCls"](parent=self.view)
            field["widget"] = widget
            widget.Name = fkey
            label = wx.StaticText(self.view, label=field["label"],
                                  style=wx.ALIGN_LEFT,
                                  )
            sizerflx.Add(label, 0, wx.ALIGN_LEFT)
            sizerflx.Add(widget, 0)

        btn_sizer = wx.BoxSizer(wx.HORIZONTAL)
        btn = wx.Button(self.view, label=_("Search"))
        btn.Bind(wx.EVT_BUTTON, self.on_search)
        btn_sizer.Add(btn, 0)
        btn = wx.Button(self.view, label=_("Clear"))
        btn.Bind(wx.EVT_BUTTON, self.on_clear)
        btn_sizer.Add(wx.Size(20, 20))
        btn_sizer.Add(btn, 0)

        self.view.obj_list_view = \
            FastObjectListView(self.view, -1,
                               style=wx.LC_REPORT | wx.SUNKEN_BORDER,
                               size=(-1, 300))
        self.view.obj_list_view.SetEmptyListMsg(_("List is Empty"))
        date_format = ConfigManager()["date_format"]
        col_defs = []
        for key, val in fields_meta.items():
            kw = {}
            kw["align"] = "left"
            kw["minimumWidth"] = 120
            _type = val["type"].upper()
            if  _type in ("TIMESTAMP", "DATETIME"):
                kw["stringConverter"] = stringConverter(datetime.datetime,
                                    datetime_format=date_format + " %H:%M:%S")
            if  _type == "DATE":
                kw["stringConverter"] = stringConverter(datetime.datetime,
                                    datetime_format=date_format)
            elif _type in ("INTEGER", "INT"):
                kw["align"] = "right"
                kw["stringConverter"] = stringConverter(int)
                kw["minimumWidth"] = 60
            elif _type == "REAL":
                kw["align"] = "right"
                kw["stringConverter"] = stringConverter(float)
                kw["minimumWidth"] = 60
            col_defs.append(ColumnDefn(coltr(key, self.tablename),
                                       valueGetter=key,
                                       isEditable=False, **kw))
        self.view.obj_list_view.SetColumns(col_defs)
        pager_sizer = wx.FlexGridSizer(cols=4, rows=1, hgap=5, vgap=5)
        self.view.cb_pagesize = wx.ComboBox(self.view,
                        choices=list(map(str, range(100, 600, 100))),
                        size=(100, -1), style=wx.CB_READONLY)
        self.view.cb_pagesize.AppendItems(["500+"])
        self.view.cb_pagesize.SetSelection(0)
        pager_sizer.Add(wx.StaticText(self.view, label=_("Page Size")), 0,
                        wx.ALIGN_CENTER_VERTICAL)
        pager_sizer.Add(self.view.cb_pagesize, 0)

        spin_sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.view.spin_pagenum = wx.SpinCtrl(self.view)
        spin_sizer.Add(self.view.spin_pagenum, 0)
        self.view.label_pagenum = wx.StaticText(self.view)
        spin_sizer.Add(self.view.label_pagenum, 0, wx.ALIGN_CENTRE_VERTICAL)
        pager_sizer.Add(wx.StaticText(self.view, label=_("Page")), 0,
                        wx.ALIGN_CENTER_VERTICAL)
        pager_sizer.Add(spin_sizer, 0)

        self.view.sizerv = wx.BoxSizer(wx.VERTICAL)
        self.view.sizerv.Add(sizerflx, 0)
        self.view.sizerv.Add(wx.Size(20, 20))
        self.view.sizerv.Add(btn_sizer, 0)
        self.view.sizerv.Add(wx.Size(10, 10))
        self.view.sizerv.Add(self.view.obj_list_view, 0, flag=wx.ALL | wx.GROW)
        self.view.sizerv.Add(pager_sizer, 0)
        self.view.SetSizerAndFit(self.view.sizerv)

    def on_search(self, evt=None, pagenum=None):
        with self.view.busy_cursor():
            return self._on_search(evt, pagenum)

    def _on_search(self, evt=None, pagenum=None):
        self.on_clear()
        self.view.spin_pagenum.Unbind(wx.EVT_SPINCTRL)
        if evt:
            evt.Skip()
        try:
            pagesize = int(self.view.cb_pagesize.GetValue() or 100)
        except ValueError:
            pagesize = 0
        if pagenum is None:
            pagenum = self.view.spin_pagenum.GetValue()
        where, tail = self.build_where()
        rec_count = self.model.count(where, tail)
        maxpagecount = int(rec_count / (pagesize <= 0 and 1 or pagesize))
        if maxpagecount == 1:
            maxpagecount = 2
        elif maxpagecount <= 0:
            maxpagecount = 1
        if DEBUG:
            log_debug("where statement=", where, tail)
            log_debug("pagesize=", pagesize)
            log_debug("pagenum=", pagenum)
            log_debug("maxpagecount=", maxpagecount)
        self.view.label_pagenum.SetLabel(" / " + str(maxpagecount))
        self.view.spin_pagenum.SetRange(1, maxpagecount)
        items = self.get_search_items(where, tail, pagesize, pagenum)
        if DEBUG:
            log_debug("RECORDS=", items)
        self.view.obj_list_view.SetObjects([dict(x) for x in items])
        self.view.spin_pagenum.Bind(wx.EVT_SPINCTRL, self.on_page_changed)
        self.view.obj_list_view.AutoSizeColumns()

    def on_page_changed(self, evt=None):
        pagenum = self.view.spin_pagenum.GetValue()
        if pagenum > self.view.spin_pagenum.GetMax():
            return
        return self.on_search(pagenum=pagenum)

    def get_search_items(self, where, tail, pagesize, pagenum):
        return self.model.search(where, tail, pagesize, pagenum > 0 and pagenum or 1)

    def build_where(self):
        fields_meta = self.get_fields_meta()
        if DEBUG:
            log_debug(fields_meta)
        search_values = {i: v["valueGetter"](v["widget"])
                         for i, v in fields_meta.items()
                         if v["valueGetter"](v["widget"])}
        tail = {}
        where = []
        for s, v in search_values.items():
            qry, tl = build_qry_where(v, s)
            if qry is not None:
                where.append(qry)
                tail.update(tl)
            else:
                where.append("1=0")
        if where:
            where = " WHERE " + " AND ".join(where)
        else:
            where = ""
        return where, self.tail_modifier(tail)

    def tail_modifier(self, tail):
        if not isinstance(tail, dict):
            return tail
        fields_meta = self.get_fields_meta()
        for key, value in tail.items():
            func = fields_meta.get(key, {}).get("valueConverter")
            if func is not None:
                tail[key] = func(value)
        return tail

    def on_clear(self, evt=None):
        self.view.obj_list_view.SetObjects(None)


class BaseHistLogPanel(wx.Panel):
    def __init__(self, parent, tablename="",
                 manager_cls=BaseHistLogManager,
                 db_cls=MainSql):
        super(BaseHistLogPanel, self).__init__(parent)
        self.manager_cls = manager_cls
        self.manager = manager_cls(view=self, tablename=tablename, db_cls=db_cls)
        self._fields = {}

    def create_widgets(self):
        with self.busy_cursor():
            return self.manager.create_widgets()

    def get_panel_name(self):
        return self.manager.get_panel_name()

    def busy_cursor(self):
        return wx.BusyCursor()


# main table manager
class TableMainHistLogManager(BaseHistLogManager):

    def get_panel_name(self):
        return "main " + _("Table's Log")

    def get_sizer_colnum(self):
        return 3


# main table panel
class TableMainHistLogPanel(BaseHistLogPanel):
    def __init__(self, parent, tablename="mainLog",
                 manager_cls=TableMainHistLogManager,
                 db_cls=MainSql):
        super(TableMainHistLogPanel, self).__init__(parent,
                                                    tablename=tablename,
                                                    manager_cls=manager_cls,
                                                    db_cls=db_cls)


# subrecords table manager
class TableSubrecordsHistLogManager(BaseHistLogManager):

    def get_panel_name(self):
        return "subrecords " + _("Table's Log")

    def get_sizer_colnum(self):
        return 3


# subrecords table panel
class TableSubrecordsHistLogPanel(BaseHistLogPanel):
    def __init__(self, parent, tablename="subrecordsLog",
                 manager_cls=TableSubrecordsHistLogManager,
                 db_cls=MainSql):
        super(TableSubrecordsHistLogPanel, self).__init__(parent,
                                                        tablename=tablename,
                                                        manager_cls=manager_cls,
                                                        db_cls=db_cls)


class RepDevStatusModel(BaseModel):

    def fields_meta(self):
        fields_meta = super(RepDevStatusModel, self).fields_meta()
        for row in fields_meta:
            if row["name"] == "status_duration_sec":
                row["type"] = "INTEGER"
            elif row["name"] == "status_duration_day":
                row["type"] = "REAL"
            elif row["name"] in ("id", "registryNo", "status"):
                row["type"] = "TEXT"
            elif row["name"] in ("timestamp1", "timestamp2"):
                row["type"] = "TEXT"
            elif not row["type"]:
                row["type"] = "TEXT"
        return fields_meta

    def search(self, where, tail, pagesize, pagenum, order_by=""):
        return super(RepDevStatusModel, self).search(where, tail, pagesize,
                                                     pagenum, order_by)


# repDevStatus view manager
class ViewRepDevStatusManager(BaseHistLogManager):

    def _fix_columns(self):
        self.view.obj_list_view.SetColumnWidth(5, wx.LIST_AUTOSIZE_USEHEADER)
        self.view.obj_list_view.SetColumnWidth(6, wx.LIST_AUTOSIZE_USEHEADER)

    def create_widgets(self):
        ret = super(ViewRepDevStatusManager, self).create_widgets()
        self._fix_columns()
        return ret

    def on_search(self, evt=None, pagenum=None):
        ret = super(ViewRepDevStatusManager, self).on_search(evt=evt,
                                                             pagenum=pagenum)
        self._fix_columns()
        return ret

    def get_model_cls(self):
        return RepDevStatusModel

    def get_panel_name(self):
        return _("Device Status Duration Report")

    def get_sizer_colnum(self):
        return 3


# repDevStatus view panel
class ViewRepDevStatusPanel(BaseHistLogPanel):
    def __init__(self, parent, tablename="repDevStatus",
                 manager_cls=ViewRepDevStatusManager,
                 db_cls=MainSql):
        super(ViewRepDevStatusPanel, self).__init__(parent,
                                                    tablename=tablename,
                                                    manager_cls=manager_cls,
                                                    db_cls=db_cls)


class RepDevOwnerModel(BaseModel):

    def fields_meta(self):
        fields_meta = super(RepDevOwnerModel, self).fields_meta()
        for row in fields_meta:
            if row["name"] == "owner_duration_sec":
                row["type"] = "INTEGER"
            elif row["name"] == "owner_duration_day":
                row["type"] = "REAL"
            elif row["name"] in ("id", "registryNo", "owner"):
                row["type"] = "TEXT"
            elif row["name"] in ("timestamp1", "timestamp2"):
                row["type"] = "TEXT"
            elif not row["type"]:
                row["type"] = "TEXT"
        return fields_meta

    def search(self, where, tail, pagesize, pagenum, order_by=""):
        return super(RepDevOwnerModel, self).search(where, tail, pagesize,
                                                    pagenum, order_by)


# repDevStatus view manager
class ViewRepDevOwnerManager(BaseHistLogManager):

    def _fix_columns(self):
        self.view.obj_list_view.SetColumnWidth(5, wx.LIST_AUTOSIZE_USEHEADER)
        self.view.obj_list_view.SetColumnWidth(6, wx.LIST_AUTOSIZE_USEHEADER)

    def create_widgets(self):
        ret = super(ViewRepDevOwnerManager, self).create_widgets()
        self._fix_columns()
        return ret

    def on_search(self, evt=None, pagenum=None):
        ret = super(ViewRepDevOwnerManager, self).on_search(evt=evt,
                                                            pagenum=pagenum)
        self._fix_columns()
        return ret

    def get_model_cls(self):
        return RepDevOwnerModel

    def get_panel_name(self):
        return _("Device Owner Duration Report")

    def get_sizer_colnum(self):
        return 3


# repDevOwner view panel
class ViewRepDevOwnerPanel(BaseHistLogPanel):
    def __init__(self, parent, tablename="repDevOwner",
                 manager_cls=ViewRepDevOwnerManager,
                 db_cls=MainSql):
        super(ViewRepDevOwnerPanel, self).__init__(parent,
                                                    tablename=tablename,
                                                    manager_cls=manager_cls,
                                                    db_cls=db_cls)


# repMainLogStatus view manager
class ViewRepMainLogStatusManager(BaseHistLogManager):

    def get_panel_name(self):
        return _("Device Status Log")

    def get_sizer_colnum(self):
        return 3


# repMainLogStatus view panel
class ViewRepMainLogStatusPanel(BaseHistLogPanel):
    def __init__(self, parent, tablename="repMainLogStatus",
                 manager_cls=ViewRepMainLogStatusManager,
                 db_cls=MainSql):
        super(ViewRepMainLogStatusPanel, self).__init__(parent,
                                                    tablename=tablename,
                                                    manager_cls=manager_cls,
                                                    db_cls=db_cls)


# repMainLogComingDate view manager
class ViewRepMainLogComingDateManager(BaseHistLogManager):

    def get_panel_name(self):
        return _("Calibration Date Log")

    def get_sizer_colnum(self):
        return 3


# repMainLogStatus view panel
class ViewRepMainLogComingDatePanel(BaseHistLogPanel):
    def __init__(self, parent, tablename="repMainLogComingDate",
                 manager_cls=ViewRepMainLogComingDateManager,
                 db_cls=MainSql):
        super(ViewRepMainLogComingDatePanel, self).__init__(parent,
                                                    tablename=tablename,
                                                    manager_cls=manager_cls,
                                                    db_cls=db_cls)


class HistoryLogBook(wx.Choicebook):
    def __init__(self, parent):
        super(HistoryLogBook, self).__init__(parent, -1)
        win = wx.Panel(self)

        wx.StaticText(win, -1,
                      _("Please select appropriate window from choices above"),
                      pos=(10, 15))

        self.AddPage(win, "")
        win = ViewRepDevStatusPanel(parent=self)
        self.AddPage(win, win.get_panel_name())
        win = ViewRepDevOwnerPanel(parent=self)
        self.AddPage(win, win.get_panel_name())
        win = ViewRepMainLogStatusPanel(parent=self)
        self.AddPage(win, win.get_panel_name())
        win = ViewRepMainLogComingDatePanel(parent=self)
        self.AddPage(win, win.get_panel_name())
        win = TableMainHistLogPanel(parent=self)
        self.AddPage(win, win.get_panel_name())
        win = TableSubrecordsHistLogPanel(parent=self)
        self.AddPage(win, win.get_panel_name())
        self.Bind(wx.EVT_CHOICEBOOK_PAGE_CHANGING, self.OnPageChanging)

    def OnPageChanging(self, evt):
        new = evt.GetSelection()
        win = self.GetPage(new)
        if not getattr(win, "widgets_created", False) and \
            hasattr(win, "create_widgets"):
            win.create_widgets()
            win.widgets_created = True
        evt.Skip()


class HistoryLogFrame(wx.Frame):

    def __init__(self, parent):
        super(HistoryLogFrame, self).__init__(parent, size=(1000, 768))
        self.SetIcon(get_icon())
        self.SetTitle(_("History Log"))
        self.add_menu()
        panel = wx.Panel(self)
        notebook = HistoryLogBook(panel)
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(notebook, 1, wx.ALL | wx.EXPAND, 5)
        panel.SetSizer(sizer)
        self.CenterOnScreen()
        self.Show()

    def add_menu(self):
        menubar = wx.MenuBar()
        helpmenu = wx.Menu()
        id_search = wx.NewId()
        helpmenu.Append(id_search, _("How to &Search") + "\tCtrl+Shift+S")
        menubar.Append(helpmenu, _("&Help"))
        self.SetMenuBar(menubar)
        self.Bind(wx.EVT_MENU, self.on_search_help, id=id_search)

    def on_search_help(self, evt):
        size = list(self.GetSizeTuple())
        size[0] -= 100
        size[1] -= 100
        help_frame = wx.Frame(self, size=size)
        help_frame.SetIcon(get_icon())
        help_win = HelpWindow(help_frame)
        help_win.SetRelatedFrame(help_frame, "%s")
        help_win.SetRelatedStatusBar(0)
        help_win.SetPage(get_html_help())
        help_frame.Center()
        help_frame.Show()
