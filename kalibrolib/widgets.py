# encoding: utf-8
from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import

import os

import wx


class MinimizeApp(wx.TaskBarIcon):

    def __init__(self, icon, tooltip, frame):
        wx.TaskBarIcon.__init__(self)
        try:
            self.SetIcon(icon, tooltip)
        except wx.PyAssertionError:
            # windows ico file generates error on Linux, fallback to png
            if os.path.exists("kalibro.png"):
                icon = wx.Icon("kalibro.png", wx.BITMAP_TYPE_PNG)
            else:
                icon = wx.NullIcon
            self.SetIcon(icon, tooltip)
        self.frame = frame
        self.Bind(wx.EVT_TASKBAR_LEFT_DCLICK, self.onLeftDClick)

    def onLeftDClick(self, event):
        if self.frame.IsIconized():
            self.frame.Iconize(False)
        if not self.frame.IsShown():
            self.frame.Show(True)
        self.frame.Raise()

    def CreatePopUpMenu(self):
        """Add menu Later"""
        return None

    def removeAll(self):
        self.RemoveIcon()
        self.Destroy()
