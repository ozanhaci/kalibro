# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import os
import datetime
import sqlite3
from distutils.version import StrictVersion

from .debugging import DEBUG, debug_trace, log_debug
from .alphanum import alphanum

__all__ = ["MainSql", "IntegrityError"]

str = unicode
IntegrityError = sqlite3.IntegrityError


def conv_datetime(dtin):
    dtlen = len(dtin)
    if dtlen == 19 and " " in dtin:
        return datetime.datetime.strptime(dtin, "%Y-%m-%d %H:%M:%S")
    elif dtlen > 19 and " " in dtin:
        return datetime.datetime.strptime(dtin, "%Y-%m-%d %H:%M:%S.%f")
    else:
        return datetime.datetime.strptime(dtin, "%Y-%m-%d")

sqlite3.register_converter(b"DATETIME", conv_datetime)
sqlite3.enable_callback_tracebacks(True)


class MainSql(object):

    new_db = None

    def __init__(self, db="kalibro.sqlite", forceCreate=False):
        self.dbName = db
        self.colNames = []
        self.result = []
        if MainSql.new_db is None:
            if db == ":memory:":
                MainSql.new_db = True
            elif not os.path.isfile(db):
                MainSql.new_db = True
            else:
                MainSql.new_db = False
        self.db = sqlite3.connect(db, isolation_level=None,
                                  detect_types=sqlite3.PARSE_DECLTYPES |
                                  sqlite3.PARSE_COLNAMES)
        self.db.row_factory = sqlite3.Row
        self.db.create_collation(b"naturalsort", alphanum)
        self.db.create_function(b"get_user", 0, get_user)
        self.db.execute('''PRAGMA foreign_keys=ON''')
        self._created = False
        if forceCreate:
            self.createDB()
            self._created = True
        if MainSql.new_db is True:
            if db != ":memory:":
                MainSql.new_db = bool(os.path.isfile(db))

    def execute(self, query, tail=()):
        self.cursor = self.db.cursor()
        self.cursor.execute(query, tail)

    def execStmt(self, query, tail=()):
        self.cursor = self.db.cursor()
        self.cursor.execute(query, tail)

    def fetch_all(self, query, tail=()):
        self.cursor = self.db.cursor()
        self.cursor.execute(query, tail)
        self.result = self.cursor.fetchall()
        return self

    def getColNames(self):
        self.colNames = [i[0] for i in self.cursor.description]
        return tuple(self.colNames)

    def optimizeDB(self):
        initSize = os.path.getsize(self.dbName)
        self.db.execute("VACUUM")
        finalSize = os.path.getsize(self.dbName)
        return initSize, finalSize

    def closeConn(self):
        try:
            self.cursor.close()
        except AttributeError:
            pass
        self.db.close()

    def createDB(self):
        self.cursor = self.db.cursor()
        self._fix_add_field_labels()
        version = getattr(__import__('__main__'), "__version__", "")
        verTbl = '''CREATE TABLE IF NOT EXISTS "version"
            ("version" TEXT);'''
        if DEBUG:
            log_debug(debug_trace(), verTbl)
        self.cursor.execute(verTbl)
        selVer = '''SELECT version FROM version LIMIT 1'''
        if DEBUG:
            log_debug(debug_trace(), selVer)
        self.cursor.execute(selVer)
        verRes = self.cursor.fetchall()
        if version and verRes and len(verRes) > 0 and len(verRes[0]) > 0:
            if StrictVersion(version) <= StrictVersion(verRes[0][0]):
                updStmt = '''UPDATE version SET version=?'''
                self.cursor.execute(updStmt, (version,))
        else:
            insStmt = '''INSERT INTO version (version) VALUES (?)'''
            self.cursor.execute(insStmt, (version,))

        statusTbl = '''CREATE TABLE IF NOT EXISTS "status" (
                    "name" TEXT PRIMARY KEY NOT NULL COLLATE NOCASE)'''
        if DEBUG:
            log_debug(debug_trace(), statusTbl)
        self.cursor.execute(statusTbl)

        resultTbl = '''CREATE TABLE IF NOT EXISTS "result" (
                    "name" TEXT PRIMARY KEY NOT NULL COLLATE NOCASE)'''
        if DEBUG:
            log_debug(debug_trace(), resultTbl)
        self.cursor.execute(resultTbl)
        # add owner table in version 2.6
        ownerTbl = '''CREATE TABLE IF NOT EXISTS "owner" (
                   "name" TEXT PRIMARY KEY NOT NULL COLLATE NOCASE)'''
        if DEBUG:
            log_debug(debug_trace(), ownerTbl)
        self.cursor.execute(ownerTbl)

        # create auth table in version 2.6
        authTbl = '''CREATE TABLE IF NOT EXISTS auth (
            userName TEXT PRIMARY KEY NOT NULL,
            nameSurname TEXT,
            password TEXT NOT NULL,
            write INTEGER DEFAULT 0 NOT NULL,
            lastSeen TIMESTAMP);'''
        if DEBUG:
            log_debug(debug_trace(), authTbl)
        self.cursor.execute(authTbl)

        self.cursor.execute('pragma table_info("auth");')
        authInfo = self.cursor.fetchall()
        needUpgrade = "extra" not in [n[b"name"] for n in authInfo]
        if needUpgrade:
            upgradeSubSql = '''ALTER TABLE "auth" ADD COLUMN "extra" TEXT'''
            if DEBUG:
                log_debug(debug_trace(), upgradeSubSql)
            self.cursor.execute(upgradeSubSql)

        mainTbl = '''CREATE TABLE IF NOT EXISTS "main"
            ("registryNo" TEXT PRIMARY KEY  NOT NULL ,
             "deviceName" TEXT,
             "deviceManuf" TEXT,
             "manufSerialNo" TEXT,
             "features" TEXT,
             "comingDate" TIMESTAMP,
             "description" TEXT,
             "status" TEXT CONSTRAINT status_fk
             REFERENCES status ("name") ON UPDATE CASCADE ON DELETE RESTRICT,
             "owner" TEXT CONTRAINT owner_fk
             REFERENCES owner ("name") ON UPDATE CASCADE ON DELETE RESTRICT)
        '''
        if DEBUG:
            log_debug(debug_trace(), mainTbl)
        self.cursor.execute(mainTbl)
        self.cursor.execute('''pragma table_info(main)''')
        mainRes = self.cursor.fetchall()
        needUpgrade = "status" not in [n[b"name"] for n in mainRes]
        if needUpgrade:
            upgradeMainSql = '''ALTER TABLE "main" ADD COLUMN "status" TEXT COLLATE NOCASE
                CONSTRAINT "status_fk"
                REFERENCES status (name)
                ON UPDATE CASCADE ON DELETE RESTRICT'''
            if DEBUG:
                log_debug(debug_trace(), upgradeMainSql)
            self.cursor.execute(upgradeMainSql)
        # add column owner version 2.6
        needUpgrade = "owner" not in [n[b"name"] for n in mainRes]
        if needUpgrade:
            upgradeMainSql = '''ALTER TABLE "main" ADD COLUMN "owner" TEXT COLLATE NOCASE
                CONSTRAINT "owner_fk"
                REFERENCES owner (name)
                ON UPDATE CASCADE ON DELETE RESTRICT'''
            if DEBUG:
                log_debug(debug_trace(), upgradeMainSql)
            self.cursor.execute(upgradeMainSql)
        # add new column result version 2.5
        calibTbl = '''CREATE TABLE IF NOT EXISTS "subrecords"
            ("id" INTEGER NOT NULL , "registryNo" TEXT NOT NULL ,
            "plannedDate" TIMESTAMP, "realizedDate" TIMESTAMP,
            "trackingNo" TEXT, "method" TEXT, "result" TEXT,
            "path" TEXT,
            PRIMARY KEY ("id", "registryNo"),
            FOREIGN KEY ("registryNo") REFERENCES "main"("registryNo")
            FOREIGN KEY ("result") REFERENCES "result"("name")
            ON UPDATE CASCADE ON DELETE CASCADE)'''
        if DEBUG:
            log_debug(debug_trace(), calibTbl)
        self.cursor.execute(calibTbl)
        # create new column "path" version 2.5
        self.cursor.execute('''pragma table_info(subrecords)''')
        mainRes = self.cursor.fetchall()
        needUpgrade = "path" not in [n[b"name"] for n in mainRes]
        if needUpgrade:
            upgradeSubSql = '''ALTER TABLE "subrecords" ADD COLUMN "path" TEXT'''
            if DEBUG:
                log_debug(debug_trace(), upgradeSubSql)
            self.cursor.execute(upgradeSubSql)
        # create new column "result" version 2.5
        needUpgrade = "result" not in [n[b"name"] for n in mainRes]
        if needUpgrade:
            upgradeSubSql = '''ALTER TABLE "subrecords" ADD COLUMN "result" TEXT COLLATE NOCASE
                CONSTRAINT "result_fk"
                REFERENCES result (name)
                ON UPDATE CASCADE ON DELETE RESTRICT'''
            if DEBUG:
                log_debug(debug_trace(), upgradeSubSql)
            self.cursor.execute(upgradeSubSql)

        reportFileBlobTbl = '''CREATE TABLE IF NOT EXISTS "reportBlobData"
            ("id" INTEGER NOT NULL , "registryNo" TEXT NOT NULL ,
            "reportNo" TEXT NOT NULL , "fileName" TEXT,
            "fileData" BLOB, PRIMARY KEY ("id", "registryNo"),
            FOREIGN KEY ("registryNo") REFERENCES "main"("registryNo")
            ON UPDATE CASCADE ON DELETE CASCADE)'''
        if DEBUG:
            log_debug(debug_trace(), reportFileBlobTbl)
        self.cursor.execute(reportFileBlobTbl)
        # create new mainAddField table version 2.4
        mainAddFieldTbl = '''CREATE TABLE IF NOT EXISTS "mainAddField"
            ("registryNo" TEXT PRIMARY KEY  NOT NULL ,
            "IntValue" INTEGER,
            "TextValue1" TEXT,
            "TextValue2" TEXT,
            "DateValue" TIMESTAMP,
            FOREIGN KEY ("registryNo") REFERENCES "main"("registryNo")
            ON UPDATE CASCADE ON DELETE CASCADE)'''
        if DEBUG:
            log_debug(debug_trace(), mainAddFieldTbl)
        self.cursor.execute(mainAddFieldTbl)
        # create new mainAddFieldLabel table and records version 2.4
        self._fix_add_field_labels()
        # create log tables and triggers
        self._install_logging()
        # create views
        self._install_views()
        # update version string with the current one
        upStmt = '''UPDATE "version" SET version=?'''
        self.cursor.execute(upStmt, (version,))
        # copy records from old mainAdditional table to mainAddField table except new date field
        self.cursor.execute('''pragma table_info(mainAdditional)''')
        oldAddTbl = self.cursor.fetchall()
        # if mainAdditional table does not exist, stop processing
        if not oldAddTbl:
            return
        copyStmt = '''INSERT INTO "mainAddField" (registryNo,IntValue,TextValue1,TextValue2)
            SELECT registryNo,IntValue,VcharValue,TextValue FROM mainAdditional;'''
        if DEBUG:
            log_debug(debug_trace(), copyStmt)
        self.cursor.execute(copyStmt)
        dropStmt = '''DROP TABLE "mainAdditional";'''
        if DEBUG:
            log_debug(debug_trace(), dropStmt)
        self.cursor.execute(dropStmt)

    def _install_logging(self):
        logMainTblSql = '''
        CREATE TABLE IF NOT EXISTS "mainLog" (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        old_registryNo TEXT,
        new_registryNo TEXT,
        old_deviceName TEXT,
        new_deviceName TEXT,
        old_deviceManuf TEXT,
        new_deviceManuf TEXT,
        old_manufSerialNo TEXT,
        new_manufSerialNo TEXT,
        old_features TEXT,
        new_features TEXT,
        old_comingDate TIMESTAMP,
        new_comingDate TIMESTAMP,
        old_description TEXT,
        new_description TEXT,
        old_status TEXT,
        new_status TEXT,
        old_owner TEXT,
        new_owner TEXT,
        operation TEXT,
        user TEXT DEFAULT (get_user()),
        timestamp TIMESTAMP NOT NULL DEFAULT (datetime('now','localtime'))
        );
        '''
        triggerMainSql = '''
        DROP TRIGGER IF EXISTS "log_main_ai";
        DROP TRIGGER IF EXISTS "log_main_au";
        DROP TRIGGER IF EXISTS "log_main_ad";
        CREATE TRIGGER IF NOT EXISTS "log_main_ai" AFTER INSERT ON "main" FOR EACH ROW
        BEGIN
             INSERT INTO "mainLog"
             (new_registryNo, new_deviceName,new_deviceManuf,new_manufSerialNo,
              new_features,new_comingDate,new_description,new_status,new_owner,operation)
            VALUES
             (NEW.registryNo, NEW.deviceName, NEW.deviceManuf, NEW.manufSerialNo,
              NEW.features,NEW.comingDate,NEW.description,NEW.status,NEW.owner,"INSERT");
        END;
        CREATE TRIGGER IF NOT EXISTS "log_main_au" AFTER UPDATE ON "main" FOR EACH ROW
        WHEN
        (IFNULL(OLD.registryNo,"")!=IFNULL(NEW.registryNo,"")
         OR IFNULL(OLD.deviceName,"")!=IFNULL(NEW.deviceName,"")
         OR IFNULL(OLD.deviceManuf,"")!=IFNULL(NEW.deviceManuf,"")
         OR IFNULL(OLD.manufSerialNo,"")!=IFNULL(NEW.manufSerialNo,"")
         OR IFNULL(OLD.features,"")!=IFNULL(NEW.features,"")
         OR IFNULL(OLD.comingDate,"")!=IFNULL(NEW.comingDate,"")
         OR IFNULL(OLD.description,"")!=IFNULL(NEW.description,"")
         OR IFNULL(OLD.status,"")!=IFNULL(NEW.status,"")
         OR IFNULL(OLD.owner,"")!=IFNULL(NEW.owner,""))
        BEGIN
             INSERT INTO "mainLog"
             (old_registryNo,new_registryNo,old_deviceName,new_deviceName,
              old_deviceManuf,new_deviceManuf,old_manufSerialNo,new_manufSerialNo,
              old_features,new_features,old_comingDate,new_comingDate,
              old_description,new_description,old_status,new_status,
              old_owner,new_owner,operation)
            VALUES
             (OLD.registryNo,NEW.registryNo,OLD.deviceName,NEW.deviceName,
              OLD.deviceManuf,NEW.deviceManuf,OLD.manufSerialNo,NEW.manufSerialNo,
              OLD.features,NEW.features,OLD.comingDate,NEW.comingDate,
              OLD.description,NEW.description,OLD.status,NEW.status,
              OLD.owner,NEW.owner,"UPDATE");
        END;
        CREATE TRIGGER IF NOT EXISTS "log_main_ad" AFTER DELETE ON "main" FOR EACH ROW 
        BEGIN
             INSERT INTO "mainLog"
             (old_registryNo, old_deviceName,old_deviceManuf,old_manufSerialNo,
              old_features,old_comingDate,old_description,old_status,old_owner,operation)
            VALUES
             (OLD.registryNo, OLD.deviceName, OLD.deviceManuf, OLD.manufSerialNo,
              OLD.features,OLD.comingDate,OLD.description,OLD.status,OLD.owner,"DELETE");
        END;
        '''
        self.cursor.executescript(logMainTblSql)
        self.cursor.executescript(triggerMainSql)

        logSubrecordsTbl = '''
        CREATE TABLE IF NOT EXISTS "subrecordsLog" (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        old_id INTEGER,
        new_id INTEGER,
        old_registryNo TEXT,
        new_registryNo TEXT,
        old_plannedDate TIMESTAMP,
        new_plannedDate TIMESTAMP,
        old_realizedDate TIMESTAMP,
        new_realizedDate TIMESTAMP,
        old_trackingNo TEXT,
        new_trackingNo TEXT,
        old_method TEXT,
        new_method TEXT,
        old_result TEXT,
        new_result TEXT,
        old_path TEXT,
        new_path TEXT,
        operation TEXT,
        user TEXT DEFAULT (get_user()),
        timestamp TIMESTAMP NOT NULL DEFAULT (datetime('now','localtime'))
        );
        '''
        triggerSubrecordsSql = '''
        DROP TRIGGER IF EXISTS "log_subrecords_ai";
        DROP TRIGGER IF EXISTS "log_subrecords_au";
        DROP TRIGGER IF EXISTS "log_subrecords_ad";
        CREATE TRIGGER IF NOT EXISTS "log_subrecords_ai" AFTER INSERT ON "subrecords" FOR EACH ROW 
        BEGIN
             INSERT INTO "subrecordsLog"
             (new_id,new_registryNo,new_plannedDate,new_realizedDate,new_trackingNo,
              new_method,new_result,new_path,operation)
            VALUES
             (NEW.id,NEW.registryNo,NEW.plannedDate,NEW.realizedDate,NEW.trackingNo,
              NEW.method,NEW.result,NEW.path,"INSERT");
        END;
        CREATE TRIGGER IF NOT EXISTS "log_subrecords_au" AFTER UPDATE ON "subrecords" FOR EACH ROW
        WHEN
        (IFNULL(OLD.id,"")!=IFNULL(NEW.id,"")
         OR IFNULL(OLD.registryNo,"")!=IFNULL(NEW.registryNo,"")
         OR IFNULL(OLD.plannedDate,"")!=IFNULL(NEW.plannedDate,"")
         OR IFNULL(OLD.realizedDate,"")!=IFNULL(NEW.realizedDate,"")
         OR IFNULL(OLD.trackingNo,"")!=IFNULL(NEW.trackingNo,"")
         OR IFNULL(OLD.method,"")!=IFNULL(NEW.method,"")
         OR IFNULL(OLD.result,"")!=IFNULL(NEW.result,"")
         OR IFNULL(OLD.path,"")!=IFNULL(NEW.path,""))
        BEGIN
             INSERT INTO "subrecordsLog"
             (old_id,new_id,old_registryNo,new_registryNo,old_plannedDate,
              new_plannedDate,old_realizedDate,new_realizedDate,
              old_trackingNo,new_trackingNo,old_method,new_method,
              old_result,new_result,old_path,new_path,operation)
            VALUES
             (OLD.id,NEW.id,OLD.registryNo,NEW.registryNo,OLD.plannedDate,
              NEW.plannedDate,OLD.realizedDate,NEW.realizedDate,
              OLD.trackingNo,NEW.trackingNo,OLD.method,NEW.method,
              OLD.result,NEW.result,OLD.path,NEW.path,"UPDATE");
        END;
        CREATE TRIGGER IF NOT EXISTS "log_subrecords_ad" AFTER DELETE ON "subrecords" FOR EACH ROW 
        BEGIN
             INSERT INTO "subrecordsLog"
             (old_id,old_registryNo,old_plannedDate,old_realizedDate,old_trackingNo,
              old_method,old_result,old_path,operation)
            VALUES
             (OLD.id,OLD.registryNo,OLD.plannedDate,OLD.realizedDate,OLD.trackingNo,
              OLD.method,OLD.result,OLD.path,"DELETE");
        END;
        '''
        self.cursor.executescript(logSubrecordsTbl)
        self.cursor.executescript(triggerSubrecordsSql)

        logMainAddTblSql = '''
        CREATE TABLE IF NOT EXISTS "mainAddFieldLog" (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        old_registryNo TEXT,
        new_registryNo TEXT,
        old_IntValue INTEGER,
        new_IntValue INTEGER,
        old_TextValue1 TEXT,
        new_TextValue1 TEXT,
        old_TextValue2 TEXT,
        new_TextValue2 TEXT,
        old_DateValue TIMESTAMP,
        new_DateValue TIMESTAMP,
        operation TEXT,
        user TEXT DEFAULT (get_user()),
        timestamp TIMESTAMP NOT NULL DEFAULT (datetime('now','localtime'))
        );
        '''
        triggerMainAddSql = '''
        DROP TRIGGER IF EXISTS "log_mainAddField_ai";
        DROP TRIGGER IF EXISTS "log_mainAddField_au";
        DROP TRIGGER IF EXISTS "log_mainAddField_ad";
        CREATE TRIGGER IF NOT EXISTS "log_mainAddField_ai" AFTER INSERT ON "mainAddField" FOR EACH ROW
        BEGIN
             INSERT INTO "mainAddFieldLog"
             (new_registryNo, new_IntValue,new_TextValue1,new_TextValue2,
              new_DateValue,operation)
            VALUES
             (NEW.registryNo,NEW.IntValue,NEW.TextValue1,NEW.TextValue2,
              NEW.DateValue,"INSERT");
        END;
        CREATE TRIGGER IF NOT EXISTS "log_mainAddField_au" AFTER UPDATE ON "mainAddField" FOR EACH ROW
        WHEN
        (IFNULL(OLD.registryNo,"")!=IFNULL(NEW.registryNo,"")
         OR IFNULL(OLD.IntValue,"")!=IFNULL(NEW.IntValue,"")
         OR IFNULL(OLD.TextValue1,"")!=IFNULL(NEW.TextValue1,"")
         OR IFNULL(OLD.TextValue2,"")!=IFNULL(NEW.TextValue2,"")
         OR IFNULL(OLD.DateValue,"")!=IFNULL(NEW.DateValue,""))
        BEGIN
             INSERT INTO "mainAddFieldLog"
             (old_registryNo,new_registryNo,old_IntValue,new_IntValue,
              old_TextValue1,new_TextValue1,old_TextValue2,new_TextValue2,
              old_DateValue,new_DateValue,operation)
            VALUES
             (OLD.registryNo,NEW.registryNo,OLD.IntValue,NEW.IntValue,
              OLD.TextValue1,NEW.TextValue1,OLD.TextValue2,NEW.TextValue2,
              OLD.DateValue,NEW.DateValue,"UPDATE");
        END;
        CREATE TRIGGER IF NOT EXISTS "log_mainAddField_ad" AFTER DELETE ON "mainAddField" FOR EACH ROW 
        BEGIN
             INSERT INTO "mainAddFieldLog"
             (old_registryNo,old_IntValue,old_TextValue1,old_TextValue2,
              old_DateValue,operation)
            VALUES
             (OLD.registryNo,OLD.IntValue,OLD.TextValue1,OLD.TextValue2,
              OLD.DateValue,"DELETE");
        END;
        '''
        self.cursor.executescript(logMainAddTblSql)
        self.cursor.executescript(triggerMainAddSql)

        logMainAddLabelTblSql = '''
        CREATE TABLE IF NOT EXISTS "mainAddFieldLabelLog" (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        old_IntLabel TEXT,
        new_IntLabel TEXT,
        old_TextLabel1 TEXT,
        new_TextLabel1 TEXT,
        old_TextLabel2 TEXT,
        new_TextLabel2 TEXT,
        old_DateLabel TEXT,
        new_DateLabel TEXT,
        operation TEXT,
        user TEXT DEFAULT (get_user()),
        timestamp TIMESTAMP NOT NULL DEFAULT (datetime('now','localtime'))
        );
        '''
        triggerMainAddLabelSql = '''
        DROP TRIGGER IF EXISTS "log_mainAddFieldLabel_au";
        CREATE TRIGGER IF NOT EXISTS "log_mainAddFieldLabel_au" AFTER UPDATE ON "mainAddFieldLabel" FOR EACH ROW
        WHEN
        (IFNULL(OLD.IntLabel,"")!=IFNULL(NEW.IntLabel,"")
         OR IFNULL(OLD.TextLabel1,"")!=IFNULL(NEW.TextLabel1,"")
         OR IFNULL(OLD.TextLabel2,"")!=IFNULL(NEW.TextLabel2,"")
         OR IFNULL(OLD.DateLabel,"")!=IFNULL(NEW.DateLabel,""))
        BEGIN
             INSERT INTO "mainAddFieldLabelLog"
             (old_IntLabel,new_IntLabel,old_TextLabel1,new_TextLabel1,
              old_TextLabel2,new_TextLabel2,old_DateLabel,new_DateLabel,
              operation)
            VALUES
             (OLD.IntLabel,NEW.IntLabel,OLD.TextLabel1,NEW.TextLabel1,
              OLD.TextLabel2,NEW.TextLabel2,OLD.DateLabel,NEW.DateLabel,
              "UPDATE");
        END;
        '''
        self.cursor.executescript(logMainAddLabelTblSql)
        self.cursor.executescript(triggerMainAddLabelSql)

    def _install_views(self):
        repDevStatusView = '''
        DROP VIEW IF EXISTS "repDevStatus";
        CREATE VIEW IF NOT EXISTS "repDevStatus" AS
        SELECT id,registryNo,status,
        timestamp1,timestamp2,
        ROUND(CASE WHEN CAST(status_duration_sec AS REAL)/(24*60*60)>0 THEN CAST(status_duration_sec AS REAL)/(24*60*60) ELSE 0 END,1) "status_duration_day",
        status_duration_sec FROM (
        SELECT id1||"-"||id2 "id",old_registryNo "registryNo",old_status "status",timestamp1,timestamp2,strftime("%s",datetime(timestamp2))-strftime("%s",datetime(timestamp1)) "status_duration_sec"
        FROM (
        WITH status_rep AS (
        SELECT   mainLog.id id1,
              status_change.id id2,
              mainLog.old_registryNo,
              mainLog.new_status old_status,
              status_change.new_status,
              mainLog.timestamp timestamp1,
              status_change.timestamp timestamp2
                 
         FROM     
             mainLog,
             mainLog status_change
         WHERE   
         mainLog.new_status=status_change.old_status AND 
         mainLog.timestamp<status_change.timestamp
         AND      mainLog.old_registryNo=status_change.old_registryNo
         AND      mainLog.old_registryNo=mainLog.new_registryNo
         AND      mainLog.old_status!=mainLog.new_status
        AND mainLog.new_status!=status_change.new_status
        ORDER BY timestamp1 DESC, timestamp2 DESC
        )
        SELECT s.* FROM status_rep s WHERE s.timestamp2 = (SELECT MIN(r.timestamp2) FROM status_rep r WHERE r.id1=s.id1)
        UNION
           SELECT   m.id,
                    m.id,
                    m.old_registryNo,
                    m.new_status,
                    NULL,
                    m.timestamp,
                    datetime('now','localtime')
           FROM     mainLog m
           WHERE    timestamp =
                    (SELECT MAX(k.timestamp)
                     FROM   mainLog k
                     WHERE  k.old_registryNo=m.old_registryNo AND k.old_registryNo=k.new_registryNo )
                    AND m.old_registryNo IS NOT NULL
        )
        )
        ORDER BY timestamp1 DESC, timestamp2 DESC;      
        '''
        self.cursor.executescript(repDevStatusView)

        repDevOwnerView = '''
        DROP VIEW IF EXISTS "repDevOwner";
        CREATE VIEW IF NOT EXISTS "repDevOwner" AS
        SELECT id,registryNo,owner,
        timestamp1,timestamp2,
        ROUND(CASE WHEN CAST(owner_duration_sec AS REAL)/(24*60*60)>0 THEN CAST(owner_duration_sec AS REAL)/(24*60*60) ELSE 0 END,1) "owner_duration_day",
        owner_duration_sec FROM (
        SELECT id1||"-"||id2 "id",old_registryNo "registryNo",old_owner "owner",timestamp1,timestamp2,strftime("%s",datetime(timestamp2))-strftime("%s",datetime(timestamp1)) "owner_duration_sec"
        FROM (
        WITH owner_rep AS (
        SELECT   mainLog.id id1,
              owner_change.id id2,
              mainLog.old_registryNo,
              mainLog.new_owner old_owner,
              owner_change.new_owner,
              mainLog.timestamp timestamp1,
              owner_change.timestamp timestamp2
         FROM     
             mainLog,
             mainLog owner_change
         WHERE   
         mainLog.new_owner=owner_change.old_owner AND 
         mainLog.timestamp<owner_change.timestamp
         AND      mainLog.old_registryNo=owner_change.old_registryNo
         AND      mainLog.old_registryNo=mainLog.new_registryNo
         AND      mainLog.old_owner!=mainLog.new_owner
        AND mainLog.new_owner!=owner_change.new_owner
        ORDER BY timestamp1 DESC, timestamp2 DESC
        )
        SELECT s.* FROM owner_rep s WHERE s.timestamp2 = (SELECT MIN(r.timestamp2) FROM owner_rep r WHERE r.id1=s.id1)
        UNION
           SELECT   m.id,
                    m.id,
                    m.old_registryNo,
                    m.new_owner,
                    NULL,
                    m.timestamp,
                    datetime('now','localtime')
           FROM     mainLog m
           WHERE    timestamp =
                    (SELECT MAX(k.timestamp)
                     FROM   mainLog k
                     WHERE  k.old_registryNo=m.old_registryNo AND k.old_registryNo=k.new_registryNo )
                    AND m.old_registryNo IS NOT NULL
        )
        )
        ORDER BY timestamp1 DESC, timestamp2 DESC;            
        '''
        self.cursor.executescript(repDevOwnerView)

        mainLogStatus = '''
        CREATE VIEW IF NOT EXISTS "repMainLogStatus" AS 
            SELECT id,new_registryNo "registryNo",new_deviceName "deviceName",
            old_status,new_status,user,timestamp FROM mainLog
            WHERE IFNULL(old_status,"")!=IFNULL(new_status,"");
        '''
        self.cursor.executescript('''
        DROP VIEW IF EXISTS "repMainLogStatus";
        ''')
        self.cursor.executescript(mainLogStatus)

        mainLogComingDate = '''
        CREATE VIEW IF NOT EXISTS "repMainLogComingDate" AS 
            SELECT id,new_registryNo "registryNo",new_deviceName "deviceName",
            old_comingDate,new_comingDate,user,timestamp FROM mainLog
            WHERE IFNULL(old_comingDate,"")!=IFNULL(new_comingDate,"");
        '''
        self.cursor.executescript('''
        DROP VIEW IF EXISTS "repMainLogComingDate";
        ''')
        self.cursor.executescript(mainLogComingDate)

    def _fix_add_field_labels(self):
        # insert default values for additional field labels
        selStmt = '''SELECT COUNT(*) FROM mainAddFieldLabel'''
        if DEBUG:
            log_debug(debug_trace(), selStmt)
        try:
            self.cursor.execute(selStmt)
        except sqlite3.OperationalError:
            mainAddLabelTbl = '''CREATE TABLE IF NOT EXISTS "mainAddFieldLabel"
            ("IntLabel" TEXT,
             "TextLabel1" TEXT,
             "TextLabel2" TEXT,
             "DateLabel" TEXT)'''
            if DEBUG:
                log_debug(debug_trace(), mainAddLabelTbl)
            self.cursor.execute(mainAddLabelTbl)
            return self._fix_add_field_labels()
        resCount = self.cursor.fetchall()
        if not resCount or (resCount and resCount[0][0] < 1):
            insStmt = '''INSERT INTO "mainAddFieldLabel" (IntLabel,TextLabel1,TextLabel2,DateLabel)
                VALUES ("Integer Field","Text Field 1","Text Field 2","Date Field");'''
            if DEBUG:
                log_debug(debug_trace(), insStmt)
            self.cursor.execute(insStmt)

# due to circular import, import get_user function at the end
from .auth import get_user
