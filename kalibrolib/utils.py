# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import
import sys
import os
import datetime
import csv
import codecs
import errno
import tempfile
import re
import urlparse
import urllib
try:
    from cStringIO import StringIO
except ImportError:
    from io import StringIO
from base64 import b64decode

import wx

from .constants import IS_WINDOWS
from .debugging import DEBUG, log_debug, debug_decorator

str = unicode


def fix_report_dt(inpt, dtfmt):
    if isinstance(inpt, (datetime.datetime, datetime.date)):
        try:
            return inpt.strftime(dtfmt)
        except ValueError:
            return _("INVALID DATE")
    elif inpt is None:
        return ""
    elif isinstance(inpt, basestring):
        try:
            return datetime.datetime.strptime(inpt, "%Y-%m-%d %H:%M:%S").strftime(dtfmt)
        except ValueError:
            try:
                return datetime.datetime.strptime(inpt, "%Y-%m-%d").strftime(dtfmt)
            except ValueError:
                pass
        return inpt


def fix_report_timestamp(inpt, dtfmt):
    fmt = dtfmt + " %H:%M:%S"
    if isinstance(inpt, (datetime.datetime, datetime.date)):
        try:
            return inpt.strftime(fmt)
        except ValueError:
            return _("INVALID DATE")
    elif inpt is None:
        return ""
    elif isinstance(inpt, basestring):
        try:
            return datetime.datetime.strptime(inpt, "%Y-%m-%d %H:%M:%S").strftime(fmt)
        except ValueError:
            try:
                return datetime.datetime.strptime(inpt, "%Y-%m-%d").strftime(fmt)
            except ValueError:
                pass
        return inpt


def fix_none(inpt):
    if inpt is None:
        return ""
    return inpt


def fix_file_path(path):
    if isinstance(path, basestring) and path:
        return path2url(path)
    return path


def fix_basename(path):
    try:
        return os.path.basename(path)
    except:
        return path


def str_result(result, dateformat):
    data = []
    for rec in result:
        temp = []
        for i in rec:
            if isinstance(i, (datetime.date, datetime.datetime)):
                i = fix_report_dt(i, dateformat)
            elif i is None:
                i = ""
            elif isinstance(i, (float, int, long)) or not isinstance(i, basestring):
                i = str(i)
            temp.append(i)
        data.append(temp)
    return data


def path2url(path):
    return urlparse.urljoin(
      'file:', urllib.pathname2url(path))


def sanitize_space(inpt):
    return re.sub(r"\s+", " ", inpt, re.I | re.U | re.M)

@debug_decorator
def coltr(col, table="main"):
    try:
        return coltr.dict.get(table + "." + col, col)
    except AttributeError:
        coltr.dict = {"main.registryNo": _("Registry No"),
                      "main.deviceName": _("Device Name"),
                      "main.deviceManuf": _("Device Manufacturer"),
                      "main.manufSerialNo": _("Manufacturer Serial No"),
                      "main.features": _("Device Feature"),
                      "main.comingDate": _("Next Calibration Date"),
                      "main.description": _("Description"),
                      "main.status": _("Device Status"),
                      "main.owner": _("Device Owner"),
                      "subrecords.id": _("ID"),
                      "subrecords.registryNo": _("Registry No"),
                      "subrecords.plannedDate": _("Planned Calibration Date"),
                      "subrecords.realizedDate": _("Realized Calibration Date"),
                      "subrecords.trackingNo": _("Report No"),
                      "subrecords.method": _("Method"),
                      "subrecords.result": _("Result"),
                      "subrecords.path": _("File Path"),
                      "auth.userName": _("User Name"),
                      "auth.nameSurname": _("Name Surname"),
                      "auth.password": _("Password"),
                      "auth.write": _("Write Permission"),
                      "auth.lastSeen": _("Last Seen"),
                      "auth.extra": _("Extra"),
                      "mainAddFieldLabel.IntLabel": _("Integer Field Label"),
                      "mainAddFieldLabel.TextLabel1": _("1st Text Field Label"),
                      "mainAddFieldLabel.TextLabel2": _("2nd Text Field Label"),
                      "mainAddFieldLabel.DateLabel": _("Date Field Label")
                      }
        d = coltr.dict
        new = "(" + _("NEW") + ") "
        old = "(" + _("OLD") + ") "
        ID = _("ID")
        user = _("User")
        operation = _("Operation")
        timestamp = _("Timestamp")
        logTableDict = {}
        for k, v in d.items():
            tbl, column = k.split(".")
            tbl = tbl + "Log"
            logTableDict[tbl + ".old_" + column] = old + v
            logTableDict[tbl + ".new_" + column] = new + v
            if (tbl + ".id") not in logTableDict:
                logTableDict[tbl + ".id"] = ID
                logTableDict[tbl + ".user"] = user
                logTableDict[tbl + ".operation"] = operation
                logTableDict[tbl + ".timestamp"] = timestamp
        d.update(logTableDict)
        viewDict = {"repDevStatus.id": d["subrecords.id"],
                    "repDevStatus.registryNo": d["main.registryNo"],
                    "repDevStatus.status": d["main.status"],
                    "repDevStatus.timestamp1": d["mainLog.timestamp"] + "1",
                    "repDevStatus.timestamp2": d["mainLog.timestamp"] + "2",
                    "repDevStatus.status_duration_day": _("Status Duration (days)"),
                    "repDevStatus.status_duration_sec": _("Status Duration (secs)"),
                    "repDevOwner.id": d["subrecords.id"],
                    "repDevOwner.registryNo": d["main.registryNo"],
                    "repDevOwner.owner": d["main.owner"],
                    "repDevOwner.timestamp1": d["mainLog.timestamp"] + "1",
                    "repDevOwner.timestamp2": d["mainLog.timestamp"] + "2",
                    "repDevOwner.owner_duration_day": _("Owner Duration (days)"),
                    "repDevOwner.owner_duration_sec": _("Owner Duration (secs)"),
                    "repMainLogComingDate.id": d["subrecords.id"],
                    "repMainLogComingDate.registryNo": d["main.registryNo"],
                    "repMainLogComingDate.deviceName": d["main.deviceName"],
                    "repMainLogComingDate.old_comingDate": d["mainLog.old_comingDate"],
                    "repMainLogComingDate.new_comingDate": d["mainLog.new_comingDate"],
                    "repMainLogComingDate.user": d["auth.userName"],
                    "repMainLogComingDate.timestamp": d["mainLog.timestamp"],
                    "repMainLogStatus.id": d["subrecords.id"],
                    "repMainLogStatus.registryNo": d["main.registryNo"],
                    "repMainLogStatus.deviceName": d["main.deviceName"],
                    "repMainLogStatus.old_status": d["mainLog.old_status"],
                    "repMainLogStatus.new_status": d["mainLog.new_status"],
                    "repMainLogStatus.user": d["auth.userName"],
                    "repMainLogStatus.timestamp": d["mainLog.timestamp"],
                    }
        d.update(viewDict)
        return coltr.dict.get(table + "." + col, col)


class UnicodeWriter(object):
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8",
                 lineterminator=os.linesep, **kw):
        # Redirect output to a queue
        self.queue = StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect,
                                 lineterminator=lineterminator, **kw)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        self.writer.writerow([s.encode("utf-8") for s in row])
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)


def isWritable(path):
    '''
    Check directory is writable
    
    :returns: True or False
    :rtype: boolean
    '''
    if not os.path.isdir(path):
        return False
    try:
        testfile = tempfile.TemporaryFile(dir=path)
        testfile.write("...")
        testfile.close()
    except Exception as e:
        if hasattr(e, "errno") and e.errno == errno.EACCES:
            return False
        else:
            if DEBUG:
                log_debug(str(e))
            return False
    return True


def get_icon():
    ic = wx.NullIcon
    if not hasattr(sys, "frozen"):
        if IS_WINDOWS:
            ic = wx.Icon('kalibro.ico', wx.BITMAP_TYPE_ICO)
        else:
            ic = wx.Icon('kalibro.png', wx.BITMAP_TYPE_PNG)
    else:
        ic = wx.Icon(sys.executable + ";0", wx.BITMAP_TYPE_ICO)
    return ic


def getCalendarButtonImage():
    png = b64decode(calbut_png_64)
    pngstream = StringIO(png)
    return pngstream


calbut_png_64 = '''\
iVBORw0KGgoAAAANSUhEUgAAABYAAAAWCAYAAADEtGw7AAAABGdBTUEAALGOfPtRkwAAACBjSFJN
AAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAACXBIWXMAAAsRAAALEQF/ZF+R
AAAFqUlEQVRIS5WVCUzTVxzHH8UqoGzzmsMDOdSBopuAJoszblNnNtEYI3MznlNJNjWyLIsuEPBg
c0438JibcogIQg8uQaZyy2UrpaVKKVCgXC0tpS0ttLSUfveKC4uOLdsv+eW9/8t7n/d97/d7/58T
+cucQglhTKfuQYgTHWa8Qls7bR0O6kxCnKfRdtQx5kYYLiAMhgtxdqbzmNOIzWYlA3tVxETX0unU
rnsQt8xlrqty/SZvz/Jm7uZ6MsMyPBjhnDmME5zZ5BTndXKeO5Nc5nhNT8rydE3PnUdyCxaShwWL
SfVDfyIqXk4aileSytTF5DAVR/f+0+74M5fx3pla0LBiSn+dl7NB4O1srvVhjNYtckL9Eic89Sfg
r/fCo+jjqPz6UwiObULTvkVQ7HCG6jOCvn0Eil0EnEDC3U7IwnEwJ8A1WLjGTSxfPBnSWQRNcwma
vQhalhC0BdD+Sgbq4yJQK6oHr7oKDaJa1CXHoPPALKgotO8gQfcegoxgkrOFEO9xcJofM6g60EXU
tHASGmYQSOYQNC6gG/hSOFXb8JYzWnJSIWlphaSpGUqdAYICFuSHPaHeS6Ch4B7asla9BE72ZQaV
+k0WSd5wRsOrFDybgudRpd4EMqpa5u+Eqv0bUPrgPvJuJ6EoPRlVEZ9AuvU1SLe4QbJ1GoQfuyNh
6ZQXFV/zYAbleruLnsyfidq5c8D3nQ9+gA94wW+Cty4AtSHBKA5dh18P7cKl/aG4eewgHl+IhDTx
Z8i4KegoLEBrQT7Svgh7ERyzfHlg8uGwutqrVyFLSYI8JxPdpUXoFfDR19gInVwOo0qFbtpvrhdD
090Fq04LS083rEoFLF1yGKQS5Jw98yI4+oNNgWnJWQLloA12u33MRy0WWChgWNaModoaDD15DGuz
FIMPczFUVgSTsAba29dhljyDiVeC/sJ7yI6OmgCcwBYojNZx8IjBANNTMSwdrdBzEqHj3MGIRgXj
Aw4MRQ8wolVDz07BCFVu0/RCW12JzKjIfwbbqFqHWyl4uFUGu22EbiDEwO/5z/tCAfSZbJgbGqCO
uwAtO51+c6DOyfoXxQYLRkwmWIxGmDrboaMAfRYb6ovnoPjmOPrjr0H57XH0xkRDl3oDypNHYCwv
xmBpLtQF2ciMjp5AcSJH0KXUYlBYhyGqSptxA+pLP2KwqgzahJ+gvZMMm04DQ2EmjJVlGDUbqNI0
WHuVYwHsq6oAN3Kiq7jJFXS0K2CSyWjghmESPIL+biY9vo32eRTCovdNo19cCG1qMg0mb+wkeppB
ei4Likwu2BMFL9UBllMwXTD8TAxDXg7U35+GoSAPmis/ofd0BPTZ9C5/iIDml1gM3M2GMurEWLYY
y4qhzM/7OziGptutBI6A36aBhJWFLhYLKlY6upMSoOdXQJscC23aLVgVXTQruDCWFsJmGIAuIxUj
6l6aIVqoKivAiox46YE48pimm1hjwX3FMCr7zegZMqOxrRMdHZ0wiQQYyM2i92qmOc0fC6qptQX9
nAzoy8swUF8PlVgM9qlTOZsJ8Rn/CTkUp8SzBW16K9TDdgi0drQarBAJhGgsK4WuqhL9udkYEImg
pZD+Rik0rXKourqhbG8f6ZA2Donu5fdf2bsrcS0hC8bB597/MCgpniPk9dnQZbZDbbWjZ9CCOqEY
4uoa9MraIafPuYn3eLi+pERblZ3Tcf+3G+LUqKiK2M/3c6I2ro/90m/JVxunu6yZT4jrOPi71auX
xsdcLikXtdr5z2SoKa+012RnD3Lj4hSpZ04/pYDSS0ePsqNDQy+eXLc2PMzfd+fmqVPeCyBkBYVQ
FqFVjNDK9ZJFe3i4nf8oZNvlA2EpZ3fuZoWHbIsN27Ah/FDQ2zv2+Cx4d9NU5rKlhHjSZTOoOxTR
MvcfjdaqyUfc3WeGEDJr0XMFDsAk6o7C+r/tD56UELpFTUitAAAAAElFTkSuQmCC
'''
