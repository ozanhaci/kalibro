# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import os
import sys
import unittest
import sqlite3
import six
import wx

from .wtc import WidgetTestCase
from ..conn_sqlite import MainSql
from .. import pubsub
from ..basicdata.owner import OwnerModel, OwnerWin
from ..basicdata.devstatus import DevStatusModel, DevStatusWin
from ..basicdata.calresult import CalibrationResultModel, CalibrationResultWin

if six.PY3:
    def text(in_):
        return in_

elif six.PY2:
    def text(in_):
        return bytes(in_)


class MockMainSql(MainSql):

    def __init__(self, db=":memory:", forceCreate=True):
        super(MockMainSql, self).__init__(db=db, forceCreate=forceCreate)


class MockWin(object):

    def msgbox(self, msg, caption, flag=wx.ICON_INFORMATION):
        assert isinstance(msg, six.string_types)
        assert isinstance(caption, six.string_types)
        assert isinstance(flag, six.integer_types)
        sys.stderr.write("\n" + caption + ":" + msg + "\n")
        if flag & wx.ICON_ERROR:
            raise Exception(msg)


class MockOwnerWin(MockWin, OwnerWin):
    pass


class MockDevStatusWin(MockWin, DevStatusWin):
    pass


class MockCalResultWin(MockWin, CalibrationResultWin):
    pass


class MockEvent(object):

    def Skip(self):
        pass


def gettext(s):
    return s


def setUpModule():
    debug = os.environ.pop("DEBUG", None)
    if debug is not None:
        os.environ["_DEBUG"] = debug
    os.environ["TEST"] = "1"
    import __builtin__
    __builtin__.__dict__['_'] = gettext


def tearDownModule():
    os.environ.pop("TEST", None)
    debug = os.environ.pop("_DEBUG", None)
    if debug is not None:
        os.environ["DEBUG"] = debug


def get_baseclass():

    class BaseTests(WidgetTestCase):

        _model = None
        _mock_sql = None
        _mock_win = None

        def setUp(self):
            pubsub.unsubscribe_all()
            super(BaseTests, self).setUp()
            self.view = self._mock_win(parent=self.frame,
                                       manager_model_cls=self._model,
                                       manager_db_cls=self._mock_sql)
            self.view.PostSizeEvent()

        def click_save(self):
            clickEvent = wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED,
                                         self.view.btnSave.GetId())
            self.view.btnSave.ProcessEvent(clickEvent)

        def click_delete(self):
            clickEvent = wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED,
                                         self.view.btnDelete.GetId())
            self.view.btnDelete.ProcessEvent(clickEvent)

        def test_view(self):
            view = self.view
            self.assertTrue(view.IsShown())
            view.txtCtrl.SetValue("test")
            evt = MockEvent()
            view.add_new_rec(evt)
            model = view.manager.model
            self.assertIn("test", model.get_all())

            view.listBox.SetSelection(0)
            evt = MockEvent()
            view.delete_rec(evt)
            self.assertListEqual([], model.get_all())

            view.txtCtrl.SetValue("test2")
            self.click_save()
            self.assertIn("test2", model.get_all())
            view.listBox.SetSelection(0)
            self.assertFalse(self.view.btnDelete.Enabled)
            self.click_delete()
            self.assertNotIn("test2", model.get_all())

        def test_manager(self):
            called = [False]
            def inner():
                called[0] = True
            view = self.view
            pubsub.subscribe(view.manager.update_topic, inner)
            view.txtCtrl.SetValue("test")
            manager = view.manager
            manager.add_new_rec()
            self.assertTrue(called[0])
            model = view.manager.model
            self.assertIn("test", model.get_all())

            view.listBox.SetSelection(0)
            manager.delete_rec()
            self.assertListEqual([], model.get_all())

        def test_model(self):
            db = self._mock_sql()
            model = self._model(db=db)
            model.add(param="test1")
            model.add(param="test2")
            records = model.get_all()
            self.assertIn("test1", records)
            self.assertIn("test2", records)
            model.delete(param="test1")
            records = model.get_all()
            self.assertNotIn("test1", records)
            self.assertIn("test2", records)
            model.delete(param="test2")
            records = model.get_all()
            self.assertNotIn("test1", records)
            self.assertNotIn("test2", records)

            model.add(param="test1")
            with self.assertRaises(sqlite3.IntegrityError):
                model.add(param="test1")

    return BaseTests


def class_factory():
    classes = []
    for (name, datamodel, mockdbcls, mockwin) \
        in [("owner", OwnerModel, MockMainSql, MockOwnerWin),
            ("calResult", CalibrationResultModel, MockMainSql, MockCalResultWin),
            ("devStatus", DevStatusModel, MockMainSql, MockDevStatusWin)
            ]:
        testcls = type(text(name.title() + 'BasicDataTests'), (get_baseclass(),),
                       {text("_model"): datamodel,
                        text("_mock_sql"): mockdbcls,
                        text("_mock_win"): mockwin})
        classes.append(testcls)
    return classes


def suite():
    classes = tuple(map(lambda x: unittest.makeSuite(x), class_factory()))
    return unittest.TestSuite(classes)

