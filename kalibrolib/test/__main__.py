# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import sys
from unittest import runner
from unittest.signals import installHandler

from . import suite as test_suites


def test():
    installHandler()  # enable CTRL+C
    test_runner = runner.TextTestRunner(verbosity=2,
                                        failfast=True,
                                        buffer=False,
                                        )
    result = test_runner.run(test_suites())
    sys.exit(not result.wasSuccessful())


if __name__ == "__main__":
    test()
