# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import unittest
import doctest

from . import test_search
from . import test_constants
from . import test_utils
from . import test_images
from . import test_db
from . import test_version
from . import test_pubsub
from . import test_basicdata
from . import test_config
from . import test_versionctrl
from . import test_searchwin

# doctest modules
from .. import alphanum


def doctest_suite():
    suite = unittest.TestSuite()
    for mod in [alphanum]:
        try:
            suite.addTest(doctest.DocTestSuite(mod))
        except ValueError:
            pass
    return suite


def suite():
    return unittest.TestSuite(tuple([test_basicdata.suite(),
                                     test_search.suite(),
                                     test_searchwin.suite(),
                                     test_constants.suite(),
                                     test_images.suite(),
                                     test_db.suite(),
                                     test_version.suite(),
                                     test_versionctrl.suite(),
                                     test_pubsub.suite(),
                                     test_utils.suite(),
                                     test_config.suite(),
                                     doctest_suite(),
                                     ]))

