# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import os
import sys
import unittest
import datetime
import codecs

import six


def setUpModule():
    debug = os.environ.pop("DEBUG", None)
    if debug is not None:
        os.environ["_DEBUG"] = debug
    os.environ["TEST"] = "1"


def tearDownModule():
    os.environ.pop("TEST", None)
    debug = os.environ.pop("_DEBUG", None)
    if debug is not None:
        os.environ["DEBUG"] = debug


class BaseConstantsTests(unittest.TestCase):

    def setUp(self):
        from .. import constants
        self.delete_module_name = constants.__name__
        self.consmod = constants

    def tearDown(self):
        self.consmod = None
        del self.consmod
        if self.delete_module_name in sys.modules.keys():
            del sys.modules[self.delete_module_name]


class ConstantsTests(BaseConstantsTests):

    def test_exists(self):
        self.consmod.VERSION
        self.consmod.AUTHOR
        self.consmod.AUTHOR_EMAIL
        self.consmod.HOME_PAGE_URL
        self.consmod.HELP_PAGE_URL
        self.consmod.VERSION_CONTROL_URL
        self.consmod.DONATION_URL
        self.consmod.IS_LINUX
        self.consmod.IS_WINDOWS
        self.consmod.VERSION_CHECK
        self.consmod.DEF_LANG
        self.consmod.DEF_DATEFORMAT
        self.consmod.DEF_FILE_ENCODING

    def test_values(self):
        self.assertIsInstance(self.consmod.VERSION, six.string_types)
        if self.consmod.VERSION != "":
            self.assertRegexpMatches(self.consmod.VERSION, "[0-9]+\.\w+")
        self.assertIsInstance(self.consmod.IS_LINUX, bool)
        self.assertIsInstance(self.consmod.IS_WINDOWS, bool)
        self.assertIsInstance(self.consmod.VERSION_CHECK, bool)
        self.assertEqual(self.consmod.IS_LINUX + self.consmod.IS_WINDOWS, 1)
        self.assertIsInstance(self.consmod.DEF_LANG, six.string_types)
        self.assertGreater(len(self.consmod.DEF_LANG), 0)

        now = datetime.datetime.now()
        datetime.datetime.strftime(now, self.consmod.DEF_DATEFORMAT)
        codecs.getdecoder(self.consmod.DEF_FILE_ENCODING)
        codecs.getencoder(self.consmod.DEF_FILE_ENCODING)

        self.assertRegexpMatches(self.consmod.HOME_PAGE_URL, "(?i)http[s]?://.*")
        self.assertRegexpMatches(self.consmod.HELP_PAGE_URL, "(?i)http[s]?://.*")
        self.assertRegexpMatches(self.consmod.VERSION_CONTROL_URL, "(?i)http[s]?://.*")
        self.assertRegexpMatches(self.consmod.AUTHOR_EMAIL, "\w+@\w+\.\w+")

def suite():
    st1 = unittest.makeSuite(ConstantsTests)
    return unittest.TestSuite((st1,))
