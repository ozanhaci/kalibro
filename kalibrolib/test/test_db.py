# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import os
import datetime
import sqlite3
import unittest

from .. import conn_sqlite as DB


def setUpModule():
    debug = os.environ.pop("DEBUG", None)
    if debug is not None:
        os.environ["_DEBUG"] = debug
    os.environ["TEST"] = "1"


def tearDownModule():
    os.environ.pop("TEST", None)
    debug = os.environ.pop("_DEBUG", None)
    if debug is not None:
        os.environ["DEBUG"] = debug


class DBFirstTests(unittest.TestCase):

    def setUp(self):
        self.main_mod = __import__("__main__")
        self.old_version = getattr(self.main_mod, "__version__", None)
        setattr(self.main_mod, "__version__", "1")
        self.sql = DB.MainSql(db=":memory:", forceCreate=True)

    def tearDown(self):
        self.sql.closeConn()
        self.sql = None
        del self.sql
        if self.old_version is not None:
            del self.main_mod.__version__
            setattr(self.main_mod, "__version__", self.old_version)
        self.main_mod = None
        del self.main_mod
        del self.old_version

    def test_conv_datetime(self):
        strlist = ["2016-01-01 12:00:00", "2016-01-31 00:01:00.00"]
        for input_ in strlist:
            res = DB.conv_datetime(input_)
            self.assertIsInstance(res, datetime.datetime)
        self.assertIsInstance(DB.conv_datetime("2016-12-31"), datetime.date)

    def test_fk_on(self):
        cursor = self.sql.db.cursor()
        cursor.execute("PRAGMA foreign_keys")
        self.assertEqual(cursor.fetchall()[0][0], 1)
        cursor.close()

    def test_db_created(self):
        self.assertTrue(self.sql._created)

    def test_conn_params(self):
        self.assertIsNone(self.sql.db.isolation_level)
        self.assertIs(self.sql.db.row_factory, sqlite3.Row)

    def test_collations(self):
        cursor = self.sql.db.cursor()
        cursor.execute("pragma collation_list;")
        cols = [i[1] for i in cursor.fetchall()]
        self.assertIn("NATURALSORT", cols)


class Helper(object):
    class ParentTestCase(unittest.TestCase):

        def setUp(self):
            self.main_mod = __import__("__main__")
            self.old_version = getattr(self.main_mod, "__version__", None)
            setattr(self.main_mod, "__version__", "1")
            self.sql = DB.MainSql(db=":memory:", forceCreate=True)

        def tearDown(self):
            self.sql.closeConn()
            self.sql = None
            del self.sql
            if self.old_version is not None:
                del self.main_mod.__version__
                setattr(self.main_mod, "__version__", self.old_version)
            self.main_mod = None
            del self.main_mod
            del self.old_version


class DBSecondTests(Helper.ParentTestCase):

    def test_table_exists(self):
        tables = ["version", "main", "subrecords", "mainAddField", "mainAddFieldLabel",
                  "reportBlobData", "result", "status"]
        for table in tables:
            self.assertEqual(self.get_tables(table)[0][0], table)

    def test_columns_exists(self):
        tablesdict = {"version": ["version"],
                      "main": ["registryNo", "deviceName", "deviceManuf",
                               "manufSerialNo", "features", "comingDate",
                               "description", "status", "owner"],
                      "subrecords": ["id", "registryNo", "plannedDate",
                                     "realizedDate", "trackingNo", "method",
                                     "result", "path"],
                      "mainAddField": ["registryNo", "IntValue", "TextValue1",
                                       "TextValue2", "DateValue"],
                      "mainAddFieldLabel": ["IntLabel", "TextLabel1",
                                            "TextLabel2", "DateLabel"],
                      "reportBlobData": ["id", "registryNo", "reportNo",
                                         "fileName", "fileData"],
                      "result": ["name"],
                      "status": ["name"],
                      }
        for table in tablesdict:
            info = self.get_table_info(table)
            columns = [i[b"name"] for i in info]
            self.assertListEqual(tablesdict[table], columns)

    def test_primary_keys(self):
        pkeys = {"main": ["registryNo"],
                 "subrecords": ["id", "registryNo"],
                 "mainAddField": ["registryNo"],
                 "reportBlobData": ["id", "registryNo"],
                 "result": ["name"],
                 "status": ["name"],
                 }
        for table in pkeys:
            info = self.get_table_info(table)
            keys = []
            for rec in info:
                recd = dict(rec)
                if recd["pk"] > 0:
                    keys.append(recd["name"])
            self.assertListEqual(pkeys[table], keys)

    def get_tables(self, tablename):
        cursor = self.sql.db.cursor()
        cursor.execute('SELECT name FROM sqlite_master WHERE type="table" AND name="%s"' % tablename)
        result = cursor.fetchall()
        cursor.close()
        return result

    def get_table_info(self, tablename):
        cursor = self.sql.db.cursor()
        cursor.execute('pragma table_info(%s);' % tablename)
        result = cursor.fetchall()
        cursor.close()
        return result


class DBThirdTests(Helper.ParentTestCase):

    def tearDown(self):
        if self.old_version is not None:
            del self.main_mod.__version__
            setattr(self.main_mod, "__version__", self.old_version)
        self.main_mod = None
        del self.main_mod
        del self.old_version

    def test_close_conn(self):
        self.sql.closeConn()
        try:
            self.sql.db.cursor()
        except sqlite3.ProgrammingError as e:
            self.assertEqual("Cannot operate on a closed database.", e.message)
        finally:
            self.sql = None
            del self.sql


class DBFourthTests(Helper.ParentTestCase):

    def test_attr_exists(self):
        DB.IntegrityError
        self.sql.createDB
        self.sql.execute
        self.sql.execStmt
        self.sql.fetch_all
        self.sql.getColNames
        self.sql.optimizeDB
        self.sql.closeConn
        self.sql._fix_add_field_labels
        self.sql._created
        self.sql.fetch_all("SELECT 1;")
        self.sql.cursor
        self.sql.result


def suite():
    st1 = unittest.makeSuite(DBFirstTests)
    st2 = unittest.makeSuite(DBSecondTests)
    st3 = unittest.makeSuite(DBThirdTests)
    st4 = unittest.makeSuite(DBFourthTests)
    return unittest.TestSuite((st1, st2, st3, st4))
