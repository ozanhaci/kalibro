# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import unittest
import wx

class WidgetTestCase(unittest.TestCase):
    """
    A testcase that will create an app and frame for various widget test
    modules to use. They can inherit from this class to save some work. This
    is also good for test cases that just need to have an application object
    created.
    """
    def setUp(self):
        self.app = wx.App()
        wx.Log.SetActiveTarget(wx.LogStderr())
        self.frame = wx.Frame(None, title='WTC: ' + self.__class__.__name__, size=(100, 100))
        self.frame.Show()
        self.frame.PostSizeEvent()
        wx.WakeUpIdle()

    def tearDown(self):
        def _cleanup():
            for tlw in wx.GetTopLevelWindows():
                if tlw:
                    if isinstance(tlw, wx.Dialog) and tlw.IsModal():
                        tlw.EndModal(0)
                        wx.CallAfter(tlw.Destroy)
                    else:
                        tlw.Close(force=True)
            wx.WakeUpIdle()
        timer = wx.PyTimer(_cleanup)
        timer.Start(100)
#        wx.CallAfter(_cleanup)
#        wx.CallAfter(_cleanup)
#        wx.CallAfter(self.app.Exit)
        self.app.MainLoop()
        self.app = None
        del self.app

    def closeDialogs(self):
        """
        Close dialogs by calling their EndModal method
        """
        for w in wx.GetTopLevelWindows():
            if isinstance(w, wx.Dialog):
                w.EndModal(wx.ID_CANCEL)

