# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import io
import unittest

from .. import mainwin
from . import wtc

version = "1.0"
version_orig = None
version_check = None


def setUpModule():
    global version_orig, version_check
    version_orig = mainwin.VERSION
    mainwin.VERSION = version
    mainwin.VERSION_CONTROL_URL
    version_check = mainwin.VERSION_CHECK
    mainwin.VERSION_CHECK = True


def tearDownModule():
    mainwin.VERSION = version_orig
    mainwin.VERSION_CHECK = version_check


class VersionControlTests(wtc.WidgetTestCase):

    def test_has_new_version(self):
        version_new = "2.0"
        opened = [False]
        called = [False]
        url = [""]

        def urlopen(u):
            opened[0] = True
            url[0] = u
            return io.StringIO(version_new)

        def on_new_version(evt):
            called[0] = True
            self.assertEqual(evt.version, version_new)

        self.frame.Bind(mainwin.EVT_NEW_VERSION, on_new_version)
        thr = mainwin.NewVersionThread(parentWin=self.frame, urlopen=urlopen, sleep=0)
        self.assertTrue(thr.daemon)
        thr.start()
        thr.join(1)
        self.assertTrue(opened[0])
        self.assertTrue(called[0])
        self.assertEqual(url[0], mainwin.VERSION_CONTROL_URL)

    def test_no_new_version(self):
        for version_new in ["1.0", "", "blabla"]:
            opened = [False]
            called = [False]
            url = [""]

            def urlopen(u):
                opened[0] = True
                url[0] = u
                return io.StringIO(version_new)

            def on_new_version(evt):
                called[0] = True
                self.assertEqual(evt.version, version_new)

            self.frame.Bind(mainwin.EVT_NEW_VERSION, on_new_version)
            thr = mainwin.NewVersionThread(parentWin=self.frame, urlopen=urlopen, sleep=0)
            thr.start()
            thr.join(1)
            self.assertTrue(opened[0])
            self.assertFalse(called[0])
            self.assertEqual(url[0], mainwin.VERSION_CONTROL_URL)


def suite():
    st1 = unittest.makeSuite(VersionControlTests)
    return unittest.TestSuite((st1,))
