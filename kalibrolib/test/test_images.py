# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import os
import unittest

import wx
from . import wtc
from .. import images


def setUpModule():
    debug = os.environ.pop("DEBUG", None)
    if debug is not None:
        os.environ["_DEBUG"] = debug
    os.environ["TEST"] = "1"


def tearDownModule():
    os.environ.pop("TEST", None)
    debug = os.environ.pop("_DEBUG", None)
    if debug is not None:
        os.environ["DEBUG"] = debug


class ImagesTests(wtc.WidgetTestCase):

    def test_images(self):
        bmp = images.SmallDnArrow.GetBitmap()
        self.assertIsInstance(bmp, wx.Bitmap)
        bmp = images.SmallUpArrow.GetBitmap()
        self.assertIsInstance(bmp, wx.Bitmap)


def suite():
    st1 = unittest.makeSuite(ImagesTests)
    return unittest.TestSuite((st1,))
