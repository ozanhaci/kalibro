# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import os
import sys
import unittest

import six


def setUpModule():
    debug = os.environ.pop("DEBUG", None)
    if debug is not None:
        os.environ["_DEBUG"] = debug
    os.environ["TEST"] = "1"


def tearDownModule():
    os.environ.pop("TEST", None)
    debug = os.environ.pop("_DEBUG", None)
    if debug is not None:
        os.environ["DEBUG"] = debug


class SearchWinQryWhereTests(unittest.TestCase):

    def setUp(self):
        from .. import search
        self.delete_module_name = search.__name__
        self.build_qry_where = search.build_qry_where

    def tearDown(self):
        if self.delete_module_name in sys.modules.keys():
            del sys.modules[self.delete_module_name]


    def test_qry_where1(self):
        str_exps = ["=ABC", " =ABC", "= ABC", "= ABC", " = ABC"]
        colname = "test"
        for str_exp in str_exps:
            where = self.build_qry_where(str_exp, colname)
            self.check_types(where)
            qry, tail = where
            self.assertEqual(qry, " {0} = :{0} ".format(colname))
            self.assertIn("test", tail)
            self.assertEqual(tail["test"], "ABC")

    def test_qry_where2(self):
        str_exp = "!"
        colname = "test"
        where = self.build_qry_where(str_exp, colname)
        self.check_types(where)
        qry, tail = where
        self.assertEqual(qry, " ifnull(\"%s\",'')='' " % colname)
        self.assertEqual(len(tail), 0)

    def test_qry_where3(self):
        str_exps = ["!=ABC", " !=ABC", "!= ABC", "!= ABC", " != ABC"]
        colname = "test"
        for str_exp in str_exps:
            where = self.build_qry_where(str_exp, colname)
            self.check_types(where)
            qry, tail = where
            self.assertEqual(qry, " {0} != :{0} ".format(colname))
            self.assertIn("test", tail)
            self.assertEqual(tail["test"], "ABC")

    def test_qry_where4(self):
        str_exps = ["<=1001", " <=1001", "<= 1001", "<= 1001 ", " <= 1001 "]
        colname = "test"
        for str_exp in str_exps:
            where = self.build_qry_where(str_exp, colname)
            self.check_types(where)
            qry, tail = where
            self.assertEqual(qry, " {0} <= :{0} ".format(colname))
            self.assertIn("test", tail)
            self.assertEqual(tail["test"], "1001")

    def test_qry_where5(self):
        str_exps = [">=1001", " >=1001", ">= 1001", ">= 1001 ", " >= 1001 "]
        colname = "test"
        for str_exp in str_exps:
            where = self.build_qry_where(str_exp, colname)
            self.check_types(where)
            qry, tail = where
            self.assertEqual(qry, " {0} >= :{0} ".format(colname))
            self.assertIn("test", tail)
            self.assertEqual(tail["test"], "1001")

    def test_qry_where6(self):
        str_exps = ["<1001", " <1001", "< 1001", "< 1001 ", " < 1001 "]
        colname = "test"
        for str_exp in str_exps:
            where = self.build_qry_where(str_exp, colname)
            self.check_types(where)
            qry, tail = where
            self.assertEqual(qry, " {0} < :{0} ".format(colname))
            self.assertIn("test", tail)
            self.assertEqual(tail["test"], "1001")

    def test_qry_where7(self):
        str_exps = [">1001", " >1001", "> 1001", "> 1001 ", " > 1001 "]
        colname = "test"
        for str_exp in str_exps:
            where = self.build_qry_where(str_exp, colname)
            self.check_types(where)
            qry, tail = where
            self.assertEqual(qry, " {0} > :{0} ".format(colname))
            self.assertIn("test", tail)
            self.assertEqual(tail["test"], "1001")

    def test_qry_where8(self):
        str_exps = ["ABC;DEF", "ABC;DEF;", ";ABC;DEF;", ";ABC;DEF; ", " ;ABC;DEF; ", " ;ABC;DEF;"]
        colname = "test"
        for str_exp in str_exps:
            where = self.build_qry_where(str_exp, colname)
            self.check_types(where)
            qry, tail = where
            self.assertRegexpMatches(qry, "\( {0} LIKE \:{0}\w* OR {0} LIKE \:{0}\w* \)".format(colname))
            self.assertIn("%ABC%", tail.values())
            self.assertIn("%DEF%", tail.values())

    def test_qry_where9(self):
        str_exps = ["ABC;DEF..GHI", "ABC;DEF .. GHI", ";ABC;DEF..GHI ", ";ABC;DEF..GHI;"]
        colname = "test"
        for str_exp in str_exps:
            where = self.build_qry_where(str_exp, colname)
            self.check_types(where)
            qry, tail = where
            self.assertRegexpMatches(qry, "\( {0} LIKE \:{0}\w* OR {0} BETWEEN \:{0}\w*_first AND \:{0}\w*_second \)".format(colname))
            self.assertIn("%ABC%", tail.values())
            self.assertIn("DEF", tail.values())
            self.assertIn("GHI", tail.values())

    def test_qry_where10(self):
        str_exps = ["ABC..DEF", " ABC..DEF ", " ABC ..DEF", "ABC.. DEF ", " ABC .. DEF "]
        colname = "test"
        for str_exp in str_exps:
            where = self.build_qry_where(str_exp, colname)
            self.check_types(where)
            qry, tail = where
            self.assertRegexpMatches(qry, " {0} BETWEEN \:{0}\w*_first AND \:{0}\w*_second ".format(colname))
            self.assertIn("ABC", tail.values())
            self.assertIn("DEF", tail.values())

    def test_qry_where11(self):
        params = [(None, None, AssertionError), ("", None, AssertionError),
                  (None, "", AssertionError)]
        for p in params:
            with self.assertRaises(p[2]):
                self.build_qry_where(*p[:2])
        params = [("", ""), (" ", ""), (" ", " ")]
        for p in params:
            where = self.build_qry_where(*p)
            self.assertEqual(where, (None, None))

    def check_types(self, where):
        self.assertIsInstance(where, tuple)
        self.assertIsInstance(where[0], six.string_types)
        self.assertIsInstance(where[1], dict)


def suite():
    st = unittest.makeSuite(SearchWinQryWhereTests)
    return unittest.TestSuite((st,))
