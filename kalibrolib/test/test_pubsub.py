# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import weakref
import threading
import unittest

from .. import pubsub


RLOCK = threading.RLock()


class PubsubTests(unittest.TestCase):

    def setUp(self):
        with RLOCK:
            self.old_registry = pubsub.MyPubSub._registry
            pubsub.MyPubSub._registry = {}

    def tearDown(self):
        pubsub.unsubscribe_all()
        with RLOCK:
            pubsub.MyPubSub._registry = self.old_registry

    def test_pubsub1(self):
        called = [False]
        def subfunc():
            called[0] = True
        pubsub.subscribe("test1", subfunc)
        for i in range(10):
            pubsub.publish("test1")
            self.assertTrue(called[0])
            called[0] = False
        called[0] = False
        with RLOCK:
            self.assertListEqual(pubsub.MyPubSub._registry["test1"], [weakref.ref(subfunc)])
        pubsub.unsubscribe("test1", subfunc)
        pubsub.publish("test1")
        self.assertFalse(called[0])
        with RLOCK:
            self.assertEqual(len(pubsub.MyPubSub._registry), 0)

    def test_pubsub2(self):
        called = [False]
        class Something(object):
            def some_method(self):
                called[0] = True
        inst = Something()
        pubsub.subscribe("test1", inst.some_method)
        for i in range(10):
            pubsub.publish("test1")
            self.assertTrue(called[0])
            called[0] = False
        called[0] = False
        pubsub.unsubscribe("test1", inst.some_method)
        pubsub.publish("test1")
        self.assertFalse(called[0])
        with RLOCK:
            self.assertEqual(len(pubsub.MyPubSub._registry), 0)

    def test_pubsub3(self):
        called = [False]
        class Something(object):
            def some_method(self):
                called[0] = True
        inst = Something()
        pubsub.subscribe("test1", inst.some_method)
        del inst
        pubsub.publish("test1")
        self.assertFalse(called[0])
        with RLOCK:
            self.assertEqual(len(pubsub.MyPubSub._registry), 0)

    def test_pubsub4(self):
        param = []
        def function1(arg1, arg2):
            param.append(arg1)
            param.append(arg2)

        pubsub.subscribe("topic", function1)
        pubsub.publish("topic", 1, "2")
        self.assertEqual(param[0], 1)
        self.assertEqual(param[1], "2")
        pubsub.unsubscribe_all()

        param = {}
        def function2(arg1, arg2):
            param[0] = arg1
            param[1] = arg2

        pubsub.subscribe("topic", function2)
        pubsub.publish("topic", arg1="2", arg2=10)
        self.assertEqual(param[0], "2")
        self.assertEqual(param[1], 10)

    def test_pubsub5(self):
        current_thr = threading.current_thread()
        param = []
        def function1():
            param.append(threading.current_thread())
        pubsub.subscribe("topic", function1)
        pubsub.publish("topic")
        self.assertIs(current_thr, param[0])
        param.pop(0)

        def difthread():
            function1()
        thr = threading.Thread(target=difthread)
        thr.start()
        thr.join()
        self.assertIsNot(current_thr, param[0])


def suite():
    st1 = unittest.makeSuite(PubsubTests)
    return unittest.TestSuite((st1,))
