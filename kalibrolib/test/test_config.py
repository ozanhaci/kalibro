# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import os
import tempfile
import inspect
import unittest

import six

from ..config import ConfigError, ConfigManager


class MockConfigManager(ConfigManager):

    def fix_compatibility(self):
        pass



class ConfigTests(unittest.TestCase):

    def setUp(self):
        tmp = tempfile.NamedTemporaryFile(delete=False)
        self.tmp_config_path = tmp.name
        tmp.close()
        assert os.path.exists(self.tmp_config_path)
        self.config_manager = MockConfigManager(config_path=self.tmp_config_path)

    def tearDown(self):
        if os.path.exists(self.tmp_config_path):
            os.remove(self.tmp_config_path)
        self.tmp_config_path = None
        del self.tmp_config_path
        self.config_manager = None
        del self.config_manager

    def test_exists(self):
        self.config_manager.bool_options
        self.config_manager.compfixed
        self.config_manager.config_path
        self.config_manager.created
        self.config_manager.defaults
        self.config_manager.float_options
        self.config_manager.installed_encodings
        self.config_manager.installed_langs
        self.config_manager.int_options
        self.config_manager.old_config_path

        self.assertTrue(inspect.ismethod(self.config_manager.create))
        self.assertTrue(inspect.ismethod(self.config_manager.fix_compatibility))
        self.assertTrue(inspect.ismethod(self.config_manager.get))
        self.assertTrue(inspect.ismethod(self.config_manager.set))
        self.assertTrue(inspect.ismethod(self.config_manager.option_check_fix))
        self.assertTrue(inspect.ismethod(self.config_manager.flush))

        self.config_manager.__getitem__
        self.config_manager.__setitem__

    def test_defaults(self):
        self.config_manager.create()
        # config file path exists, no need create file
        self.assertFalse(self.config_manager.created)
        parser = self.get_parser()
        self.assertTrue(parser.has_section("client"))
        for k in self.config_manager.defaults.keys():
            attr = "get"
            if k in self.config_manager.bool_options:
                attr = "getboolean"
            elif k in self.config_manager.int_options:
                attr = "getint"
            elif k in self.config_manager.float_options:
                attr = "getfloat"
            self.assertEqual(self.config_manager[k], getattr(parser, attr)("client", k))

    def test_set(self):
        for opt in self.config_manager.options:
            comp_val = "test"
            attr = "get"
            if opt in self.config_manager.bool_options:
                comp_val = True
                attr = "getboolean"
            elif opt in self.config_manager.int_options:
                comp_val = 1000
                attr = "getint"
            elif opt in self.config_manager.float_options:
                comp_val = 10.99
                attr = "getfloat"
            elif opt == "language":
                comp_val = (self.config_manager.installed_langs() or ["en"])[0]
            elif opt == "file_encoding":
                comp_val = (self.config_manager.installed_encodings() or ["utf_8"])[0]
            self.config_manager[opt] = comp_val
            parser = self.get_parser()
            verify_val = getattr(parser, attr)("client", opt)
            self.assertEqual(self.config_manager[opt], verify_val)
            self.assertEqual(comp_val, verify_val)

    def test_error(self):
        """ check errors for foreign options"""
        with self.assertRaises(ConfigError):
            self.config_manager[self.get_option()]
        with self.assertRaises(ConfigError):
            self.config_manager[self.get_option()] = "test"

    def get_option(self):
        """ return an config option name which is not in current options"""
        opts = self.config_manager.options
        for opt in opts:
            self.assertIsInstance(opt, six.string_types)
        return "".join(opts)

    def get_parser(self):
        parser = self.config_manager.parser_cls(self.config_manager.defaults,
                                                allow_no_value=False)
        parser.read(self.tmp_config_path)
        return parser

def suite():
    st1 = unittest.makeSuite(ConfigTests)
    return unittest.TestSuite((st1,))
