# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import os
import datetime
import unittest
import tempfile

import wx
import six

from .wtc import WidgetTestCase
from .. import utils


def gettext(msg):
    return msg


def setUpModule():
    debug = os.environ.pop("DEBUG", None)
    if debug is not None:
        os.environ["_DEBUG"] = debug
    os.environ["TEST"] = "1"
    import __builtin__
    __builtin__.__dict__['_'] = gettext


def tearDownModule():
    os.environ.pop("TEST", None)
    debug = os.environ.pop("_DEBUG", None)
    if debug is not None:
        os.environ["DEBUG"] = debug


class JinjaFunctionsTests(WidgetTestCase):

    def test_none(self):
        self.assertEqual(utils.fix_none(None), "")
        input_ = "xyz"
        self.assertIs(utils.fix_none(input_), input_)

    def test_report_dt(self):
        dtfmt = "%Y-%m-%d %H:%M:%S"
        now = datetime.datetime.now()
        self.assertIsInstance(utils.fix_report_dt(now, dtfmt),
                              six.string_types)
        self.assertEqual(utils.fix_report_dt(None, ""), "")

    def test_file_path(self):
        self.assertEqual(utils.fix_file_path("abc"), "file:///abc")
        self.assertIs(utils.fix_file_path(None), None)


class HelperFunctionsTests(WidgetTestCase):

    def test_sanitize_space(self):
        self.assertEqual(utils.sanitize_space("\n \t  \rabc\t\n  "), " abc ")
        self.assertEqual(utils.sanitize_space("\n \t  \rabc\t\n  xyz "), " abc xyz ")
        with self.assertRaises(TypeError):
            utils.sanitize_space(None)

    def test_coltr(self):
        try:
            del utils.coltr.dict
        except AttributeError:
            pass
        self.assertEqual(utils.coltr("test_column", "test_table"), "test_column")
        self.assertIsInstance(utils.coltr.dict, dict)
        del utils.coltr.dict

    def test_image(self):
        pngstream = utils.getCalendarButtonImage()
        wx.BitmapFromImage(wx.ImageFromStream(pngstream))

    def test_iswritable(self):
        temp_path = tempfile.gettempdir()
        self.assertTrue(utils.isWritable(temp_path))
        testfile = tempfile.NamedTemporaryFile(dir=temp_path, delete=True)
        self.assertFalse(utils.isWritable(testfile.name))
        testfile.close()

    def test_icon(self):
        self.assertIsInstance(utils.get_icon(), wx.Icon)

    def test_unicodewriter(self):
        class DummyStream(object):
            def __init__(self):
                self.data = None

            def write(self, data):
                self.data = data

        row = [b"abc", b"xyz", "çığöşü"]
        stream = DummyStream()
        uw = utils.UnicodeWriter(stream, delimiter=b';')
        uw.writerow(row)
        self.assertIsInstance(stream.data, six.binary_type)
        self.assertEqual(stream.data, b"abc;xyz;\xc3\xa7\xc4\xb1\xc4\x9f\xc3\xb6\xc5\x9f\xc3\xbc" + six.b(os.linesep))


def suite():
    st1 = unittest.makeSuite(JinjaFunctionsTests)
    st2 = unittest.makeSuite(HelperFunctionsTests)
    return unittest.TestSuite((st1, st2))
