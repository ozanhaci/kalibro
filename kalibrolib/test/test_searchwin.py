# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import sys
import os
import threading
import datetime
import tempfile
import unittest

import wx
import six

from ..conn_sqlite import MainSql
from ..search import MainSearchWin, SearchManager, SearchModel
from .. import pubsub
from .. import search as searchmod

from . import wtc
from .test_basicdata import setUpModule, tearDownModule

RLOCK = threading.RLock()


class MockMainSql(MainSql):
    def __init__(self, db=":memory:", forceCreate=True):
        super(MockMainSql, self).__init__(db=db, forceCreate=forceCreate)


class MockSearchWin(MainSearchWin):

    def messagebox(self, msg, caption, flag=wx.OK):
        assert isinstance(msg, six.string_types)
        assert isinstance(caption, six.string_types)
        assert isinstance(flag, six.integer_types)
        sys.stderr.write("\n" + caption + ": " + msg + "\n")
        if flag & wx.ICON_ERROR:
            raise Exception(msg)


class SearchWindowTests(wtc.WidgetTestCase):

    def setUp(self):
        pubsub.unsubscribe_all()
        super(SearchWindowTests, self).setUp()
        self.search_frame = MockSearchWin(parent=self.frame,
                                          manager_cls=SearchManager,
                                          model_cls=SearchModel,
                                          db_cls=MockMainSql)
        self.old_conf_cls = old_conf_cls = searchmod.ConfigManager
        self.config_path = config_path = []

        def get_config_manager(*kw):
            tmp = tempfile.NamedTemporaryFile(delete=False)
            fpath = tmp.name
            tmp.close()
            config_path.append(fpath)
            kw["config_path"] = fpath
            kw["old_config_path"] = ""
            conf = old_conf_cls(**kw)
            conf.create()
            return conf
        searchmod.ConfigManager = get_config_manager

    def tearDown(self):
        super(SearchWindowTests, self).tearDown()
        pubsub.unsubscribe_all()
        self.search_frame = None
        del self.search_frame
        searchmod.ConfigManager = self.old_conf_cls
        self.old_conf_cls = None
        del self.old_conf_cls
        if self.config_path:
            for p in self.config_path:
                if os.path.exists(p):
                    os.remove(p)

    def click_button(self, wdgt):
        clickEvent = wx.CommandEvent(wx.wxEVT_COMMAND_BUTTON_CLICKED,
                                     wdgt.GetId())
        wdgt.ProcessEvent(clickEvent)

    def get_search_item_values(self):
        values = {}
        for name, item in self.search_frame.search_items.items():
            values[name] = item["widget"].GetValue()
        return values

    def add_sample_record(self):
        db = self.search_frame.manager.model.db
        status = "broken"
        owner = "a user"
        result = "OK"
        db.execute("INSERT INTO status (name) VALUES (?)", (status,))
        db.execute("INSERT INTO owner (name) VALUES (?)", (owner,))
        db.execute("INSERT INTO result (name) VALUES (?)", (result,))

        query_main = '''INSERT INTO main
            (registryNo,deviceName,deviceManuf,manufSerialNo,
            features,comingDate,description,status,owner) VALUES
            (?,?,?,?,?,?,?,?,?)'''
        query_sub = '''INSERT INTO subrecords
            (id,registryNo,plannedDate,realizedDate,
            trackingNo,method,result,path)
            VALUES
            (?,?,?,?,?,?,?,?)'''
        for i in range(10):
            testdatetime = datetime.datetime.now()
            regno = "registryNo" + str(i)
            tail = [regno, "deviceName" + str(i), "deviceManuf" + str(i),
                    "manufSerialNo" + str(i), None, testdatetime,
                    None, status, owner]
            db.execute(query_main, tail)
            for j in range(5):
                tail = [j, regno, testdatetime + datetime.timedelta(days=j),
                        testdatetime + datetime.timedelta(days=j + 1),
                        None, "method", result, None]
                db.execute(query_sub, tail)

    def test_init(self):
        self.search_frame.manager.model
        self.assertTrue(self.search_frame.IsShown())

    def test_pubsub(self):
        with RLOCK:
            reg = pubsub.MyPubSub._registry
            self.assertTrue(SearchManager.topic_update_dev_status in reg)
            self.assertTrue(SearchManager.topic_update_owner in reg)

    def test_clear_form(self):
        for item in self.search_frame.search_items.values():
            if isinstance(item["widget"], wx.TextCtrl):
                item["widget"].SetValue("test")
            elif isinstance(item["widget"], wx.ComboBox):
                item["widget"].SetValue(item["widget"].GetStrings() and
                                        item["widget"].GetStrings()[-1] or "")
        self.click_button(wdgt=self.search_frame.btnClearForm)
        self.assertTrue(True not in list(map(lambda x: x == "", self.get_search_item_values())))

    def test_open_help(self):
        self.click_button(wdgt=self.search_frame.btnHelp)
        self.assertTrue(self.search_frame.help_frame.IsShown())

    def test_clear_search(self):
        self.add_sample_record()
        self.click_button(wdgt=self.search_frame.btnSearch)
        self.click_button(wdgt=self.search_frame.btnClearResult)
        self.assertEqual(self.search_frame.listResult.GetItemCount(), 0)

    def test_search(self):
        self.add_sample_record()
        for i in range(10):
            self.click_button(wdgt=self.search_frame.btnClearForm)
            self.click_button(wdgt=self.search_frame.btnClearResult)
            self.assertEqual(self.search_frame.listResult.GetItemCount(), 0)
            wdgt = self.search_frame.search_items["registryNo"]["widget"]
            wdgt.SetValue("registryNo" + str(i))
            self.click_button(wdgt=self.search_frame.btnSearch)
            self.assertEqual(self.search_frame.listResult.GetItemCount(), 5)
        for i in range(10):
            self.click_button(wdgt=self.search_frame.btnClearForm)
            self.click_button(wdgt=self.search_frame.btnClearResult)
            self.assertEqual(self.search_frame.listResult.GetItemCount(), 0)
            wdgt = self.search_frame.search_items["result"]["widget"]
            wdgt.SetValue("OK")
            self.click_button(wdgt=self.search_frame.btnSearch)
            self.assertEqual(self.search_frame.listResult.GetItemCount(), 50)
        for i in range(10):
            self.click_button(wdgt=self.search_frame.btnClearForm)
            self.click_button(wdgt=self.search_frame.btnClearResult)
            self.assertEqual(self.search_frame.listResult.GetItemCount(), 0)
            wdgt = self.search_frame.search_items["owner"]["widget"]
            wdgt.SetValue("%user%")
            self.click_button(wdgt=self.search_frame.btnSearch)
            self.assertEqual(self.search_frame.listResult.GetItemCount(), 50)

def suite():
    st1 = unittest.makeSuite(SearchWindowTests)
    return unittest.TestSuite((st1,))
