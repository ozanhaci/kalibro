# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import unittest
from distutils.version import StrictVersion


class VersionComparisonTests(unittest.TestCase):

    def test_strict_version(self):
        complist = [("1.0", "1.1", -1), ("1.0", "1.0.0", 0), ("2.0", "1.9", 1),
                    ("1.0a1", "1.0a2", -1), ("1.0a1", "1.0a10", -1),
                    ("2.0", "1.9a1", 1), ("2.0b1", "2.0a5", 1), ("2.0b2", "2.0b1", 1), ]
        for item in complist:
            self.assertTrue(cmp(StrictVersion(item[0]), StrictVersion(item[1])) == item[2])


def suite():
    st1 = unittest.makeSuite(VersionComparisonTests)
    return unittest.TestSuite((st1,))
