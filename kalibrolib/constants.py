# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import os
import sys
import urllib
import locale
import __main__

VERSION = getattr(__main__, "__version__", "")
AUTHOR = getattr(__main__, "__author__", "")
AUTHOR_EMAIL = "ozan_haci@yahoo.com"
HOME_PAGE_URL = ("http://www.ozanh.com/kalibro" + "?from=" +
                 urllib.quote(VERSION))
HELP_PAGE_URL = ("http://www.ozanh.com/kalibro/help.php" + "?from=" +
                 urllib.quote(VERSION))
VERSION_CONTROL_URL = ("http://www.ozanh.com/kalibro/version.php?from=" +
                       urllib.quote(VERSION))
DONATION_URL = ("http://www.ozanh.com/kalibro/donate.php?from=" +
                urllib.quote(VERSION))

IS_LINUX = IS_WINDOWS = False
if sys.platform.lower().startswith("linux"):
    IS_LINUX = True
elif sys.platform.lower().startswith("win"):
    IS_WINDOWS = True
else:
    IS_LINUX = True

VERSION_CHECK = True
if "VERSION_CHECK" in os.environ and os.environ["VERSION_CHECK"] == "0":
    VERSION_CHECK = False

DEF_LANG = "en"
DEF_DATEFORMAT = "%Y-%m-%d"
DEF_FILE_ENCODING = locale.getpreferredencoding()
