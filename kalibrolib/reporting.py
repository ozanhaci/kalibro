# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import os
import datetime
import tempfile
import io
import webbrowser

import wx
from jinja2 import Environment, FileSystemLoader

from .conn_sqlite import MainSql
from .debugging import DEBUG, log_debug
from .utils import (coltr, UnicodeWriter, sanitize_space, path2url,
                    fix_report_dt, fix_report_timestamp, fix_none,
                    fix_file_path, fix_basename, str_result, get_icon)
from .multiselect import MultiSelectCheckBox

str = unicode

MY_REP_PCOPY_ID = wx.NewId()

GLOBALS = {}


class SubSets(object):

    __slots__ = ("main", "label_dict", "sub", "repdevstat")

    def __init__(self, main, label_dict, sub, repdevstat):
        self.main = main
        self.label_dict = label_dict
        self.sub = sub
        self.repdevstat = repdevstat

    def __getitem__(self, k):
        try:
            return self.main[k]
        except IndexError:
            return ""


class RecordSet(object):

    def __init__(self, regnos):
        self.dbobj = MainSql()
        queryLabels = "SELECT IntLabel,TextLabel1,TextLabel2,DateLabel FROM mainAddFieldLabel LIMIT 1"
        resLabel = self.dbobj.fetch_all(queryLabels).result
        self.addLabelDict = {"IntLabel": "IntLabel", "TextLabel1": "TextLabel1",
                             "TextLabel2": "TextLabel2", "DateLabel": "DateLabel"}
        if resLabel:
            self.addLabelDict = {"IntLabel": resLabel[0][0], "TextLabel1": resLabel[0][1],
                                 "TextLabel2": resLabel[0][2], "DateLabel": resLabel[0][3]}
        query = """SELECT m.*,
            a.IntValue,
            a.TextValue1,
            a.TextValue2,
            a.DateValue
            FROM main m LEFT JOIN mainAddField a
            ON m.registryNo=a.registryNo"""
        queryWhere = ""
        if(regnos == "*"):
            regnos = ()
        elif isinstance(regnos, (tuple, list, set)) and len(regnos) > 0:
            queryWhere = " WHERE m.registryNo IN (" + ",".join(["?"] * len(regnos)) + ")"
        order_by = ""
        order_by_params = []
        if regnos:
            order_by = " ORDER BY CASE m.registryNo "
            for idx, rno in enumerate(regnos):
                order_by += " WHEN ? THEN %s" % idx
                order_by_params.append(rno)
            order_by += " END ASC"
            if DEBUG:
                log_debug(query + queryWhere + order_by)
                log_debug(regnos, order_by_params)
        self.result = self.dbobj.fetch_all(query + queryWhere + order_by, tuple(regnos) + tuple(order_by_params)).result

    def __iter__(self):
        return self

    def next(self):
        if not hasattr(self, "_counter"):
            self._counter = -1
        self._counter += 1
        if self._counter + 1 > len(self.result):
            del self._counter
            raise StopIteration
        subresults = []
        repdevstat = []
        if len(self.result) > 0 and len(self.result[self._counter]) > 0:
            regno = self.result[self._counter][b"registryNo"]
            query = "SELECT * FROM subrecords WHERE registryNo=?"
            tail = (regno,)
            subresults = self.dbobj.fetch_all(query, tail).result
            query = "SELECT * FROM repDevStatus WHERE registryNo=?"
            tail = (regno,)
            repdevstat = self.dbobj.fetch_all(query, tail).result
        return SubSets(self.result[self._counter], self.addLabelDict,
                       subresults, repdevstat)

    __next__ = next


class MainReport(wx.Frame):

    def __init__(self, parent, regnos, language, fileEncoding, dateformat):
        GLOBALS["LANG"] = language
        if os.path.exists("kalibro.png"):
            GLOBALS["ICON_PATH"] = path2url(os.path.abspath("kalibro.png"))
        self.regnos = regnos
        self.dateformat = dateformat
        self.fileEncoding = fileEncoding
        title = _("Kalibro - Reporting")
        GLOBALS["PAGE_TITLE"] = title
        wx.Frame.__init__(self, parent, wx.ID_ANY, title, size=(200, 300))
        self.SetIcon(get_icon())

        self.panel = wx.Panel(self, -1, style=wx.BORDER_STATIC)

        self.gbs = wx.GridBagSizer(2, 3)

        self.btnOpenHtmlReport = wx.Button(self.panel,
                                           label=_("Open HTML Report"))
        self.btnOpenHtmlReport.Bind(wx.EVT_BUTTON, self.onClickHtmlRepBtn)
        self.gbs.Add(self.btnOpenHtmlReport, (1, 1), flag=wx.ALL | wx.GROW)

        self.btnSaveCsv = wx.Button(self.panel, label=_("Save CSV Report"))
        self.btnSaveCsv.Bind(wx.EVT_BUTTON, self.onClickCsvRepBtn)
        self.gbs.Add(self.btnSaveCsv, (2, 1), flag=wx.ALL | wx.GROW)

        self.panel.SetSizerAndFit(self.gbs)
        self.CenterOnScreen()
        self.Show(True)

    def onClickCsvRepBtn(self, evt):
        wildcard = _("CSV (*.csv)|*.csv|All Files (*.*)|*.*")
        dlg = wx.FileDialog(
            self.panel, message=_("Save As ..."), defaultDir=os.getcwd(),
            defaultFile=_("Kalibro_CSV_Report"), wildcard=wildcard,
            style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT
            )
        if dlg.ShowModal() != wx.ID_OK:
            dlg.Destroy()
            return

        queryWhere = ""
        if(self.regnos == "*"):
            regnos = ()
        elif isinstance(self.regnos, (tuple, list, set)) and len(self.regnos) > 0:
            queryWhere = " WHERE m.registryNo IN (" + ",".join(["?"] * len(self.regnos)) + ")"
            regnos = self.regnos
        dbobj = MainSql()
        queryLabels = "SELECT IntLabel,TextLabel1,TextLabel2,DateLabel FROM mainAddFieldLabel LIMIT 1"
        resLabel = dbobj.fetch_all(queryLabels).result
        queryDict = {"IntLabel": "IntLabel", "TextLabel1": "TextLabel1",
                     "TextLabel2": "TextLabel2", "DateLabel": "DateLabel"}
        if resLabel:
            queryDict = {"IntLabel": resLabel[0][0], "TextLabel1": resLabel[0][1],
                         "TextLabel2": resLabel[0][2], "DateLabel": resLabel[0][3]}
        queryDict["where"] = queryWhere
        query = '''SELECT m.*,s.*,
        a.IntValue as "%(IntLabel)s",
        a.TextValue1 as "%(TextLabel1)s",
        a.TextValue2 as "%(TextLabel2)s",
        a.DateValue as "%(DateLabel)s" FROM main m LEFT JOIN subrecords s
        ON m.registryNo=s.registryNo
        LEFT JOIN mainAddField a ON a.registryNo=m.registryNo
        %(where)s ORDER BY m.registryNo,s.id ASC''' % queryDict
        result = dbobj.fetch_all(query, regnos).result
        _columns = dbobj.getColNames()
        columns = []
        tbl = "main"
        for idx, c in enumerate(_columns):
            tmp = coltr(c, tbl)
            if tmp == c:
                tbl = "subrecords"
                tmp = coltr(c, tbl)
            if idx == 0 and c == "ID":
                c = "Id"
            tmp = sanitize_space(tmp)
            columns.append(tmp)
        data = [columns] + str_result(result, self.dateformat)
        with io.open(dlg.GetPath(), "wb", buffering=0) as cf:
            cwriter = UnicodeWriter(cf, delimiter=b';',
                                    encoding=self.fileEncoding)
            cwriter.writerows(data)
            cf.flush()
        dlg.Destroy()

    def onClickHtmlRepBtn(self, evt):
        if self.dateformat == "us":
            self.dateformat = "%m-%d-%Y"
        elif self.dateformat == "eu":
            self.dateformat = "%d-%m-%Y"
        timeformat = "%H:%M:%S"
        GLOBALS["DATE_TIME"] = \
            datetime.datetime.now().strftime(self.dateformat + " " + timeformat)
        htmldata = self.getHtmlRepData(self.regnos)
        htmlfilename = os.path.join(tempfile.gettempdir(),
                                    _("Kalibro_Report") + ".html")
        with open(htmlfilename, "w") as htmlfile:
            htmlfile.write(htmldata.encode("utf-8"))
            htmlfile.flush()
            htmldata = None
            del htmldata
            webbrowser.open(htmlfilename)

    def getHtmlRepData(self, regnos):

        jenv = Environment(loader=FileSystemLoader(os.getcwd()),
                           autoescape=True)
        jinja_helper_dict = {"_": _}
        jenv.globals.update(**jinja_helper_dict)
        jenv.filters["fix_dt"] = lambda x: fix_report_dt(x, self.dateformat)
        jenv.filters["fix_ts"] = lambda x: fix_report_timestamp(x, self.dateformat)
        jenv.filters["fix_none"] = fix_none
        jenv.filters["fix_file_path"] = fix_file_path
        jenv.filters["fix_basename"] = fix_basename
        tpl = jenv.get_template("report.html")
        if os.path.isfile("report.html"):
            return tpl.render(mainRecords=RecordSet(regnos), **GLOBALS)
        else:
            return _("Template file cannot be found")


def reporting(parent=None, select_data=None, columns=None, title="", footer="",
              size=None, language="", file_encoding="", dateformat=""):
    assert (parent and file_encoding and dateformat and language)
    kwargs = {}
    if size:
        kwargs["size"] = size
    if title:
        kwargs["title"] = title
    else:
        kwargs["title"] = _("Please select the devices to report")
    if footer:
        kwargs["footer"] = footer
    else:
        kwargs["footer"] = _("Press OK to Continue")
    dbob = MainSql()
    if columns:
        kwargs["columns"] = columns
    else:
        dbob.fetch_all("SELECT * FROM main")
        columns = map(lambda x: coltr(x, "main"), dbob.getColNames())
        kwargs["columns"] = columns
    if select_data:
        kwargs["select_data"] = select_data
    else:
        if not dbob.result:
            dbob.fetch_all("SELECT * FROM main")

        kwargs["select_data"] = str_result(dbob.result, dateformat)
    dlg = MultiSelectCheckBox(**kwargs)
    if dlg.ShowModal() == wx.ID_OK:
        checked_items = dlg.checked_items
        if DEBUG:
            log_debug("Checked_items %r" % checked_items)
        MainReport(parent, checked_items, language,
                   file_encoding, dateformat)
    dlg.Destroy()


if __name__ == "__main__":
    _ = lambda x: x
    app = wx.App()
    frame = MainReport(None, "*", "tr", "utf_8", "us")
    app.MainLoop()
