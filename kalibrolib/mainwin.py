# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import os
import operator
import math
import traceback
import webbrowser
import urllib
import threading
import time
import io
import re
import copy
import subprocess
import functools
from datetime import date, datetime
from distutils.version import StrictVersion

import wx
import wx.grid
import wx.calendar
import wx.lib.dialogs
import wx.lib.newevent
import wx.lib.printout as printout
import wx.lib.agw.advancedsplash as AdvSplash
from wx.lib.wordwrap import wordwrap

from .conn_sqlite import MainSql
from .conn_sqlite import IntegrityError
from .utils import getCalendarButtonImage
from .utils import UnicodeWriter
from .utils import coltr
from .utils import sanitize_space
from .utils import get_icon
from .debugging import DEBUG
from .debugging import debug_trace
from .debugging import log_debug
from .debugging import nodebug
from .reporting import reporting
from .search import MainSearchWin
from .histlog import HistoryLogFrame
from .basicdata.devstatus import DevStatusWin
from .basicdata.calresult import CalibrationResultWin
from .basicdata.owner import OwnerWin
from .auth import UserWin, PasswordWin, ModelAuth, get_user
from .calgrid import CalibrationGrid
from .addfieldwin import AdditionalFieldsWin
from .widgets import MinimizeApp
from .config import ConfigManager
from . import reporttool
from . import pubsub
from .constants import (VERSION, AUTHOR, VERSION_CONTROL_URL,
                        HOME_PAGE_URL, HELP_PAGE_URL, DONATION_URL,
                        AUTHOR_EMAIL, IS_WINDOWS, VERSION_CHECK,
                        DEF_FILE_ENCODING)


str = unicode

NewVersionEvent, EVT_NEW_VERSION = wx.lib.newevent.NewEvent()

MY_DATETIME_MAIN_BUT = wx.NewId()
MY_ADDF_BUTTON = wx.NewId()

MY_MENU_QUIT = wx.ID_EXIT
MY_MENU_ADDNEW = wx.NewId()
MY_MENU_ADDSUBNEW = wx.NewId()
MY_MENU_ADDSTATUS = wx.NewId()
MY_MENU_ADDCALRESULT = wx.NewId()
MY_MENU_ADDOWNER = wx.NewId()
MY_MENU_SAVE = wx.NewId()
MY_MENU_CAL_CTRL = wx.NewId()
MY_MENU_ABOUT = wx.ID_ABOUT
MY_MENU_DATEFORMAT = wx.NewId()
MY_MENU_HELP = wx.NewId()
MY_MENU_README = wx.NewId()
MY_MENU_REL_NOTES = wx.NewId()
MY_MENU_REPORT = wx.NewId()
MY_MENU_HISTLOG = wx.NewId()
MY_MENU_ICONIZE = wx.NewId()
MY_MENU_ENCODINGS = wx.NewId()
MY_MENU_LOCALIZE = wx.NewId()
MY_MENU_DONATE = wx.NewId()
MY_MENU_VERSION_CHECK = wx.NewId()
MY_MENU_SPLASH = wx.NewId()
MY_MENU_USER = wx.NewId()
MY_MENU_PASSCHG = wx.NewId()

MY_SAVE_MAIN = wx.NewId()
MY_CLR_MAIN = wx.NewId()
MY_DEL_MAIN = wx.NewId()
MY_SEARCH_MAIN = wx.NewId()

MY_POPUP_ID1 = wx.NewId()
MY_POPUP_ID2 = wx.NewId()
MY_POPUP_ID3 = wx.NewId()
MY_POPUP_ID4 = wx.NewId()
MY_POPUP_ID5 = wx.NewId()
MY_POPUP_ID6 = wx.NewId()
MY_POPUP_ID7 = wx.NewId()
MY_POPUP_ID8 = wx.NewId()
MY_POPUP_ID9 = wx.NewId()
MY_POPUP_ID10 = wx.NewId()
MY_POPUP_ID11 = wx.NewId()
MY_POPUP_ID12 = wx.NewId()
MY_POPUP_ID13 = wx.NewId()


def needs_write_permission(func):
    @functools.wraps(func)
    def wrapper(*args, **kw):
        username = get_user()
        amodel = ModelAuth()
        installed = amodel.auth_installed()
        if username == "admin":
            return func(*args, **kw)
        elif username and installed:
            if amodel.has_write_perm(username):
                return func(*args, **kw)
            else:
                pubsub.publish("needs_write_permission")
        else:
            return func(*args, **kw)

    return wrapper


class NewVersionThread(threading.Thread):
    """
    Checks if there is a new version at background and post wx event
    """
    def __init__(self, parentWin, urlopen=urllib.urlopen, sleep=10):
        threading.Thread.__init__(self)
        self.parentWin = parentWin
        self._urlopen = urlopen
        self._sleep = sleep
        self.daemon = True

    def run(self):
        time.sleep(self._sleep)
        try:
            r = self._urlopen(VERSION_CONTROL_URL)
            version = r.read().strip()[:20]
            match = re.match(r"[0-9]+\.[0-9]+\w*", version, re.U | re.I)
            if DEBUG:
                log_debug("Current Version = %s" % VERSION)
                log_debug("New Version = %s" % version)
            if match and StrictVersion(version) > StrictVersion(VERSION):
                evt = NewVersionEvent(version=version)
                # wx.PostEvent(self.parentWin, evt)
                self.parentWin.GetEventHandler().ProcessEvent(evt)
            elif match is None:
                if DEBUG:
                    log_debug("Version string %s did not match" % version)
            r.close()
        except:
            if DEBUG:
                log_debug(debug_trace(), "version control exception")
                log_debug(traceback.format_exc())


class CalibrationCtrlList(wx.ListCtrl):
    def __init__(self, parent, data=None, headers=None, rowbgcolors=None):
        wx.ListCtrl.__init__(
            self, parent, -1,
            style=wx.LC_REPORT | wx.LC_VIRTUAL | wx.LC_HRULES | wx.LC_VRULES |
            wx.LC_SINGLE_SEL
            )
        self.SetItemCount(1000000)
        self._data = data
        self._rowbgcolors = rowbgcolors
        for idx, h in enumerate(headers):
            self.InsertColumn(idx, h)
        for i in range(len(headers)):
            self.SetColumnWidth(i, wx.LIST_AUTOSIZE_USEHEADER)

        self.attr1 = wx.ListItemAttr()
        self.attr1.SetBackgroundColour("yellow")

        for row in self._rowbgcolors:
            if "bgcolor" in self._rowbgcolors[row]:
                attr = wx.ListItemAttr()
                attr.SetBackgroundColour(self._rowbgcolors[row]["bgcolor"])
                self._rowbgcolors[row]["attr"] = attr

    def OnGetItemText(self, item, col):
        try:
            if item + 1 <= len(self._data):
                val = self._data[item][col]
                return val if val is not None else ""
            else:
                raise IndexError
        except IndexError:
            return ""

    def OnGetItemAttr(self, item):
        if item in self._rowbgcolors:
            return self._rowbgcolors[item].get("attr", None)
        else:
            return None


class MainWindow(object):

    _dbConn = None

    def __init__(self, *args, **kwargs):
        recreated = MainSql.new_db
        if DEBUG:
            log_debug(debug_trace(), "db created=", recreated)

        self.init()

        self.numTimerCalls = 0
        self.currentRegNo = None

        # Height and width of the window
        self.frame_width = 920
        self.frame_height = 768
        title = "Kalibro " + VERSION + "    Copyright " + AUTHOR
        title = title + " (" + (get_user() or _("NOT LOGGED IN")) + ")"
        self.frame = wx.Frame(parent=None, id=-1, title=title,
                              size=wx.Size(self.frame_width,
                                           self.frame_height))
        wx.GetApp().SetTopWindow(self.frame)
        self.frame.SetIcon(get_icon())

        # ADD MENU
        self.addMenu()

        self.panel1 = wx.Panel(self.frame, -1, size=(self.frame_width, -1))

        Top1Sizer = wx.BoxSizer(wx.VERTICAL)
        TopSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.SubSizer1 = wx.BoxSizer(wx.VERTICAL)
        self.SubSizer2 = wx.BoxSizer(wx.VERTICAL)
        self.SubSizer3 = wx.BoxSizer(wx.VERTICAL)

        # Create the dialog for the main features of the item
        info_sz = wx.BoxSizer(wx.HORIZONTAL)
        info_sz.Add(wx.Size(10, -1))
        self.info_bar = wx.InfoBar(self.panel1)
        info_sz.Add(self.info_bar, 1, flag=wx.ALL | wx.GROW)
        Top1Sizer.Add(info_sz, 0, flag=wx.ALL | wx.GROW, border=1)

        self.createMainWidgets()
        self.populateDevStatus()
        self.populateOwner()
        pubsub.subscribe("update_dev_status", self.populateDevStatus)
        pubsub.subscribe("update_owner", self.populateOwner)
        pubsub.subscribe("needs_write_permission", self.needsWritePermission)

        self.statusGauge = wx.Gauge(self.panel1, -1, range=50, size=(200, 15),
                                    style=wx.GA_HORIZONTAL)
        self.SubSizer1.Add(self.statusGauge, 0)

        # create grid view for the sub of the measurement devices
        self.createGridList()

        # SIZERS
        TopSizer.Add(self.SubSizer1, 0.75)
        TopSizer.Add(wx.Size(20, 20))
        TopSizer.Add(self.SubSizer2, 1.5)
        TopSizer.Add(wx.Size(20, 20))
        TopSizer.Add(self.SubSizer3, 1)

        Top1Sizer.Add(TopSizer, 0, border=5, flag=wx.LEFT)
        Top1Sizer.Add(wx.Size(20, 20))
        Top1Sizer.Add(self.myGrid, 0, flag=wx.LEFT | wx.EXPAND)

        # EVENTS
        self.quickSearch.Bind(wx.EVT_SEARCHCTRL_SEARCH_BTN,
                              self.updateDeviceList)
        self.quickSearch.Bind(wx.EVT_SEARCHCTRL_CANCEL_BTN,
                              self.onCancelQuickSearch)
        self.quickSearch.Bind(wx.EVT_TEXT_ENTER, self.updateDeviceList)
        self.openList.Bind(wx.EVT_LISTBOX_DCLICK, self.fetchMainData)
        self.frame.Bind(EVT_NEW_VERSION, self.onNewVersion)
        self.frame.Bind(wx.EVT_BUTTON, self.deleteMainRecord, id=MY_DEL_MAIN)
        self.frame.Bind(wx.EVT_BUTTON, self.searchWindow, id=MY_SEARCH_MAIN)
        self.frame.Bind(wx.EVT_BUTTON, self.onClearMainForm, id=MY_CLR_MAIN)
        self.frame.Bind(wx.EVT_BUTTON, self.onAddField, id=MY_ADDF_BUTTON)
        self.frame.Bind(wx.EVT_BUTTON, self.saveMainData, id=MY_SAVE_MAIN)
        self.frame.Bind(wx.EVT_BUTTON,
                        lambda event: self.showDateTimeFrame(event=event,
                                                             grid=False),
                        id=MY_DATETIME_MAIN_BUT)
        if not self.has_write_permission:
            if self.can_change_dev_status:
                self.cbDevStatus.Bind(wx.EVT_COMBOBOX, self.onChangeDevStatus)
            else:
                self.cbDevStatus.Disable()
            if self.can_change_dev_owner:
                self.cbOwner.Bind(wx.EVT_COMBOBOX, self.onChangeOwner)
            else:
                self.cbOwner.Disable()

        self.frame.Bind(wx.EVT_MENU, self.exit, id=MY_MENU_QUIT)
        self.frame.Bind(wx.EVT_MENU, self.saveMainData, id=MY_MENU_SAVE)
        self.frame.Bind(wx.EVT_MENU, self.createNewDevice, id=MY_MENU_ADDNEW)
        self.frame.Bind(wx.EVT_MENU, self.createSubRecord,
                        id=MY_MENU_ADDSUBNEW)
        self.frame.Bind(wx.EVT_MENU, self.devStatusWin, id=MY_MENU_ADDSTATUS)
        self.frame.Bind(wx.EVT_MENU, self.calResultWin,
                        id=MY_MENU_ADDCALRESULT)
        self.frame.Bind(wx.EVT_MENU, self.ownerWin, id=MY_MENU_ADDOWNER)
        self.frame.Bind(wx.EVT_MENU, self.checkComingForm, id=MY_MENU_CAL_CTRL)
        self.frame.Bind(wx.EVT_MENU, self.onAboutMenu, id=MY_MENU_ABOUT)
        self.frame.Bind(wx.EVT_MENU, self.changeDateFormat,
                        id=MY_MENU_DATEFORMAT)
        self.frame.Bind(wx.EVT_MENU, self.onHelp, id=MY_MENU_HELP)
        self.frame.Bind(wx.EVT_MENU, self.onReadMe, id=MY_MENU_README)
        self.frame.Bind(wx.EVT_MENU, self.onReleaseNotes, id=MY_MENU_REL_NOTES)
        self.frame.Bind(wx.EVT_MENU, self.onReporting, id=MY_MENU_REPORT)
        self.frame.Bind(wx.EVT_MENU, self.onHistroyLog, id=MY_MENU_HISTLOG)
        self.frame.Bind(wx.EVT_MENU, self.createFileEncodingDialog,
                        id=MY_MENU_ENCODINGS)
        self.frame.Bind(wx.EVT_MENU, self.versionCheckChange,
                        id=MY_MENU_VERSION_CHECK)
        self.frame.Bind(wx.EVT_MENU, self.splashWindowChange,
                        id=MY_MENU_SPLASH)
        self.frame.Bind(wx.EVT_MENU, self.userManagementWin,
                        id=MY_MENU_USER)
        self.frame.Bind(wx.EVT_MENU, self.passwordChangeWin,
                        id=MY_MENU_PASSCHG)

        def gotoDonatePage(evt):
            webbrowser.open(DONATION_URL)

        self.frame.Bind(wx.EVT_MENU, gotoDonatePage, id=MY_MENU_DONATE)

        # To iconize (minimize) the program to the task bar
        self.trayicon = MinimizeApp(icon=self.frame.GetIcon(),
                                    tooltip="Kalibro", frame=self.frame)
        self.frame.Bind(wx.EVT_MENU, self.onIconize, id=MY_MENU_ICONIZE)
        # self.frame.Bind(wx.EVT_ICONIZE,self.onIconize)

        self.frame.Bind(wx.EVT_CLOSE, self.exit)

        # Grid Events
        self.myGrid.Bind(wx.grid.EVT_GRID_CELL_RIGHT_CLICK,
                         self.addPopupMenu2Grid)
        self.myGrid.Bind(wx.grid.EVT_GRID_CELL_LEFT_CLICK,
                         self.onLeftClickGridCell)
        self.myGrid.Bind(wx.grid.EVT_GRID_RANGE_SELECT, self.onGridRangeSelect)
        self.myGrid.Bind(wx.EVT_MENU, self.saveMainData, id=MY_POPUP_ID1)
        self.myGrid.Bind(wx.EVT_MENU, self.createSubRecord, id=MY_POPUP_ID2)
        self.frame.Bind(wx.EVT_MENU, self.grid2File, id=MY_POPUP_ID3)
        self.myGrid.Bind(wx.EVT_MENU, self.onGridCopy, id=MY_POPUP_ID4)
        self.myGrid.Bind(wx.EVT_MENU, self.onGridPaste, id=MY_POPUP_ID5)
        self.myGrid.Bind(wx.EVT_MENU, self.showDateTimeFrame, id=MY_POPUP_ID6)
        self.myGrid.Bind(wx.EVT_MENU, self.deleteSubRecord,
                         id=MY_POPUP_ID7)
        self.myGrid.Bind(wx.EVT_MENU, self.openReportTool, id=MY_POPUP_ID8)
        self.myGrid.Bind(wx.EVT_MENU, self.attachGridFilePath,
                         id=MY_POPUP_ID10)
        self.myGrid.Bind(wx.EVT_MENU, self.openGridFilePath, id=MY_POPUP_ID11)
        self.myGrid.Bind(wx.EVT_MENU, self.openGridFileDir, id=MY_POPUP_ID12)
        self.myGrid.Bind(wx.EVT_MENU, self.clearGridFilePath, id=MY_POPUP_ID13)
        self.myGrid.Bind(wx.grid.EVT_GRID_CELL_LEFT_DCLICK,
                         self.onGridLeftDClick)
        self.myGrid.Bind(wx.grid.EVT_GRID_EDITOR_CREATED,
                         self.onGridResultChoiceEditorCreated)

        # create the timer and bind the event to it
        self.myTimer = wx.Timer(self.panel1, -1)
        self.myTimer_cont = wx.Timer(self.panel1, -1)
        self.panel1.Bind(wx.EVT_TIMER, self.timerHandler, source=self.myTimer)
        self.panel1.Bind(wx.EVT_TIMER,
                         (lambda evt: self.timerHandler(con=True)),
                         source=self.myTimer_cont)

        self.panel1.SetSizer(Top1Sizer)
        Top1Sizer.Fit(self.panel1)

        self.frame.Center()
        self.frame.Show(True)

        # inform user about Kalibro created an empty DB
        if recreated:
            wx.MessageBox(_("Kalibro created an Empty Database.\n"
                            "Do not change the name of the files "
                            "if you want them to work properly"),
                          _("Information"), wx.ICON_INFORMATION)
        # populate list with devices
        try:
            self.updateDeviceList()
        except:
            self.message(_("Unable to Reach Database"))
            if DEBUG:
                log_debug(debug_trace(), traceback.format_exc())

        if ConfigManager()["splash"]:
            wx.CallLater(100, self.init2)

        self.versionCheck()
        pubsub.subscribe("search_select", self.onSearchSelect)

    def needsWritePermission(self):
        self.message(_("You do not have write permission.\n"
                       "Operation has been interrupted."), flag=wx.ICON_ERROR)

    def onCancelQuickSearch(self, evt):
        evt.Skip()
        wx.CallAfter(self.updateDeviceList)

    def onSearchSelect(self, regno):
        items = self.openList.GetItems()
        try:
            idx = items.index(regno)
            self.openList.Select(idx)
            self.fetchMainData()
        except (IndexError, ValueError):
            pass

    def versionCheck(self):
        # run version check thread
        config = ConfigManager()
        if VERSION_CHECK and config["version_check"]:
            self._thrNewVersion = NewVersionThread(self.frame)
            self._thrNewVersion.start()

    def splashWindowChange(self, evt):
        try:
            ConfigManager()["splash"] = bool(evt.IsChecked())
        except Exception as e:
            self.message(str(e))

    def versionCheckChange(self, evt):
        try:
            ConfigManager()["version_check"] = bool(evt.IsChecked())
        except Exception as e:
            self.message(str(e))

    def populateOwner(self):
        self.cbOwner.Clear()
        qry = '''SELECT name FROM owner ORDER BY name ASC'''
        db = self.dbConn
        result = db.fetch_all(qry).result
        items = [""] + [i[0] for i in result if i[0]]
        curVal = self.cbOwner.GetValue()
        self.cbOwner.AppendItems(items)
        self.cbOwner.SetValue(curVal)

    def populateDevStatus(self):
        self.cbDevStatus.Clear()
        qry = '''SELECT name FROM status ORDER BY name ASC'''
        db = self.dbConn
        result = db.fetch_all(qry).result
        items = [""] + [i[0] for i in result if i[0]]
        curVal = self.cbDevStatus.GetValue()
        self.cbDevStatus.AppendItems(items)
        self.cbDevStatus.SetValue(curVal)

    def passwordChangeWin(self, evt):
        if hasattr(self, "password_change_win"):
            try:
                self.password_change_win.Raise()
                self.password_change_win.Iconize(False)
                return
            except wx.PyDeadObjectError:
                pass
        self.password_change_win = PasswordWin(self.frame)

    def userManagementWin(self, evt):
        if hasattr(self, "user_mgmt_win"):
            try:
                self.user_mgmt_win.Raise()
                self.user_mgmt_win.Iconize(False)
                return
            except wx.PyDeadObjectError:
                pass
        self.user_mgmt_win = UserWin(self.frame)

    def ownerWin(self, evt):
        if hasattr(self, "owner_window"):
            try:
                self.owner_window.Raise()
                self.owner_window.Iconize(False)
                return
            except wx.PyDeadObjectError:
                pass
        self.owner_window = OwnerWin(self.frame)

    def calResultWin(self, evt):
        if hasattr(self, "cal_result_window"):
            try:
                self.cal_result_window.Raise()
                self.cal_result_window.Iconize(False)
                return
            except wx.PyDeadObjectError:
                pass
        self.cal_result_window = CalibrationResultWin(self.frame)

    def devStatusWin(self, evt):
        if hasattr(self, "status_window"):
            try:
                self.status_window.Raise()
                self.status_window.Iconize(False)
                return
            except wx.PyDeadObjectError:
                pass
        self.status_window = DevStatusWin(self.frame)

    def searchWindow(self, evt):
        if hasattr(self, "search_window"):
            try:
                self.search_window.Raise()
                self.search_window.Iconize(False)
                return
            except wx.PyDeadObjectError:
                pass
        self.search_window = MainSearchWin(self.frame)

    def onNewVersion(self, evt):
        if hasattr(evt, "version"):
            dlg = wx.MessageDialog(self.frame,
                                   _("There is a new version (%s) available."
                                     "\nWould you like to download it?") %
                                   evt.version,
                                   _("New Version Information"),
                                   wx.ICON_INFORMATION | wx.YES_NO)
            if dlg.ShowModal() == wx.ID_YES:
                webbrowser.open(HOME_PAGE_URL)
            dlg.Destroy()

    @property
    def dbConn(self):
        if(self._dbConn is None):
            self._dbConn = MainSql()
        return self._dbConn

    @dbConn.setter
    def dbConn(self, v):
        self._dbConn = v

    def openReportTool(self, evt=None):

        ret = wx.MessageBox(_("You must first save your data. "
                              "Do you accept it?"), _("Question"),
                            wx.YES_NO | wx.YES_DEFAULT | wx.CENTER)
        if(ret == wx.NO):
            return
        else:
            self.saveMainData()

        r = self.selectedCell[0]
        id_ = self.myGrid.GetCellValue(r, 0)
        deviceNo = self.myGrid.GetCellValue(r, 1)
        reporttool.ReportStore(self.frame, self.frame.GetIcon(),
                               recID=id_, devNo=deviceNo, caller=self)

    def attachGridFilePath(self, evt):
        row, col = self.selectedCell
        dlg = wx.FileDialog(self.frame, message=_("Choose a file"),
                            defaultDir=os.getcwd(),
                            defaultFile="",
                            wildcard="*.*",
                            style=wx.OPEN
                            )
        if dlg.ShowModal() == wx.ID_OK:
            paths = dlg.GetPaths()
            if paths:
                self.myGrid.SetCellValue(row, 7, os.path.abspath(paths[0]))
        dlg.Destroy()

    def openGridFilePath(self, evt):
        row, col = self.selectedCell
        val = self.myGrid.GetCellValue(row, 7)
        if val:
            if not os.path.exists(val):
                self.message(_("File path is not a valid path"),
                             flag=wx.ICON_ERROR)
            try:
                os.startfile(val)
            except:
                tb = traceback.format_exc()
                self.message(tb, flag=wx.ICON_ERROR)
        else:
            self.message(_("File path does not exist"),
                         flag=wx.ICON_ERROR)

    def openGridFileDir(self, evt):
        row, col = self.selectedCell
        val = self.myGrid.GetCellValue(row, 7)
        if val:
            if not os.path.exists(val):
                self.message(_("File path is not a valid path"),
                             flag=wx.ICON_ERROR)
            try:
                if IS_WINDOWS:
                    subprocess.call('explorer /select,"%s"' % val)
                else:
                    fdir = os.path.basename(val)
                    os.startfile(fdir)
            except:
                pass
        else:
            self.message(_("File path does not exist"),
                         flag=wx.ICON_ERROR)

    def clearGridFilePath(self, evt):
        row, col = self.selectedCell
        dlg = wx.MessageDialog(self.frame,
                               _("Do you want to clear file path?"),
                               _("Warning"), wx.YES_NO | wx.NO_DEFAULT)
        if dlg.ShowModal() == wx.ID_YES:
            self.myGrid.SetCellValue(row, 7, "")
        dlg.Destroy()

    def onIconize(self, event):
        self.frame.Hide()
        self.trayicon.ShowBalloon("Kalibro", _("Minimized"), 2000,
                                  wx.ICON_INFORMATION)
        event.Skip()

    def onGridLeftDClick(self, evt):
        if self.myGrid.CanEnableCellControl():
            self.myGrid.EnableCellEditControl()
        evt.Skip()

    def onGridRangeSelect(self, evt):
        if evt.Selecting():
            coordinate1 = str(evt.GetTopLeftCoords())
            coordinate2 = str(evt.GetBottomRightCoords())

            for k in ["(", ")", ","]:
                coordinate1 = coordinate1.replace(k, "")
                coordinate2 = coordinate2.replace(k, "")

            coordinate1 = coordinate1.split(" ", 1)
            coordinate2 = coordinate2.split(" ", 1)

            coordinate1 = [int(i) for i in coordinate1]
            coordinate2 = [int(j) for j in coordinate2]

            self.selectedCellRange = [coordinate1, coordinate2]
        evt.Skip()

    def onLeftClickGridCell(self, evt):
        self.selectedCell = [evt.GetRow(), evt.GetCol()]
        evt.Skip()

    def getSelectedData(self):
        wholeData = ""

        try:
            i = self.selectedCellRange[0][0]
            j = self.selectedCellRange[0][1]
            k = self.selectedCellRange[1][0]
            m = self.selectedCellRange[1][1]

            for z in range(i, k + 1):
                for s in range(j, m + 1):
                    wholeData += (str(self.myGrid.GetCellValue(z, s)) +
                                  ("\t" if(s != m) else ""))
                else:
                    wholeData += os.linesep
        except AttributeError:
            wholeData = self.myGrid.GetCellValue(self.selectedCell[0],
                                                 self.selectedCell[1])
        return wholeData

    def onGridCopy(self, evt):
        if wx.TheClipboard.Open():
            wx.TheClipboard.Clear()
            wholeData = self.getSelectedData()
            wx.TheClipboard.SetData(wx.TextDataObject(wholeData))
            wx.TheClipboard.Close()
        evt.Skip()

    def onGridPaste(self, evt):
        do = wx.TextDataObject()
        wx.TheClipboard.Open()
        success = wx.TheClipboard.GetData(do)
        wx.TheClipboard.Close()
        if success:
            wholeData = do.GetText()

            line = wholeData.split(os.linesep)

            try:
                row, col = self.selectedCell
            except AttributeError:
                self.selectedCell = [0, 0]
                row, col = 0, 0

            row, col = self.selectedCell
            rowS, colS = row, col
            totRow = self.myGrid.GetNumberRows()
            totCol = self.myGrid.GetNumberCols()
            sizeExceeded = 0
            for i in range(len(line)):
                if sizeExceeded == 1:
                    break
                words = line[i].split("\t")
                for k in words:

                    if(self.myGrid.IsReadOnly(row, col)):
                        self.message(_("You are trying to paste the values"
                                       " into read-only cells. "
                                       "Try different cells!"))
                        return
                    if(row >= totRow or col >= totCol):
                        sizeExceeded = 1
                        break
                    self.myGrid.SetCellValue(row, col, k)
                    col += 1
                else:
                    row += 1
                    col = colS
        else:
            pass

    def init(self):
        config = ConfigManager()
        try:
            self.lang = config["language"]
            self.date_format = config["date_format"]
            self.file_encoding = config["file_encoding"]
        except Exception as e:
            if DEBUG:
                log_debug(debug_trace(), traceback.format_exc())
            self.message(str(e))
            self.exit()
        amodel = ModelAuth()
        username = get_user()
        installed = amodel.auth_installed()
        if installed:
            self.has_write_permission = amodel.has_write_perm(username)
            self.can_change_dev_status = amodel.can_change_dev_status(username)
            self.can_change_dev_owner = amodel.can_change_dev_owner(username)
        else:
            self.has_write_permission = True
            self.can_change_dev_status = True
            self.can_change_dev_owner = True

    def init2(self):
        if not os.path.exists("kalibro.png"):
            return
        img = wx.Image("kalibro.png")
        img.ConvertAlphaToMask()
        bitmap = wx.BitmapFromImage(img)
        frame = AdvSplash.AdvancedSplash(self.frame, bitmap=bitmap,
                                         timeout=2000,
                                         agwStyle=AdvSplash.AS_TIMEOUT |
                                         AdvSplash.AS_CENTER_ON_SCREEN)
        wx.Yield()

    def setLanguage(self, lang="en"):
        try:
            ConfigManager()["language"] = lang
            self.message(_("You need to restart the program"))
        except Exception as e:
            self.message(str(e))

    @nodebug
    def timerHandler(self, event=None, con=False):
        self.numTimerCalls += 3
        self.statusGauge.SetValue(self.numTimerCalls >= 50 and 50 or
                                  self.numTimerCalls)
        if not con:
            if(self.numTimerCalls > 50):
                self.numTimerCalls = 0
                self.statusGauge.SetValue(0)
                self.stopTimer()
        else:
            self.numTimerCalls %= 50

    def stopTimer(self, event=None):
        self.myTimer.Stop()
        self.myTimer_cont.Stop()

    def createFileEncodingDialog(self, evt=None):
        encDic = self.getEncodings()
        dlg = wx.SingleChoiceDialog(
            self.frame,
            _('Choose one of the encodings\n') +
            _("Your system encoding: %s\n") % DEF_FILE_ENCODING +
            _("Prefered enconding: utf_8"),
            _('Set File Encoding'),
            encDic,
            wx.CHOICEDLG_STYLE
            )

        dlg.SetSelection(encDic.index(self.file_encoding))

        if dlg.ShowModal() == wx.ID_OK:
            selFileEnc = encDic[dlg.GetSelection()]
            if(selFileEnc != self.file_encoding):
                ConfigManager()["file_encoding"] = selFileEnc
                self.file_encoding = selFileEnc

        dlg.Destroy()

    def getEncodings(self):
        lst = ConfigManager().installed_encodings()
        lst.sort()
        return lst

    def addMenu(self):
        menuBar = wx.MenuBar()
        programMenu = wx.Menu()
        operationsMenu = wx.Menu()
        basicDataMenu = wx.Menu()
        settingsMenu = wx.Menu()
        localize = wx.Menu()
        helpMenu = wx.Menu()

        programMenu.Append(MY_MENU_ICONIZE, _("&Minimize") + "\tCtrl+H",
                           _("Minimize to Task Bar"))
        programMenu.AppendSeparator()
        programMenu.Append(MY_MENU_QUIT, _("&Exit") + "\tCtrl+Q",
                           _("Exit Program"))

        operationsMenu.Append(MY_MENU_SAVE, _("&Save") + "\tCtrl+S",
                              _("Click to Save"))
        operationsMenu.Append(MY_MENU_ADDNEW, _("&Add Device") + "\tCtrl+N",
                              _("Click to Add New Device to System"))
        operationsMenu.Append(MY_MENU_ADDSUBNEW, _("Add &Calibration Record") +
                              "\tCtrl+Shift+N",
                              _("Click to Add Calibration Record"))
        basicDataMenu.Append(MY_MENU_ADDSTATUS, _("Define Device &Status"),
                             _("Click to Add or Remove Device Status Types"))
        basicDataMenu.Append(MY_MENU_ADDCALRESULT,
                             _("Define Calibration &Result"),
                             _("Click to Add or Remove Calibration "
                               "Result Types"))
        basicDataMenu.Append(MY_MENU_ADDOWNER, _("Define Device &Owner"),
                             _("Click to Add or Remove Device Owners"))
        operationsMenu.Append(MY_MENU_CAL_CTRL, _("Ca&libration Control") +
                              "\tCtrl+Shift+C",
                              _("List the Devices' Calibration Schedule"))

        operationsMenu.Append(MY_MENU_REPORT,
                              _("&Reporting") + "\tCtrl+Shift+R",
                              _("View html and CSV reports"))
        operationsMenu.Append(MY_MENU_HISTLOG,
                              _("&History Log") + "\tCtrl+Shift+H",
                              _("View History Log"))
        config = ConfigManager()
        langs = config.installed_langs()
        langDict = {"en": _("English"),
                    "tr": _("Turkish"),
                    "pl": _("Polish")}
        if not langs:
            langs = ["en"]
        for lang in langs:
            _menuID = wx.NewId()
            localize.Append(_menuID, langDict.get(lang, lang),
                            _("Click to change the language to %s") %
                            langDict.get(lang, lang),
                            wx.ITEM_RADIO)
            self.frame.Bind(wx.EVT_MENU,
                            (lambda lang:
                             lambda evt: self.setLanguage(lang=lang))(lang),
                            id=_menuID)
            if lang == self.lang:
                localize.Check(_menuID, True)

        localize.AppendSeparator()
        localize.Append(MY_MENU_DATEFORMAT, _("&Date Format"),
                        _("Click to change date format"))

        settingsMenu.Append(MY_MENU_PASSCHG, _("&Change Password"),
                            _("Click to change your password"))
        settingsMenu.Append(MY_MENU_USER, _("&User Management"),
                            _("Click to add/remove users and "
                              "change users' password"))
        settingsMenu.Append(MY_MENU_ENCODINGS, _("&File Encodings"),
                            _("Click to select file encoding "
                              "selection dialog"))
        settingsMenu.AppendMenu(MY_MENU_LOCALIZE, _("&Localize"), localize)
        settingsMenu.AppendSeparator()
        settingsMenu.Append(MY_MENU_VERSION_CHECK, _("&Version Check"),
                            _("Check to enable new version "
                              "control at startup"),
                            wx.ITEM_CHECK)
        settingsMenu.Append(MY_MENU_SPLASH, _("&Splash Window"),
                            _("Check to enable splash window at startup"),
                            wx.ITEM_CHECK)

        helpMenu.Append(MY_MENU_HELP, _("&Help") + "\tF1", _("Help"))
        helpMenu.Append(MY_MENU_README, _("&Read Me"), _("Read Me"))
        helpMenu.Append(MY_MENU_REL_NOTES, _("R&elease Notes"),
                     _("Release Notes"))
        helpMenu.Append(MY_MENU_ABOUT, _("&About"), _("About Kalibro"))
        helpMenu.Append(MY_MENU_DONATE, _("&Donate"), _("Donate to Kalibro"))

        settingsMenu.Check(MY_MENU_VERSION_CHECK, config["version_check"])
        settingsMenu.Check(MY_MENU_SPLASH, config["splash"])

        operationsMenu.Enable(MY_MENU_SAVE, self.has_write_permission)
        operationsMenu.Enable(MY_MENU_ADDNEW, self.has_write_permission)
        operationsMenu.Enable(MY_MENU_ADDSUBNEW, self.has_write_permission)
        basicDataMenu.Enable(MY_MENU_ADDCALRESULT, self.has_write_permission)
        basicDataMenu.Enable(MY_MENU_ADDOWNER, self.has_write_permission)
        basicDataMenu.Enable(MY_MENU_ADDSTATUS, self.has_write_permission)
        settingsMenu.Enable(MY_MENU_USER, self.has_write_permission)
        settingsMenu.Enable(MY_MENU_ENCODINGS, self.has_write_permission)
        settingsMenu.Enable(MY_MENU_LOCALIZE, self.has_write_permission)
        settingsMenu.Enable(MY_MENU_VERSION_CHECK, self.has_write_permission)
        settingsMenu.Enable(MY_MENU_SPLASH, self.has_write_permission)

        menuBar.Append(programMenu, _("&Program"))
        menuBar.Append(operationsMenu, _("&Operations"))
        menuBar.Append(basicDataMenu, _("&Basic Data"))
        menuBar.Append(settingsMenu, _("&Settings"))
#        menuBar.Append(localize, _("Localize"))
        menuBar.Append(helpMenu, _("&Help"))
        self.frame.SetMenuBar(menuBar)
        self.frame.CreateStatusBar()

    def addPopupMenu2Grid(self, evt=None):
        #  get the selected cell
        gR, gC = evt.GetRow(), evt.GetCol()

        self.selectedCell = [gR, gC]
        self.myGrid.SetFocus()
        self.myGrid.SetGridCursor(gR, gC)

        # make a menu
        self.PopMenu = wx.Menu()
        # Show how to put an icon in the menu

        self.PopMenu.Append(MY_POPUP_ID4, _("Copy"))
        self.PopMenu.Append(MY_POPUP_ID5, _("Paste"))
        self.PopMenu.Append(MY_POPUP_ID1, _("Save"))
        self.PopMenu.Append(MY_POPUP_ID2, _("Add Calibration Record"))
        self.PopMenu.Append(MY_POPUP_ID3, _("Save Grid To &File"))
        self.PopMenu.Append(MY_POPUP_ID6, _("Date Picker"))
        self.PopMenu.Append(MY_POPUP_ID7, _("Delete Record"))
        self.PopMenu.Append(MY_POPUP_ID8, _("Open Report Store"))
        self.PopMenu.AppendSeparator()
        filePathMenu = wx.Menu()
        self.PopMenu.AppendMenu(MY_POPUP_ID9, _("File"), filePathMenu)
        filePathMenu.Append(MY_POPUP_ID10, _("Attach File Path"))
        filePathMenu.Append(MY_POPUP_ID11, _("Open File"))
        filePathMenu.Append(MY_POPUP_ID12, _("Open Dir"))
        filePathMenu.Append(MY_POPUP_ID13, _("Clear File Path"))
        #######################################################################
        if(self.mainRegistryNo.GetValue() == "" or
           self.myGrid.GetCellValue(gR, 0) == ""):
            self.PopMenu.Enable(MY_POPUP_ID8, False)
            self.PopMenu.Enable(MY_POPUP_ID9, False)
        self.PopMenu.Enable(MY_POPUP_ID1, self.has_write_permission)
        self.PopMenu.Enable(MY_POPUP_ID2, self.has_write_permission)
        self.PopMenu.Enable(MY_POPUP_ID3, self.has_write_permission)
        self.PopMenu.Enable(MY_POPUP_ID5, self.has_write_permission)
        self.PopMenu.Enable(MY_POPUP_ID6, self.has_write_permission)
        self.PopMenu.Enable(MY_POPUP_ID7, self.has_write_permission)
        self.PopMenu.Enable(MY_POPUP_ID10, self.has_write_permission)
        self.PopMenu.Enable(MY_POPUP_ID13, self.has_write_permission)

        self.myGrid.PopupMenu(self.PopMenu)
        self.PopMenu.Destroy()

    def fetchMainData(self, event=None):
        self.myTimer.Start(50)
        index = self.openList.GetSelection()
        regno = self.openList.GetString(index)
        tail = {"regno": regno}
        query = "SELECT * FROM main WHERE registryNo=:regno"

        try:
            myob = self.dbConn
        except:
            self.message(_("Unable To Reach Database"))
            if DEBUG:
                log_debug(debug_trace(), traceback.format_exc())
            return

        try:
            result = myob.fetch_all(query=query, tail=tail).result
        except:
            self.message(_("During Data Fetching From Database\n"
                           "An Error Occured!"))
            if DEBUG:
                log_debug(debug_trace(), traceback.format_exc())
            return

        i = j = 0
        tempList = []
        for i in range(0, len(result)):
            for j in range(0, len(result[i])):
                temp = "" if result[i][j] is None else result[i][j]
                if(j == 5):
                    if(temp != ""):
                        if isinstance(temp, basestring):
                            temp2 = datetime.strptime(temp, "%Y-%m-%d")
                        elif isinstance(temp, (date, datetime)):
                            temp2 = temp
                        temp = temp2.strftime(self.date_format)
                tempList.append(temp)

        self.mainRegistryNo.SetValue(tempList[0])
        self.currentRegNo = tempList[0]
        self.mainRegistryNo.SetEditable(False)
        self.mainDeviceName.SetValue(tempList[1])
        self.mainDeviceManuf.SetValue(tempList[2])
        self.mainManufSerialNo.SetValue(tempList[3])
        self.mainFeatures.SetValue(tempList[4])
        self.mainComingDate.SetValue(tempList[5])
        self.mainDescription.SetValue(tempList[6])
        self.cbDevStatus.SetValue(tempList[7])
        self.cbOwner.SetValue(tempList[8])

        self.clearGrid(event)

        self.dbConn = None
        self.fetchSubData(regno)

    def fetchSubData(self, regno):
        try:
            myob = self.dbConn
        except:
            self.message(_("Unable to Reach Database"))
            if DEBUG:
                log_debug(debug_trace(), traceback.format_exc())
            return

        tail = {"regno": regno}
        query = "SELECT * FROM subrecords WHERE registryNo=:regno"
        try:
            results = myob.fetch_all(query=query, tail=tail).result
        except:
            self.message(_("During Data Fetching From Database\n"
                           "Error Occured!"))
            if DEBUG:
                log_debug(debug_trace(), traceback.format_exc())
            return

        if(len(results) == 0):
            return

        self.clearGrid()
        i = j = 0
        for i in range(0, len(results)):
            for j in range(0, len(results[i])):
                temp = ("" if results[i][j] is None else results[i][j])

                if(j == 2 or j == 3):
                    if(temp != ""):
                        if isinstance(temp, basestring):
                            temp2 = datetime.strptime(temp, "%Y-%m-%d")
                        elif isinstance(temp, (date, datetime)):
                            temp2 = temp
                        temp = temp2.strftime(self.date_format)

                self.myGrid.SetCellValue(i, j, str(temp))
                self.myGrid.SetCellBackgroundColour(i, j, wx.CYAN)

    @needs_write_permission
    def saveMainData(self, event=None):
        self.myTimer.Start(75)
        if(self.mainRegistryNo.GetValue() == ""):
            self.message(_("Device Registry Number is Missing"))
            return
        registryNo = self.mainRegistryNo.GetValue().strip()
        deviceName = self.mainDeviceName.GetValue().strip()
        deviceManuf = self.mainDeviceManuf.GetValue().strip()
        manufSerialNo = self.mainManufSerialNo.GetValue().strip()
        features = self.mainFeatures.GetValue().strip()
        devStatus = self.cbDevStatus.GetStringSelection() or None
        owner = self.cbOwner.GetStringSelection() or None
        comingDate = self.mainComingDate.GetValue().strip()
        if(comingDate != ""):
            try:
                comingDate = \
                    datetime.strptime(comingDate, self.date_format)
            except ValueError:
                self.message(_("Invalid Date Format"))
                return
        else:
            comingDate = None
        aciklama = self.mainDescription.GetValue().strip()
        op_type = "insert" if self.mainRegistryNo.IsEditable() else "update"
        queryInsert = """INSERT INTO main (registryNo,deviceName,
            deviceManuf,manufSerialNo,features,comingDate,description,
            status,owner)
            VALUES (:registryNo,:deviceName,:deviceManuf,:manufSerialNo,
            :features,:comingDate,:description,:status,:owner)"""
        queryUpdate = """UPDATE main SET deviceName=:deviceName,
            deviceManuf=:deviceManuf,manufSerialNo=:manufSerialNo,
            features=:features,comingDate=:comingDate,
            description=:description,status=:status,owner=:owner
            WHERE registryNo=:registryNo
            """
        tail = {"registryNo": registryNo, "deviceName": deviceName,
                "deviceManuf": deviceManuf, "manufSerialNo": manufSerialNo,
                "features": features, "comingDate": comingDate,
                "description": aciklama, "status": devStatus,
                "owner": owner}
        try:
            myob = self.dbConn
        except:
            self.message(_("Unable to Reach Database"))
            if DEBUG:
                log_debug(debug_trace(), traceback.format_exc())
            return
        try:
            if op_type == "insert":
                try:
                    myob.execStmt(queryInsert, tail)
                except IntegrityError:
                    self.message(_('Current registry no is already in database'
                                   ' try different one'), wx.ICON_ERROR)
                    return
            elif op_type == "update":
                myob.execStmt(queryUpdate, tail)
        except:
            if DEBUG:
                log_debug(debug_trace(), traceback.format_exc())
            msg = _("While Saving, Error Occured. Saving Interrupted!")
            self.message(msg)
            return
        self.mainRegistryNo.SetEditable(False)
        self.updateDeviceList()
        if(self.myGrid.GetCellValue(0, 0) == ""):
            msg = _("Saved")
            self.message(msg)
            return
        self.saveGridData(event)

    @needs_write_permission
    def saveGridData(self, event=None):

        #  Get the Unique Id of the device name from the main form
        regno = self.mainRegistryNo.GetValue()
        rownum = self.myGrid.GetNumberRows()
        colnum = self.myGrid.GetNumberCols()
        for i in range(rownum):

            #  prepare the query string
            queryInsert = ("INSERT INTO subrecords (id,registryNo) "
                           "VALUES (:id,:regno)")
            tailInsert = {"regno": regno}
            queryUpdate = "UPDATE subrecords"
            tailUpdate = {"regno": regno}

            if(self.myGrid.GetCellValue(i, 0) == ""):
                break
            qryStr = []

            for j in range(colnum):
                temp = self.myGrid.GetCellValue(i, j)

                try:
                    if(j == 2 or j == 3):
                        if(temp != ""):
                            temp = datetime.strptime(temp, self.date_format)
                        else:
                            temp = None
                except Exception as e:
                    msg = _("Error Occured. Unable to Save. Reason: ") + str(e)
                    msg += "\n" + _("Fix and Try it Again")
                    self.message(msg)
                    if DEBUG:
                        log_debug(debug_trace(), traceback.format_exc())
                    return

                if(j == 0):
                    # ID of the row. It is the primary key in the db
                    tailInsert["id"] = tailUpdate["id"] = temp
                    try:
                        myob = self.dbConn
                        try:
                            myob.execStmt(queryInsert, tailInsert)
                        except IntegrityError:
                            pass
                    except:
                        self.message(_("Database Error"))
                        if DEBUG:
                            log_debug(debug_trace(), traceback.format_exc())
                        return

                elif(j == 1):
                    pass
                elif(j == 2):
                    if(temp is not None):
                        qryStr.append("plannedDate=:plannedDate")
                        tailUpdate["plannedDate"] = temp
                    else:
                        qryStr.append("plannedDate=NULL")
                elif(j == 3):
                    if(temp is not None):
                        qryStr.append("realizedDate=:realizedDate")
                        tailUpdate["realizedDate"] = temp
                    else:
                        qryStr.append("realizedDate=NULL")
                elif(j == 4):
                    qryStr.append("trackingNo=:trackingNo")
                    tailUpdate["trackingNo"] = temp
                elif(j == 5):
                    qryStr.append("method=:method")
                    tailUpdate["method"] = temp
                elif(j == 6):
                    qryStr.append("result=:result")
                    tailUpdate["result"] = temp or None
                elif(j == 7):
                    qryStr.append("path=:path")
                    tailUpdate["path"] = temp or None

            queryUpdate = (queryUpdate + " SET " + " , ".join(qryStr) +
                           " WHERE id=:id AND registryNo=:regno")
            try:
                myob = self.dbConn
            except:
                self.message(_("Unable to Reach Database"))
                if DEBUG:
                    log_debug(debug_trace(), traceback.format_exc())
                return

            try:
                myob.execStmt(query=queryUpdate, tail=tailUpdate)
            except:
                msg = _("While Saving, Error Occured. Saving Interrupted!")
                self.message(msg)
                if DEBUG:
                    log_debug(debug_trace(), traceback.format_exc())
                return

        self.saveRepNumToReportBlobDataTbl()
        self.updateDeviceList()
        self.clearGrid()
        self.fetchSubData(regno)
        msg = _("Saved")
        self.message(msg)
        if event:
            event.Skip()

    def saveRepNumToReportBlobDataTbl(self):
        """
        UPDATES reportBlobData table when save button clicked
        """
        query = """UPDATE reportBlobData SET reportNo=?
                WHERE id=? AND registryNo=?"""
        regNo = self.mainRegistryNo.GetValue()
        myob = self.dbConn
        i = 0
        while True:
            id_ = self.myGrid.GetCellValue(i, 0)
            if id_ != "":
                repNo = self.myGrid.GetCellValue(i, 4)
                #  Number 4 (four) for tracking no in the grid
                if repNo != "":
                    tail = (repNo, id_, regNo)
                    myob.execStmt(query, tail)
                else:
                    pass
            else:
                break
            i += 1

    def onOptimizeDB(self, evt=None, silent=False):
        myob = self.dbConn
        initS, finS = myob.optimizeDB()
        savedSize = initS - finS
        line1 = _("Before Optimization Database Size: %s bytes\n") % (initS)
        line2 = _("After Optimization Database Size: %s bytes\n") % (finS)
        line3 = _("You saved: %s bytes") % (savedSize)
        msg = line1 + line2 + line3
        if not silent:
            wx.MessageBox(message=msg, caption=_("Information"),
                          style=wx.OK | wx.CENTER)

    def onAddField(self, evt=None):
        if self.mainRegistryNo.GetValue() == "":
            return
        AdditionalFieldsWin(self.frame, self.frame.GetIcon(),
                            self.mainRegistryNo.GetValue())

    def exit(self, event=None):
        self.frame.Hide()
        map(lambda x: x.Hide(), self.frame.GetChildren())
        try:
            self.trayicon.removeAll()
        except AttributeError:
            pass
        self.frame.Destroy()
        # RUN DB OPTIMIZATION
        try:
            self.onOptimizeDB(silent=True)
        except:
            if DEBUG:
                log_debug(debug_trace(), traceback.format_exc())
        # close thread gracefully
        if(hasattr(self, "_thrNewVersion") and
           hasattr(self._thrNewVersion, "is_alive") and
           self._thrNewVersion.is_alive()):
                self._thrNewVersion.join(5)
                if DEBUG:
                    log_debug(debug_trace(),
                              "thread %s joined" % self._thrNewVersion)
        event.Skip()

    def message(self, msg="", flag=0):
        self.info_bar.ShowMessage(msg, flag)
        wx.CallLater(3000, self.info_bar.Dismiss)
        wx.CallLater(3100, self.frame.Refresh)

    def updateDeviceList(self, event=None):
        regno = self.quickSearch.GetValue().strip()
        tail = ()
        where = ""
        if regno:
            where = "WHERE registryNo LIKE ?"
            regno = "%" + regno + "%"
            tail = (regno,)

        myob = self.dbConn
        query = """SELECT registryNo FROM main %s
            ORDER BY registryNo COLLATE naturalsort ASC""" % where

        results = myob.fetch_all(query, tail).result
        i = j = 0
        tempList = []
        for i in range(0, len(results)):
            for j in range(0, len(results[i])):
                tempList.append(results[i][j])

        self.openList.Clear()
        if(len(tempList) > 0):
            self.openList.InsertItems(tempList, 0)
        if event:
            event.Skip()

    def createGridList(self):

        self.myGrid = CalibrationGrid(self.panel1, -1,
                                      size=(self.frame_width - 10, -1),
                                      numrows=50, numcols=8)
        self.gridResultChoiceComboCtrl = None
        self.createGridResultChoice()

    def updateCalibrationResult(self):
        combo = self.gridResultChoiceComboCtrl
        items = self.getGridResultChoiceItems()
        combo.Clear()
        combo.AppendItems(items)

    def onGridResultChoiceEditorCreated(self, evt):
        column = evt.GetCol()
        if column == 6:
            self.gridResultChoiceComboCtrl = evt.GetControl()
            self.updateCalibrationResult()
            pubsub.subscribe("update_calibration_result",
                             self.updateCalibrationResult)
        evt.Skip()

    def getGridResultChoiceItems(self):
        qry = '''SELECT name FROM result ORDER BY name ASC'''
        result = self.dbConn.fetch_all(qry).result
        items = [""] + [i[0] for i in result if i[0]]
        return items

    def createGridResultChoice(self):
        '''
        make the result column editor as choice editor
        '''
        result_cheditor = wx.grid.GridCellChoiceEditor([], False)
        for i in range(self.myGrid.GetNumberRows()):
            self.myGrid.SetCellEditor(i, 6, result_cheditor)

    def grid2File(self, event=None):
        """Save Grid Data to a file"""

        wildcard = _("CSV (*.csv)|*.csv|\nAll Files (*.*)|*.*")

        dlg = wx.FileDialog(
            self.panel1, message=_("Save As ..."), defaultDir=os.getcwd(),
            defaultFile="", wildcard=wildcard,
            style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT
            )
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            data = []
            self.myTimer_cont.Start(5)
            rownum = self.myGrid.GetNumberRows()
            colnum = self.myGrid.GetNumberCols()
            for i in range(rownum):
                if(self.myGrid.GetCellValue(i, 0) == ""):
                    break
                tmp = []
                for j in range(colnum):
                    tmp.append(self.myGrid.GetCellValue(i, j))
                data.append(tmp)
            columns = []
            for j in range(colnum):
                tmp = sanitize_space(self.myGrid.GetColLabelValue(j))
                if j == 0 and tmp == "ID":
                    tmp = "Id"
                columns.append(tmp)
            data.insert(0, columns)
            try:
                with io.open(path, "wb", buffering=0) as cf:
                    cwriter = UnicodeWriter(cf, delimiter=b';',
                                            encoding=self.file_encoding)
                    cwriter.writerows(data)
                    cf.flush()
            except:
                self.message(_("While Saving, An Error Occured. "
                               "Check your file encoding settings"))
                if DEBUG:
                    log_debug(debug_trace(), traceback.format_exc())
            finally:
                self.stopTimer()

        dlg.Destroy()

    def clearGrid(self, event=None):
        """
        Clear All Grid Cells
        """
        self.myGrid.ClearGrid()
        colnum = self.myGrid.GetNumberCols()
        rownum = self.myGrid.GetNumberRows()
        for i in range(colnum + 1):
            for j in range(rownum + 1):
                self.myGrid.SetCellBackgroundColour(j, i, wx.WHITE)

    @needs_write_permission
    def createSubRecord(self, event=None):
        """
        Add New Calibration Record to Grid
        """
        registryNo = self.mainRegistryNo.GetValue()
        if not registryNo:
            wx.MessageBox(_("Device is missing.\nAdd Device First, "
                            "then Add Sub Record"), _("Warning"), wx.OK)
            return
        idList = []  # ID List of the records in the grid

        i = 0
        newPlaceRow = 0
        while True:
            val = self.myGrid.GetCellValue(i, 0)
            if not val:
                newPlaceRow = i
                break
            val = int(val)
            idList.append(val)
            i += 1
        #  ID of the new to be added sub record
        #  (id is the primary key of sub records in database)
        if(len(idList) > 0):
            newId = max(idList) + 1
        else:
            newId = 1
        colnum = self.myGrid.GetNumberCols()
        for i in range(colnum):
            if(i == 0):
                self.myGrid.SetCellValue(newPlaceRow, 0, str(newId))
                self.myGrid.SetCellValue(newPlaceRow, 1, registryNo)
            self.myGrid.SetCellBackgroundColour(newPlaceRow, i, "#FF9C00")

    @needs_write_permission
    def createNewDevice(self, event=None):
        """
        Create New Device (It creates a new record in main table in DB)
        """
        ret = wx.MessageBox(_("Do you want to create a new record?"),
                            _("Question"), wx.YES_NO | wx.NO_DEFAULT)
        if(ret == wx.NO):
            return
        else:
            self.mainRegistryNo.Clear()
            self.mainRegistryNo.SetEditable(True)
            self.mainDeviceName.Clear()
            self.mainDeviceName.SetEditable(True)
            self.mainDeviceManuf.Clear()
            self.mainDeviceManuf.SetEditable(True)
            self.mainManufSerialNo.Clear()
            self.mainManufSerialNo.SetEditable(True)
            self.mainFeatures.Clear()
            self.mainFeatures.SetEditable(True)
            self.mainComingDate.Clear()
            self.mainDescription.Clear()
            # After clear set the focus to the first entry
            self.mainRegistryNo.SetFocus()
            self.clearGrid(event)

    @needs_write_permission
    def deleteMainRecord(self, evt=None):
        regNo = self.mainRegistryNo.GetValue()
        if not regNo or self.currentRegNo is None:
            return

        ret = wx.MessageBox(_("Do you really want to delete -> %s ?") % regNo,
                            _("Question"), wx.YES_NO | wx.NO_DEFAULT |
                            wx.ICON_EXCLAMATION)
        if(ret == wx.NO):
            return
        myob = self.dbConn
        delquery = "DELETE FROM main WHERE registryNo=:regno"
        delquerySub = "DELETE FROM subrecords WHERE registryNo=:regno"
        delqueryAdditional = "DELETE FROM mainAddField WHERE registryNo=:regno"
        delqueryReport = "DELETE FROM reportBlobData WHERE registryNo=:regno"
        tail = {"regno": regNo}
        try:
            try:
                myob.execStmt(query=delqueryReport, tail=tail)
            except:
                msg = _("Error Occured While Deleting Reports")
                self.message(msg)
                if DEBUG:
                    log_debug(debug_trace(), traceback.format_exc())
                return
            try:
                myob.execStmt(query=delquerySub, tail=tail)
            except:
                msg = _("Error Occured While Deleting Subrecord")
                self.message(msg)
                if DEBUG:
                    log_debug(debug_trace(), traceback.format_exc())
                return
            try:
                myob.execStmt(query=delqueryAdditional, tail=tail)
            except:
                msg = _("Error Occured While Deleting Additional Record")
                self.message(msg)
                if DEBUG:
                    log_debug(debug_trace(), traceback.format_exc())
                return
            try:
                myob.execStmt(query=delquery, tail=tail)
            except:
                msg = _("Error Occured While Deleting Main Record")
                self.message(msg)
                if DEBUG:
                    log_debug(debug_trace(), traceback.format_exc())
                return
        except:
            msg = _("Error Occured While Deleting")
            self.message(msg)
            if DEBUG:
                log_debug(debug_trace(), traceback.format_exc())
            return
        else:
            msg = _("Deleted")
            self.message(msg)
            self.updateDeviceList()
            self.currentRegNo = None
            self.clearGrid()

    @needs_write_permission
    def deleteSubRecord(self, evt):
        ret = wx.MessageBox(_("Do you really want to delete this record?"),
                            _("Question"),
                            wx.YES_NO | wx.NO_DEFAULT | wx.ICON_EXCLAMATION)
        if(ret == wx.NO):
            return
        r, c = self.selectedCell
        id, regno = (self.myGrid.GetCellValue(r, 0),
                     self.myGrid.GetCellValue(r, 1))

        if(id == "" or regno == ""):
            msg = _("Nothing is selected")
            self.message(msg)
            return
        tail = {"id": id, "regno": regno}
        delquerySub = """DELETE FROM subrecords
                        WHERE id=:id AND registryNo=:regno"""
        myob = self.dbConn
        try:
            myob.execStmt(query=delquerySub, tail=tail)
        except:
            msg = _("Error Occured While Deleting Subrecord "
                    "id=%(id)s AND registryNo=%(regno)s") % tail
            self.message(msg)
            if DEBUG:
                log_debug(debug_trace(), traceback.format_exc())
            return
        else:
            msg = _("Deleted\n"
                    "id=%(id)s AND registryNo=%(regno)s") % tail
            self.message(msg)
            self.fetchSubData(regno)

    def onClearMainForm(self, event=None):
        ret = wx.MessageBox(_("Do you want to clear the form?"), _("Question"),
                            wx.YES_NO | wx.NO_DEFAULT | wx.CENTER)
        if(ret == wx.NO):
            return
        else:
            self.myTimer.Start(25)

            self.mainRegistryNo.Clear()
            self.currentRegNo = None
            self.mainRegistryNo.SetEditable(False)
            self.mainDeviceName.Clear()
            self.mainDeviceManuf.Clear()
            self.mainManufSerialNo.Clear()
            self.mainFeatures.Clear()
            self.mainComingDate.Clear()
            self.mainDescription.Clear()
            self.cbDevStatus.SetValue("")
            self.cbOwner.SetValue("")
            # After clear set the focus to the first entry
            self.mainRegistryNo.SetFocus()

            # Call clearGrid function to clear the whole grid content
            self.clearGrid(event)

    def createMainWidgets(self):
        """
        Creates the main window widgets
        """
        self.quickSearch = wx.SearchCtrl(self.panel1,
                                         style=wx.TE_PROCESS_ENTER)
        self.quickSearch.SetDescriptiveText(_("Quick Search"))
        self.quickSearch.SetToolTipString(_("Search in registry numbers"))
        self.quickSearch.ShowCancelButton(True)

        statlab = wx.StaticText(self.panel1, id=-1, label=_("List of Devices"))
        self.openList = wx.ListBox(self.panel1, -1, size=(200, 200),
                                   style=wx.LB_SINGLE | wx.LB_HSCROLL)
        self.openList.SetToolTipString(_("Double Click To View"))

        regnoLabel = wx.StaticText(self.panel1, id=-1,
                                   label=coltr("registryNo", "main"),
                                   style=wx.ALIGN_LEFT)
        labelFont = regnoLabel.GetFont()
        labelFont.SetWeight(wx.BOLD)
        regnoLabel.SetFont(labelFont)
        self.mainRegistryNo = wx.TextCtrl(self.panel1, style=wx.TE_READONLY)

        deviceNameLabel = wx.StaticText(self.panel1, id=-1,
                                        label=coltr("deviceName", "main"),
                                        style=wx.ALIGN_LEFT)
        deviceNameLabel.SetFont(labelFont)
        self.mainDeviceName = wx.TextCtrl(self.panel1, size=(250, -1))

        devManufLabel = wx.StaticText(self.panel1, id=-1,
                                      label=coltr("deviceManuf", "main"),
                                      style=wx.ALIGN_LEFT)
        devManufLabel.SetFont(labelFont)
        self.mainDeviceManuf = wx.TextCtrl(self.panel1, size=(250, -1))

        manufSerialNoLabel = wx.StaticText(self.panel1, id=-1,
                                           label=coltr("manufSerialNo",
                                                       "main"),
                                           style=wx.ALIGN_LEFT)
        manufSerialNoLabel.SetFont(labelFont)
        self.mainManufSerialNo = wx.TextCtrl(self.panel1, size=(250, -1))

        featureLabel = wx.StaticText(self.panel1, id=-1,
                                     label=coltr("features", "main"),
                                     style=wx.ALIGN_LEFT)
        featureLabel.SetFont(labelFont)
        self.mainFeatures = wx.TextCtrl(self.panel1, size=(250, -1))

        devStatuslabel = wx.StaticText(self.panel1,
                                       label=coltr("status", "main"))
        devStatuslabel.SetFont(labelFont)
        self.cbDevStatus = wx.ComboBox(self.panel1, style=wx.CB_READONLY |
                                       wx.CB_DROPDOWN | wx.CB_SORT)
        self.cbDevStatus.SetHelpText(_("You can add new statuses "
                                       "from the menu"))

        ownerLabel = wx.StaticText(self.panel1, label=coltr("owner", "main"))
        ownerLabel.SetFont(labelFont)
        self.cbOwner = wx.ComboBox(self.panel1, style=wx.CB_READONLY |
                                   wx.CB_DROPDOWN | wx.CB_SORT)
        self.cbOwner.SetHelpText(_("You can add new owners from the menu"))

        upComingDateSizer = wx.BoxSizer(wx.HORIZONTAL)
        calibrationDateLabel = wx.StaticText(self.panel1,
                                             label=coltr("comingDate", "main"))
        calibrationDateLabel.SetFont(labelFont)
        self.mainComingDate = wx.TextCtrl(self.panel1, size=(150, -1))

        pngstream = getCalendarButtonImage()
        png = wx.BitmapFromImage(wx.ImageFromStream(pngstream))

        try:
            mask = wx.Mask(png, wx.BLUE)
        except wx.PyAssertionError:
            mask = wx.MaskColour(png, wx.BITMAP_TYPE_PNG)
            if DEBUG:
                log_debug(debug_trace(), traceback.format_exc())
        png.SetMask(mask)

        dateTimePickerBut1 = wx.BitmapButton(self.panel1,
                                             MY_DATETIME_MAIN_BUT, png,
                                             size=(png.GetWidth() + 2,
                                                   png.GetHeight() + 2))
        dateTimePickerBut1.SetToolTipString(_("Open Date Picker"))

        # DESCRIPTION
        descLabel = wx.StaticText(self.panel1,
                                  label=coltr("description", "main"))
        descLabel.SetFont(labelFont)
        self.mainDescription = wx.TextCtrl(self.panel1, size=(350, 150),
                                        style=wx.TE_MULTILINE | wx.TE_AUTO_URL)

        # ADDITIONAL FIELDS
        bmp = wx.ArtProvider.GetBitmap(wx.ART_ADD_BOOKMARK,
                                       wx.ART_OTHER, (32, 32))
        additionalFieldButton = wx.Button(self.panel1, MY_ADDF_BUTTON,
                                          _("Additional Fields"),
                                          size=(150, -1))
        additionalFieldButton.SetBitmap(bmp)

        bmp = wx.ArtProvider.GetBitmap(wx.ART_FILE_SAVE,
                                       wx.ART_OTHER, (32, 32))
        mainSaveBut = wx.Button(self.panel1, MY_SAVE_MAIN,
                                _("Save"), size=(150, -1))
        mainSaveBut.SetBitmap(bmp)
        mainSaveBut.SetToolTipString(_("Click to Save the Record (Ctrl+S)"))
        mainSaveBut.Enable(self.has_write_permission)

        bmp = wx.ArtProvider.GetBitmap(wx.ART_UNDO,
                                       wx.ART_OTHER, (32, 32))
        mainClearBut = wx.Button(self.panel1, MY_CLR_MAIN,
                                 _("Clear"), size=(150, -1))
        mainClearBut.SetBitmap(bmp)
        mainClearBut.SetToolTipString(_("Click to Clear Form"))

        bmp = wx.ArtProvider.GetBitmap(wx.ART_DELETE, wx.ART_OTHER, (32, 32))
        deleteMainBut = wx.Button(self.panel1, MY_DEL_MAIN, _("Delete"),
                                  size=(150, -1))
        deleteMainBut.SetBitmap(bmp)
        deleteMainBut.Enable(self.has_write_permission)

        bmp = wx.ArtProvider.GetBitmap(wx.ART_FIND, wx.ART_OTHER, (32, 32))
        searchMainBut = wx.Button(self.panel1, MY_SEARCH_MAIN, _("Search"),
                                  size=(150, -1))
        searchMainBut.SetBitmap(bmp)

        self.SubSizer1.Add(wx.Size(15, 15))
        self.SubSizer1.Add(self.quickSearch, 0, wx.EXPAND)
        self.SubSizer1.Add(statlab, 0)
        self.SubSizer1.Add(self.openList, 0, wx.EXPAND)

        self.SubSizer2.Add(regnoLabel, flag=wx.LEFT | wx.TOP)
        self.SubSizer2.Add(self.mainRegistryNo, flag=wx.TOP | wx.LEFT | wx.GROW)
        self.SubSizer2.Add(deviceNameLabel, flag=wx.TOP | wx.LEFT)
        self.SubSizer2.Add(self.mainDeviceName, flag=wx.TOP | wx.LEFT | wx.GROW)
        self.SubSizer2.Add(devManufLabel, flag=wx.TOP | wx.LEFT)
        self.SubSizer2.Add(self.mainDeviceManuf,
                           flag=wx.TOP | wx.LEFT | wx.GROW)
        self.SubSizer2.Add(manufSerialNoLabel, flag=wx.TOP | wx.LEFT)
        self.SubSizer2.Add(self.mainManufSerialNo,
                           flag=wx.TOP | wx.LEFT | wx.GROW)
        self.SubSizer2.Add(featureLabel, flag=wx.TOP | wx.LEFT)
        self.SubSizer2.Add(self.mainFeatures, flag=wx.TOP | wx.LEFT | wx.GROW)

        self.SubSizer2.Add(devStatuslabel, flag=wx.TOP | wx.LEFT)
        self.SubSizer2.Add(self.cbDevStatus, flag=wx.TOP | wx.LEFT | wx.GROW)

        self.SubSizer2.Add(ownerLabel, flag=wx.TOP | wx.LEFT)
        self.SubSizer2.Add(self.cbOwner, flag=wx.TOP | wx.LEFT | wx.GROW)

        upComingDateSizer.Add(self.mainComingDate, flag=wx.TOP | wx.LEFT)
        upComingDateSizer.Add(dateTimePickerBut1, flag=wx.TOP | wx.RIGHT)
        self.SubSizer2.Add(calibrationDateLabel, 0, flag=wx.TOP | wx.LEFT)
        self.SubSizer2.Add(upComingDateSizer, flag=wx.TOP | wx.LEFT)

        self.SubSizer3.Add(descLabel, 0, flag=wx.TOP | wx.LEFT)
        self.SubSizer3.Add(self.mainDescription, 0, flag=wx.TOP | wx.RIGHT)

        btnGbSizer = wx.GridBagSizer(5, 5)
        btnGbSizer.Add(additionalFieldButton, (0, 0))
        btnGbSizer.Add(mainSaveBut, (1, 0))
        btnGbSizer.Add(mainClearBut, (2, 0))
        btnGbSizer.Add(deleteMainBut, (0, 1))
        btnGbSizer.Add(searchMainBut, (1, 1))
        self.SubSizer3.Add(btnGbSizer, 0)

    def checkComingForm(self, event=None):
        if hasattr(self, "calCtrlForm"):
            try:
                self.calCtrlForm.Raise()
                self.calCtrlForm.Iconize(False)
                return
            except wx.PyDeadObjectError:
                pass

        self.calCtrlForm = \
            wx.Frame(parent=self.frame, id=-1,
                     title=_("Kalibro - Calibration Date Control"),
                     size=(675, 750))

        self.calCtrlForm.SetIcon(self.frame.GetIcon())
        self.calCtrlForm.Bind(wx.EVT_CLOSE, self.closeFrame)
        sizer = wx.BoxSizer(wx.VERTICAL)
        panel = wx.Panel(self.calCtrlForm)
        rowbgcolors = {}
        data = []
        headers = []

        def create_list():
            query = """SELECT u1.registryNo,
            ifnull(julianday(u1.comingDate)-julianday('now'),'INVALID DATE')
            AS diff, u1.deviceName, u1.deviceManuf, u1.features, u1.status,
            u1.owner
            FROM main as u1"""
            headers.append(coltr("registryNo", "main"))
            headers.append(_("Remaining Day(s)"))
            headers.append(coltr("deviceName", "main"))
            headers.append(coltr("deviceManuf", "main"))
            headers.append(coltr("features", "main"))
            headers.append(coltr("status", "main"))
            headers.append(coltr("owner", "main"))
            myob = self.dbConn

            results = myob.fetch_all(query=query).result

            yellowAlert = 14
            orangeAlert = 7
            redAlert = 1
            sonuclarL = list(results)
            # Sort per second column of the rows
            sonuclarL.sort(key=operator.itemgetter(1))
            data.extend(list(map(list, sonuclarL)))
            for i in range(0, len(sonuclarL)):
                for j in range(0, len(sonuclarL[i])):
                    if(j == 1 and sonuclarL[i][j] != "INVALID DATE"):
                        days = int(math.ceil(float(sonuclarL[i][j])))
                        data[i][j] = str(days)
                    try:
                        if(int(sonuclarL[i][1]) <= yellowAlert and
                           int(sonuclarL[i][1]) > orangeAlert):
                            rowbgcolors[i] = {"bgcolor": "#FFFF00"}
                        elif(int(sonuclarL[i][1]) <= orangeAlert and
                             int(sonuclarL[i][1]) > redAlert):
                            rowbgcolors[i] = {"bgcolor": "#FFA500"}
                        elif(int(sonuclarL[i][1]) <= redAlert):
                            rowbgcolors[i] = {"bgcolor": "#FF0000"}
                    except ValueError:
                        rowbgcolors[i] = {"bgcolor": "#A9A9A9"}
                    if data[i][j] is None:
                        data[i][j] = ""

            calCtrlList = CalibrationCtrlList(panel, data,
                                              headers, rowbgcolors)

            def on_selected_item(evt):
                regno = evt.GetEventObject().GetItemText(evt.GetIndex())
                if not regno:
                    return
                pubsub.publish("search_select", regno)

            calCtrlList.Bind(wx.EVT_LIST_ITEM_SELECTED, on_selected_item)
            sizer.Add(calCtrlList, 1, flag=wx.ALL | wx.EXPAND)

        def on_print(e):
            prt = printout.PrintTable(self.calCtrlForm)
            prt.label = headers
            prt.data = data
            # columns width proportions
            prt.set_column = [1, 1.5, 2, 1.5, 1.5, 1, 1.5]

            for c in range(len(headers)):
                prt.SetColAlignment(c, wx.ALIGN_LEFT)
                for r in range(len(data)):
                    bgcolor = rowbgcolors.get(r, {}).get("bgcolor")
                    if bgcolor:
                        prt.SetCellColour(r, c, bgcolor)
            dt = datetime.now().strftime(self.date_format + " %H:%M:%S")
            prt.SetHeader(_("Printed") + ": " + dt, type="Text",
                          align=wx.ALIGN_RIGHT, indent=-1)
            font = copy.deepcopy(prt.default_font)
            font["Size"] += 6
            prt.SetHeader(self.calCtrlForm.GetTitle(), type="Text", font=font,
                          align=wx.ALIGN_LEFT, indent=1)
            prt.SetFooter(_("Page No") + ": ", type="Num")
            prt.SetLandscape()
            prt.Preview()

        btn_sizer = wx.BoxSizer(wx.HORIZONTAL)
        btn_print = wx.Button(panel, -1, _("Print"))
        btn_print.Bind(wx.EVT_BUTTON, on_print)
        btn_sizer.Add(btn_print, 0, flag=wx.ALIGN_LEFT)
        sizer.Add(btn_sizer, 0)
        create_list()
        panel.SetSizer(sizer)
        panel.SetAutoLayout(True)
        self.calCtrlForm.Center()
        self.calCtrlForm.Show(True)

    def showDateTimeFrame(self, event, grid=True):
        if hasattr(self, "dateTimeFrame"):
            try:
                self.dateTimeFrame.Raise()
                self.dateTimeFrame.Iconize(False)
                return
            except wx.PyDeadObjectError:
                pass

        def OnCalSelChanged(event):
            cal = event.GetEventObject()

            dtY = str(wx.DateTime.GetYear(cal.GetDate()))
            if(len(dtY) == 4):
                dtY = dtY[2:]

            dtM = wx.DateTime.GetMonth(cal.GetDate()) + 1
            dtM = str(dtM)
            dtD = str(wx.DateTime.GetDay(cal.GetDate()))
            dateStr = dtM + "/" + dtD + "/" + dtY

            dateStr = datetime.strptime(dateStr, "%m/%d/%y")
            dateStr = dateStr.strftime(self.date_format)
            dateShow.SetValue(dateStr)
            event.Skip()

        def getAndClose(*args):
            chosenDate = dateShow.GetValue()
            if len(chosenDate) == 0:
                return False

            if grid:
                try:
                    row, col = self.selectedCell
                except:
                    row, col = 0, 2

                if(self.myGrid.IsReadOnly(row, col)):
                    self.message(_("I cannot add date to that cell. "
                                   "Reason: ReadOnly"))
                else:
                    self.myGrid.SetCellValue(row, col, chosenDate)
            else:
                self.mainComingDate.SetValue(chosenDate)

            event.Skip()
            self.dateTimeFrame.Destroy()

        self.dateTimeFrame = wx.MiniFrame(self.frame, -1,
                                          title=_("Choose Date"),
                                          size=(275, 200),
                                          style=wx.DEFAULT_FRAME_STYLE)

        dateTimePanel = wx.Panel(self.dateTimeFrame, -1, size=(275, 200),
                                 pos=wx.DefaultPosition)
        # Grid Bag Sizer to place the widgets
        gbs = wx.GridBagSizer(2, 2)
        bsizer = wx.BoxSizer()

        dateShow = wx.TextCtrl(dateTimePanel, -1, size=(130, 25),
                               style=wx.TE_READONLY)

        cal1 = wx.calendar.CalendarCtrl(dateTimePanel, -1, wx.DateTime_Now(),
                                        size=(275, -1),
                                        style=wx.calendar.CAL_MONDAY_FIRST)
        self.dateTimeFrame.Bind(wx.calendar.EVT_CALENDAR_SEL_CHANGED,
                                OnCalSelChanged, cal1)

        dateButton = wx.Button(dateTimePanel, -1, _("Get and Close"))
        gbs.Add(dateShow, (0, 0))
        gbs.Add(dateButton, (0, 1))
        gbs.Add(cal1, (1, 0), (1, 2), flag=wx.EXPAND)

        bsizer.Add(gbs)
        self.dateTimeFrame.Bind(wx.EVT_CLOSE, self.closeFrame)
        self.dateTimeFrame.Bind(wx.EVT_BUTTON, getAndClose, dateButton)

        dateTimePanel.FitInside()
        self.dateTimeFrame.SetSizerAndFit(bsizer)
        self.dateTimeFrame.CentreOnParent()
        self.dateTimeFrame.Show(True)

    def closeFrame(self, event=None):
        """Closes the frame that triggers close event"""
        event.GetEventObject().Destroy()
        event.Skip()
        return

    def onAboutMenu(self, evt=None):
        info = wx.AboutDialogInfo()
        info.SetName("Kalibro")
        info.SetVersion(VERSION)
        info.SetCopyright("(C) 2009-2016 %s" % AUTHOR)
        desc_trans = _("Kalibro is such a software that you can keep your business calibration "
                       "records and check coming calibration dates. "
                       "It satisfies the requirements of the Quality Management "
                       "Standards like ISO9001, AS9100 and TS16949")

        desc_trans = wordwrap(desc_trans, 500, wx.ClientDC(self.frame))

        info.SetDescription(desc_trans)
        info.SetWebSite((HOME_PAGE_URL, _("Home Page")))
        info.SetDevelopers(["%s\n%s" % (AUTHOR, AUTHOR_EMAIL)])
        info.SetIcon(self.frame.GetIcon())

        licenseText = """This program is free software: you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>."""
        info.SetLicense(licenseText)
        wx.AboutBox(info)

    def changeDateFormat(self, event=None):
        _day, _month, _YEAR = _("day"), _("month"), _("YEAR")
        dlg = wx.SingleChoiceDialog(
               self.frame, _('Choose one of the format'), _('Set Date Format'),
               ["-".join((_day, _month, _YEAR)),
                "-".join((_month, _day, _YEAR)),
                "-".join((_YEAR, _month, _day)),
                "/".join((_day, _month, _YEAR)),
                "/".join((_month, _day, _YEAR)),
                "/".join((_YEAR, _month, _day))],
               wx.CHOICEDLG_STYLE
               )

        if(self.date_format == "%d-%m-%Y"):
            dlg.SetSelection(0)
        elif(self.date_format == "%m-%d-%Y"):
            dlg.SetSelection(1)
        elif(self.date_format == "%Y-%m-%d"):
            dlg.SetSelection(2)
        elif(self.date_format == "%d/%m/%Y"):
            dlg.SetSelection(3)
        elif(self.date_format == "%m/%d/%Y"):
            dlg.SetSelection(4)
        elif(self.date_format == "%Y/%m/%d"):
            dlg.SetSelection(5)

        if dlg.ShowModal() == wx.ID_OK:
            config = ConfigManager()
            if(dlg.GetSelection() == 0):
                config["date_format"] = "%d-%m-%Y"
                self.date_format = "%d-%m-%Y"
            elif(dlg.GetSelection() == 1):
                config["date_format"] = "%m-%d-%Y"
                self.date_format = "%m-%d-%Y"
            elif(dlg.GetSelection() == 2):
                config["date_format"] = "%Y-%m-%d"
                self.date_format = "%Y-%m-%d"
            elif(dlg.GetSelection() == 3):
                config["date_format"] = "%d/%m/%Y"
                self.date_format = "%d/%m/%Y"
            elif(dlg.GetSelection() == 4):
                config["date_format"] = "%m/%d/%Y"
                self.date_format = "%m/%d/%Y"
            elif(dlg.GetSelection() == 5):
                config["date_format"] = "%Y/%m/%d"
                self.date_format = "%Y/%m/%d"
        dlg.Destroy()

    def onHelp(self, evt):
        webbrowser.open(HELP_PAGE_URL)

    def onReleaseNotes(self, evt):
        return self.showTextFileDlg("RELEASE_NOTES.txt", _("Release Notes"))

    def onReadMe(self, evt):
        return self.showTextFileDlg("README.txt", _("Read Me"))

    def showTextFileDlg(self, filepath, title):
        try:
            msg = open(filepath, "r").read()
        except:
            self.message(_("file is missing"))
            if DEBUG:
                log_debug(debug_trace(), traceback.format_exc())
            return
        dlg = wx.lib.dialogs.ScrolledMessageDialog(self.frame, msg, title)
        dlg.ShowModal()
        dlg.Destroy()

    def onReporting(self, event=None):
        try:
            reporting(parent=self.frame,
                      size=(400, 400),
                      dateformat=self.date_format, language=self.lang,
                      file_encoding=self.file_encoding)
        except Exception as e:
            wx.MessageBox(str(e), _("Error"), wx.ICON_ERROR)
            if DEBUG:
                log_debug(debug_trace(), traceback.format_exc())

    def onHistroyLog(self, evt=None):
        HistoryLogFrame(self.frame)

    def onChangeDevStatus(self, evt):
        if self.mainRegistryNo.IsEditable():
            return
        query = "UPDATE main SET status=? WHERE registryNo=?"
        registryNo = self.mainRegistryNo.GetValue()
        status = self.cbDevStatus.GetValue() or None
        if not registryNo:
            return
        tail = (status, registryNo)
        sql = self.dbConn
        try:
            dlg = wx.MessageDialog(self.frame,
                                   _("Do you want to save this status?"),
                                   _("Save?"), wx.YES_NO | wx.YES_DEFAULT)
            if dlg.ShowModal() == wx.ID_YES:
                sql.execute(query, tail)
            dlg.Destroy()
        except Exception as e:
            if DEBUG:
                log_debug(debug_trace(), traceback.format_exc())
            wx.MessageBox(str(e), _("Error"), wx.ICON_ERROR)

    def onChangeOwner(self, evt):
        if self.mainRegistryNo.IsEditable():
            return
        query = "UPDATE main SET owner=? WHERE registryNo=?"
        registryNo = self.mainRegistryNo.GetValue()
        owner = self.cbOwner.GetValue() or None
        if not registryNo:
            return
        tail = (owner, registryNo)
        sql = self.dbConn
        try:
            dlg = wx.MessageDialog(self.frame,
                                   _("Do you want to save this owner?"),
                                   _("Save?"), wx.YES_NO | wx.YES_DEFAULT)
            if dlg.ShowModal() == wx.ID_YES:
                sql.execute(query, tail)
            dlg.Destroy()
        except Exception as e:
            if DEBUG:
                log_debug(debug_trace(), traceback.format_exc())
            wx.MessageBox(str(e), _("Error"), wx.ICON_ERROR)

