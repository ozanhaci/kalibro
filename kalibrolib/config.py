# encoding: utf-8
from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import

import os
import re
import glob
import traceback
import encodings
import datetime
import ConfigParser

from .debugging import DEBUG, debug_trace, log_debug
from .constants import (VERSION, DEF_DATEFORMAT, DEF_LANG, DEF_FILE_ENCODING)

__all__ = ["ConfigManager", "ConfigError"]
str = unicode


class ConfigError(Exception):
    pass


class ConfigManager(object):

    def __init__(self,
                 config_path="config/kalibro.cfg",
                 old_config_path="config/mainconfig.ini",
                 defaults=None,
                 bool_options=None,
                 int_options=None,
                 float_options=None,
                 parser_cls=ConfigParser.RawConfigParser,
                 ):
        self.parser_cls = parser_cls
        self.config_path = config_path
        self.old_config_path = old_config_path
        self._parser = None
        self.compfixed = False
        self.created = False
        self.defaults = defaults or {"language": DEF_LANG,
                                     "date_format": DEF_DATEFORMAT,
                                     "file_encoding": DEF_FILE_ENCODING,
                                     "version_check": True,
                                     "splash": True}
        self.bool_options = bool_options or ["version_check", "splash"]
        self.int_options = int_options or []
        self.float_options = float_options or []
        self.options = self.defaults.keys()

    def create(self):
        if not os.path.exists(self.config_path):
            dirs, file_ = os.path.split(self.config_path)
            if not os.path.exists(dirs):
                try:
                    os.makedirs(dirs)
                except:
                    err = "config folder could not be created"
                    if DEBUG:
                        log_debug(debug_trace(), err,
                                  traceback.format_exc())
                    raise ConfigError(err)
            self.flush()
            self.created = True
        else:
            self.get_parser()
            self.flush()

    def get_parser(self):
        if self._parser is None:
            parser = self.parser_cls(self.defaults,
                                     allow_no_value=False)
            parser.read(self.config_path)
            if not parser.has_section("client"):
                parser.add_section("client")
            self._parser = parser
            return parser
        else:
            return self._parser

    def set_parser(self, v):
        self._parser = v

    parser = property(get_parser, set_parser)

    def fix_compatibility(self):
        """
        Backward compatibility fix
        """
        if self.compfixed:
            return
        configparser = self.parser
        if configparser.has_option("client", "db_backup"):
            configparser.remove_option("client", "db_backup")
        if configparser.has_option("DEFAULT", "db_backup"):
            configparser.remove_option("DEFAULT", "db_backup")
        if configparser.has_option("client", "version"):
            configparser.remove_option("client", "version")
        if configparser.has_option("DEFAULT", "version"):
            configparser.remove_option("DEFAULT", "version")
        self.flush()
        self.compfixed = True

    def installed_langs(self):
        _langs = map(lambda x:
                     re.findall(r"locale[\/|\\](\w{2,})[\/|\\]LC_MESSAGES", x,
                                re.U | re.I),
                     glob.glob(r"locale/*/LC_MESSAGES/Kalibro.mo"))
        langs = []
        for lang in _langs:
            if isinstance(lang, list) and lang:
                langs.append(lang[0])
        return langs

    @classmethod
    def installed_encodings(self):
        lst = set(encodings.aliases.aliases.values())
        return list(lst)

    def set(self, option, val):
        option = option.lower()
        if option not in self.options:
            raise ConfigError("Undefined Option in Config")
        if not self.option_check_fix(option, mod="set", setval=val):
            raise ConfigError("Option Value Error")
        self.parser.set("client", option, val)
        self.flush()

    def get(self, option):
        option = option.lower()
        if option not in self.options:
            raise ConfigError("Undefined Option in Config")
        try:
            self.option_check_fix(option, mod="get")
            if option in self.bool_options:
                return self.parser.getboolean("client", option)
            elif option in self.int_options:
                return self.parser.getint("client", option)
            elif option in self.float_options:
                return self.parser.getfloat("client", option)
            else:
                return self.parser.get("client", option)
        except ConfigParser.NoOptionError:
            try:
                return self.defaults[option]
            except KeyError:
                return None

    def option_check_fix(self, option, mod="get", setval=None):
        if mod == "get":
            val = self.parser.get("client", option)
        elif mod == "set":
            val = setval
        if option == "language":
            if val not in self.installed_langs():
                if mod == "get" and self.parser.has_option("client", option):
                    self.parser.remove_option("client", option)
                    self.flush()
            else:
                return True
        elif option == "file_encoding":
            if val not in self.installed_encodings():
                if mod == "get" and self.parser.has_option("client", option):
                    self.parser.remove_option("client", option)
                    self.flush()
            else:
                return True
        elif option == "date_format":
            try:
                datetime.datetime.now().strftime(val)
            except ValueError:
                if mod == "get" and self.parser.has_option("client", option):
                    self.parser.remove_option("client", option)
                    self.flush()
            else:
                return True
        else:
            return True
        return False

    def flush(self):
        """
        write config to file
        """
        try:
            with open(self.config_path, 'wb') as configfile:
                self.parser.write(configfile)
                configfile.flush()
            self.parser = None
        except:
            if DEBUG:
                log_debug(debug_trace(), traceback.format_exc())

    def __getitem__(self, option):
        return self.get(option)

    def __setitem__(self, option, val):
        return self.set(option, val)

