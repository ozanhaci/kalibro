# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import datetime

import wx
from wx.lib.intctrl import IntCtrl

from .conn_sqlite import MainSql, IntegrityError
from .auth import get_user, ModelAuth
from .utils import coltr


class AdditionalFieldsWin(wx.Frame, object):

    def __init__(self, parent, icon, regNo=""):

        amodel = ModelAuth()
        username = get_user()
        installed = amodel.auth_installed()
        if installed:
            self.has_write_permission = amodel.has_write_perm(username)
        else:
            self.has_write_permission = True

        self.regNo = regNo
        super(AdditionalFieldsWin, self).__init__(parent, -1, size=(500, 450),
                                                    title=_("Additional Fields Window for *%s*") % self.regNo,
                                                    style=wx.FRAME_FLOAT_ON_PARENT | wx.DEFAULT_FRAME_STYLE)
        self.SetIcon(icon)
        self.panel = panel = wx.ScrolledWindow(self, -1, style=wx.HSCROLL | wx.VSCROLL)
        panel.SetScrollRate(5, 5)

        mainsizer = wx.BoxSizer(wx.VERTICAL)
        self.col_pane = wx.CollapsiblePane(panel, label=_("Field Labels"),
                                           style=wx.CP_DEFAULT_STYLE | wx.CP_NO_TLW_RESIZE)
        self.Bind(wx.EVT_COLLAPSIBLEPANE_CHANGED, self.onPaneChanged, self.col_pane)
        self.makePaneContent(self.col_pane.GetPane())
        mainsizer.Add(self.col_pane, 0, flag=wx.LEFT | wx.GROW, border=5)

        sizer = wx.FlexGridSizer(cols=2, hgap=5, vgap=5)
        sizer.AddGrowableCol(1)
        self.stIntLabel = wx.StaticText(panel, -1, _("Integer Field"))
        sizer.Add(self.stIntLabel, 0)
        self.textInt = IntCtrl(panel, allow_long=True, allow_none=True, value=None)
        sizer.Add(self.textInt, 0, flag=wx.ALL | wx.GROW)

        self.stText1Label = wx.StaticText(panel, -1, _("Text Field 1"))
        sizer.Add(self.stText1Label, 0)

        self.textText1 = wx.TextCtrl(panel, -1, size=(-1, 100),
                                     style=wx.TE_MULTILINE | wx.HSCROLL)
        sizer.Add(self.textText1, 0, flag=wx.ALL | wx.GROW)

        self.stText2Label = wx.StaticText(panel, -1, _("Text Field 2"))
        sizer.Add(self.stText2Label, 0)
        self.textText2 = wx.TextCtrl(panel, -1, size=(-1, 100),
                                    style=wx.TE_MULTILINE | wx.HSCROLL)
        sizer.Add(self.textText2, 0, flag=wx.ALL | wx.GROW)


        self.stDateLabel = wx.StaticText(panel, -1, _("Date Field"))
        sizer.Add(self.stDateLabel, 0)
        self.textDate = wx.DatePickerCtrl(panel, -1, style=wx.DP_DROPDOWN |
                                          wx.DP_SHOWCENTURY | wx.DP_ALLOWNONE)

        sizer.Add(self.textDate, 0, flag=wx.ALL | wx.GROW)

        btn_sizer = wx.BoxSizer(wx.HORIZONTAL)
        saveBut = wx.Button(panel, -1, label=_("Save"))
        saveBut.Enable(self.has_write_permission)
        cancelBut = wx.Button(panel, -1, label=_("Cancel"))
        cancelBut.Enable(self.has_write_permission)

        btn_sizer.Add(saveBut, 0)
        btn_sizer.Add(cancelBut, 0)
        sizer.Add(wx.Size(10, 10), 0)
        sizer.Add(btn_sizer, 0)

        panel.Bind(wx.EVT_BUTTON, self.saveValuesToDB)
        panel.Bind(wx.EVT_BUTTON, self.cancelButton, source=cancelBut)

        mainsizer.Add(sizer, 0, flag=wx.ALL | wx.GROW, border=5)
        panel.SetSizerAndFit(mainsizer)
        self.CenterOnParent()
        self.Show(True)

        self.getValuesFromDB()

    def makePaneContent(self, pane):
        label1 = wx.StaticText(pane, -1, coltr("IntLabel", "mainAddFieldLabel"))
        self.labelIntCtrl = wx.TextCtrl(pane,)
        label2 = wx.StaticText(pane, -1, coltr("TextLabel1", "mainAddFieldLabel"))
        self.labelText1Ctrl = wx.TextCtrl(pane,)
        label3 = wx.StaticText(pane, -1, coltr("TextLabel2", "mainAddFieldLabel"))
        self.labelText2Ctrl = wx.TextCtrl(pane,)
        label4 = wx.StaticText(pane, -1, coltr("DateLabel", "mainAddFieldLabel"))
        self.labelDateCtrl = wx.TextCtrl(pane,)

        fsizer = wx.FlexGridSizer(cols=2, hgap=5, vgap=5)
        fsizer.AddGrowableCol(1)
        fsizer.Add(label1, 0)
        fsizer.Add(self.labelIntCtrl, 0)
        fsizer.Add(label2, 0)
        fsizer.Add(self.labelText1Ctrl, 0)
        fsizer.Add(label3, 0)
        fsizer.Add(self.labelText2Ctrl, 0)
        fsizer.Add(label4, 0)
        fsizer.Add(self.labelDateCtrl, 0)

        border = wx.BoxSizer()
        border.Add(fsizer, 1, wx.EXPAND | wx.ALL, 5)
        pane.SetSizer(border)

    def onPaneChanged(self, evt):
        maxWidth, maxHeight = self.panel.GetSize()
        if maxWidth < self.GetSize()[0]:
            maxWidth = self.GetSize()[0]
        if maxHeight < self.GetSize()[1]:
            maxHeight = self.GetSize()[1]
        self.panel.SetVirtualSize((maxWidth, maxHeight))
        self.panel.Layout()
        self.Refresh()
        evt.Skip()

    def getValuesFromDB(self, evt=None):
        if(self.regNo == ""):
            return
        tail = {"regno": self.regNo}
        query = "SELECT * FROM mainAddField WHERE registryNo=:regno"
        myob = MainSql()
        results = myob.fetch_all(query, tail=tail).result
        for rec in results:
            self.textInt.SetValue(int(rec[1]) if rec[1] is not None else None)
            self.textText1.SetValue(rec[2] or "")
            self.textText2.SetValue(rec[3] or "")
            if isinstance(rec[4], (datetime.datetime, datetime.date)):
                tt = rec[4].timetuple()[:3]
                conval = wx.DateTimeFromDMY(tt[2], tt[1] - 1, tt[0])
                self.textDate.SetValue(conval)

        query = "SELECT * FROM mainAddFieldLabel LIMIT 1"
        myob = MainSql()
        results = myob.fetch_all(query).result
        for rec in results:
            self.labelIntCtrl.SetValue(rec[0] or "")
            if rec[0]:
                self.stIntLabel.SetLabel(rec[0])
            if rec[1]:
                self.stText1Label.SetLabel(rec[1])
            self.labelText1Ctrl.SetValue(rec[1] or "")
            if rec[2]:
                self.stText2Label.SetLabel(rec[2])
            self.labelText2Ctrl.SetValue(rec[2] or "")
            if rec[3]:
                self.stDateLabel.SetLabel(rec[3])
            self.labelDateCtrl.SetValue(rec[3] or "")
        self.panel.Layout()

    def saveValuesToDB(self, evt=None):
        if self.regNo == "":
            return
        queryInsert = """INSERT INTO mainAddField
                 (registryNo,IntValue,TextValue1,TextValue2, DateValue)
                 VALUES
                 (:registryNo,:IntValue,:TextValue1,:TextValue2,:DateValue)"""
        queryUpdate = """UPDATE mainAddField SET IntValue=:IntValue,
            TextValue1=:TextValue1,TextValue2=:TextValue2,DateValue=:DateValue
            WHERE registryNo=:registryNo"""
        queryLabelUpdate = """UPDATE mainAddFieldLabel SET IntLabel=:IntLabel,
            TextLabel1=:TextLabel1,TextLabel2=:TextLabel2,DateLabel=:DateLabel"""
        try:
            int_val = int(self.textInt.GetValue())
        except:
            int_val = None
        date_val = self.textDate.GetValue()
        date_val = date_val.IsValid() and date_val or None
        if date_val is not None:
            try:
                date_val = date_val.FormatISOCombined(b" ")
                date_val = datetime.datetime.strptime(date_val, "%Y-%m-%d %H:%M:%S")
            except:
                date_val = None
        tail = {"registryNo": self.regNo,
                "IntValue": int_val,
                "TextValue1": self.textText1.GetValue() or None,
                "TextValue2": self.textText2.GetValue() or None,
                "DateValue": date_val}
        tailLabel = {"IntLabel": self.labelIntCtrl.GetValue(),
                     "TextLabel1": self.labelText1Ctrl.GetValue(),
                     "TextLabel2": self.labelText2Ctrl.GetValue(),
                     "DateLabel": self.labelDateCtrl.GetValue()}
        myob = MainSql()
        try:
            if len(list(tailLabel.values())) != len(set(tailLabel.values())):
                raise Exception(_("Label names must be unique"))
            # if all values are none, do not save it.
            if not all([v is None for k, v in tail.items() if k != "registryNo"]):
                try:
                    myob.execStmt(queryInsert, tail)
                except IntegrityError:
                    myob.execStmt(queryUpdate, tail)
            else:
                try:
                    myob.execStmt('DELETE FROM mainAddField WHERE registryNo=:registryNo',
                                  {"registryNo": self.regNo})
                except:
                    pass
            if self.labelIntCtrl.IsModified() or \
                self.labelText1Ctrl.IsModified() or \
                self.labelText2Ctrl.IsModified() or \
                self.labelDateCtrl.IsModified():
                myob.execStmt(queryLabelUpdate, tailLabel)
        except Exception as e:
            wx.MessageBox(str(e), _("Error"), style=wx.ICON_ERROR)
        else:
            wx.MessageBox(_("Saved"), _("Information"),
                          style=wx.OK | wx.CENTER)

    def cancelButton(self, evt):
        self.Close(True)
