# encoding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import absolute_import

import sys

import wx
from wx.lib.mixins.listctrl import CheckListCtrlMixin, ColumnSorterMixin
from wx.lib.newevent import NewEvent

from . import images

CheckListItemChecked, EVT_CHLIST_ITEM_CHECKED = NewEvent()


class CheckListCtrl(wx.ListCtrl, CheckListCtrlMixin):

    def __init__(self, parent, select_data, size):
        wx.ListCtrl.__init__(self, parent, style=wx.LC_REPORT, size=size)
        self.select_data = select_data
        CheckListCtrlMixin.__init__(self)
        self.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.OnItemActivated)

    def OnItemActivated(self, evt):
        self.ToggleItem(evt.m_itemIndex)

    # this is called by the base class when an item is checked/unchecked
    def OnCheckItem(self, index, flag):
        data = self.GetItemData(index)
        row = self.select_data[data]
        evt = CheckListItemChecked(itemData=data,
                                   row=row, index=index, flag=flag)
        wx.PostEvent(self.Parent, evt)


class MultiSelectCheckBox(wx.Dialog, ColumnSorterMixin):

    def __init__(self, parent=None, title="", footer="",
                 select_data=None, columns=None, size=wx.DefaultSize):
        wx.Dialog.__init__(self, parent)
        self.SetTitle(title)
        self.list = CheckListCtrl(self, select_data, size=size)

        self.img_list = self.list.GetImageList(wx.IMAGE_LIST_SMALL) or \
            wx.ImageList(16, 16)
        self.img_down = self.img_list.Add(images.SmallDnArrow.GetBitmap())
        self.img_up = self.img_list.Add(images.SmallUpArrow.GetBitmap())
        self.list.SetImageList(self.img_list, wx.IMAGE_LIST_SMALL)

        ColumnSorterMixin.__init__(self, len(columns))
        self.itemDataMap = select_data
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(wx.Size(0, 10))

        sizer.Add(wx.StaticText(self, label=title), 0, wx.ALIGN_CENTER)
        sizer.Add(self.list, 1, wx.EXPAND)

        for cidx, col in enumerate(columns):
            self.list.InsertColumn(cidx, col)

        for key, row in enumerate(select_data):
            index = self.list.InsertStringItem(sys.maxint, row[0])
            for cidx, cell in enumerate(row[1:]):
                self.list.SetStringItem(index, cidx + 1, cell)

            self.list.SetItemData(index, key)

        for cidx in range(len(columns)):
            self.list.SetColumnWidth(cidx, wx.LIST_AUTOSIZE)

        btnsizer = wx.BoxSizer(wx.HORIZONTAL)

        btn = wx.Button(self, wx.ID_ANY, label=_("Toggle Items"))
        btn.Bind(wx.EVT_BUTTON, self.on_toggle)
        btnsizer.Add(btn)

        btn = wx.Button(self, wx.ID_OK)
        btn.SetDefault()
        btnsizer.Add(btn)

        btn = wx.Button(self, wx.ID_CANCEL)
        btnsizer.Add(btn)

        if footer:
            sizer.Add(wx.StaticText(self, label=footer), 0, wx.ALIGN_CENTER)
        sizer.Add(btnsizer, 0, wx.ALIGN_CENTER_VERTICAL |
                  wx.ALIGN_CENTER_HORIZONTAL | wx.ALL, 5)
        self.SetSizer(sizer)
        sizer.Fit(self)
        self.CenterOnParent()

    def GetListCtrl(self):
        return self.list

    def GetSortImages(self):
        return (self.img_down, self.img_up)

    def on_toggle(self, evt):
        nitem = self.list.GetItemCount()
        if not nitem:
            return
        for i in range(nitem):
            self.list.ToggleItem(i)

    @property
    def checked_items(self):
        nitem = self.list.GetItemCount()
        if not nitem:
            return []
        else:
            checkeds = []
            for i in range(nitem):
                if self.list.IsChecked(i):
                    regno = self.list.GetItemText(i)
                    if regno not in checkeds:
                        checkeds.append(regno)
            return checkeds



if __name__ == "__main__":

    columns = ["name", "surname", "age"]
    select_data = [["name1", "surname1", "32"], ["name2", "surname2", "29"]]
    title = "MultiSelectCheckBox Test"
    footer = "Footer Test\nFooter Line 2"

    app = wx.App()
    frame = wx.Frame(None, -1, size=(-1, 200))
    panel = wx.Panel(frame)
    button = wx.Button(panel, pos=(0, 0), label="Click to Test")

    def test(evt):
        dlg = MultiSelectCheckBox(frame, title, footer, select_data, columns,
                                  size=(500, 500))
        dlg.ShowModal()
        print("Checked Items", dlg.checked_items)
        dlg.Destroy()
    button.Bind(wx.EVT_BUTTON, test)

    frame.CenterOnScreen()
    frame.Show()
    app.MainLoop()
