Kalibro is an open source software to manage devices and tools for calibration and
maintenance records. It helps you to keep inventory and reminds you the
scheduled dates. Kalibro also help you to satisfy the requirements of the
Quality Management Standards like ISO9001, AS9100 and TS16949. Manufacturing,
Quality and IT departments of small/medium sized companies are targeted to use
Kalibro.

Licenses of libraries used in Kalibro are placed in licenses folder

See INSTALL.txt for installation and upgrade instructions

See RELEASE_NOTES.txt for changes


Copyright (C) 2009-2016  Ozan Hacibekiroglu (ozan_haci@yahoo.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
