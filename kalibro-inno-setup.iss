
#define MyAppName "Kalibro"
#define MyAppVersion "2.6.2.1"
#define MyAppPublisher "Ozan HACIBEKIROGLU"
#define MyAppURL "http://www.ozanh.com/kalibro"
#define MyAppExeName "Kalibro.exe"

[Setup]
DisableFinishedPage=True
AlwaysShowGroupOnReadyPage=True
AlwaysShowDirOnReadyPage=True
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppCopyright={#MyAppPublisher}
AppId={{7482EA6A-838C-4294-938E-7D758E326F43}
LicenseFile=licenses\kalibro\LICENSE
InfoBeforeFile=README.txt
AppPublisher={#MyAppPublisher}
DefaultDirName={pf}\{#MyAppName}
DefaultGroupName={#MyAppName}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
VersionInfoVersion={#MyAppVersion}
VersionInfoCopyright={#MyAppPublisher}
VersionInfoProductName=Kalibro
VersionInfoProductVersion={#MyAppVersion}
SolidCompression=True
Compression=lzma2/ultra64
MinVersion=0,5.01
OutputDir=.
OutputBaseFilename={#MyAppName}-v{#MyAppVersion}-Setup
AllowNoIcons=yes
UninstallDisplayName={#MyAppName} {#MyAppVersion}
UninstallDisplayIcon={app}\Kalibro.exe
InternalCompressLevel=ultra64
InfoAfterFile=RELEASE_NOTES.txt

[Files]
Source: "dist\Kalibro.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "dist\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs; Excludes: "Kalibro.exe"

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{group}\{cm:ProgramOnTheWeb,{#MyAppName}}"; Filename: "{#MyAppURL}"
Name: "{group}\{cm:UninstallProgram,{#MyAppName}}"; Filename: "{uninstallexe}"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: quicklaunchicon

[ThirdParty]
UseRelativePaths=True

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"
Name: "quicklaunchicon"; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}"; OnlyBelowVersion: 0,6.1

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"
Name: "polish"; MessagesFile: "compiler:Languages\Polish.isl"
Name: "turkish"; MessagesFile: "compiler:Languages\Turkish.isl"

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent

[Code]

procedure CurPageChanged(CurPageID: Integer);
begin
  if CurPageID = wpSelectDir then
    MsgBox('If you do not have administrative rights in the target setup folder,  please use zip archive instead of this installer', mbInformation, MB_OK);
end;


function GetUninstallString: string;
var
  sUnInstPath: string;
  sUnInstallString: String;
begin
  Result := '';
  sUnInstPath := ExpandConstant('Software\Microsoft\Windows\CurrentVersion\Uninstall\{{7482EA6A-838C-4294-938E-7D758E326F43}_is1');
  sUnInstallString := '';
  if not RegQueryStringValue(HKLM, sUnInstPath, 'UninstallString', sUnInstallString) then
    RegQueryStringValue(HKCU, sUnInstPath, 'UninstallString', sUnInstallString);
  Result := sUnInstallString;
end;

function IsUpgrade: Boolean;
begin
  Result := (GetUninstallString() <> '');
end;

function InitializeSetup: Boolean;
var
  V: Integer;
  iResultCode: Integer;
  sUnInstallString: string;
begin
  Result := True; // in case when no previous version is found
  if RegValueExists(HKEY_LOCAL_MACHINE,'Software\Microsoft\Windows\CurrentVersion\Uninstall\{7482EA6A-838C-4294-938E-7D758E326F43}_is1', 'UninstallString') then
  begin
    V := MsgBox(ExpandConstant('Another version of {#MyAppName} was detected. You should uninstall it to continue setup. Do not forget to backup your data. Would you like to uninstall?'), mbInformation, MB_YESNO); //Custom Message if App installed
    if V = IDYES then
    begin
      sUnInstallString := GetUninstallString();
      sUnInstallString := RemoveQuotes(sUnInstallString);
      Exec(ExpandConstant(sUnInstallString), '', '', SW_SHOW, ewWaitUntilTerminated, iResultCode);
      Result := True; //if you want to proceed after uninstall
      //Exit; //if you want to quit after uninstall
    end
    else
      Result := False; //when another version present and not uninstalled
  end;
end;
